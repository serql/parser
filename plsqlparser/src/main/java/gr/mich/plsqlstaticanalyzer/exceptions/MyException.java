package gr.mich.plsqlstaticanalyzer.exceptions;

/**
 * A Custom Exception interface for the sole purpose of throwing exceptions 
 * that do not crush the program or do not require try-catch statements
 * 
 * @author Michael Michailidis
 */
public interface MyException {
    /**
     * The message for the exception
     * @return Returns the message for the exception
     */
    public String getMessage();
}
