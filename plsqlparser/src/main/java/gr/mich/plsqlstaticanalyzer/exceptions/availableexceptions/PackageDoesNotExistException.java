package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when trying to access a package that doesn't exist
 * @author Michael Michailidis
 */
public class PackageDoesNotExistException extends AbstractException {    
    public PackageDoesNotExistException(String packageName) {
        super("The package " + packageName + " doesnt exist!");
    }
}
