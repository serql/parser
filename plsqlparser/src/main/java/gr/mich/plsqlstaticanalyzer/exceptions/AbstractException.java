package gr.mich.plsqlstaticanalyzer.exceptions;

/**
 *  The base model of a custom exception
 * 
 * @author Michael Michailidis
 */
public abstract class AbstractException implements MyException{
    private final String message;

    protected AbstractException(String message) {
        this.message = message;
    }

    @Override
    public String getMessage() {
        return message;
    }    
}
