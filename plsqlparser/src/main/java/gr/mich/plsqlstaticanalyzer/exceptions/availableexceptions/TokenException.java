package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when an unexpected token occur
 * 
 * @author Michael Michailidis
 */
public class TokenException extends AbstractException {

    public TokenException(String message) {
        super(message);
    }
    
}
