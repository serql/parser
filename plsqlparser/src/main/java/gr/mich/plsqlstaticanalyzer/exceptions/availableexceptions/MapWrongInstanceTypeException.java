package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;
import gr.mich.plsqlstaticanalyzer.output.function.OutputFunction;

/**
 * This exception should be thrown when the map which is trying to print 
 * is not readable by the {@link OutputFunction}
 * @author Michael Michailidis
 */
public class MapWrongInstanceTypeException  extends AbstractException{
    public MapWrongInstanceTypeException( ) {
        super("Output failed cause provided map class "
                + "was different than the one that the function accepts");
    }        
    
}
