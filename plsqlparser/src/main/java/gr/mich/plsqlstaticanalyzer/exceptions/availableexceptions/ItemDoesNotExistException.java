package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when an item doesn't exist
 * 
 * @author Michael Michailidis
 */
public class ItemDoesNotExistException extends AbstractException{
    public ItemDoesNotExistException(String itemName) {
        super("Item " + itemName + " does not exist");
    }    
}
