package gr.mich.plsqlstaticanalyzer.exceptions.availableexceptions;

import gr.mich.plsqlstaticanalyzer.exceptions.AbstractException;

/**
 * This exception should be thrown when an item has its default name. No item 
 * should have the default name to be valid
 * 
 * @author Michael Michailidis
 */
public class ItemNameIsDefaultValueException extends AbstractException {
    
    public ItemNameIsDefaultValueException(String sourceName, String sourcePackage, String sourceSchema) {
        super("Source name : " + sourceName + " Source Package : " 
                + sourcePackage + " SourceSchema : " + sourceSchema);
    }    
}
