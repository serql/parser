package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.helper.OtherRow;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 * @author Michael Michailidis
 */
public class AnalyzerPartForTable implements AnalyzerPart {

    @Override
    public void exec(UpgradedEnumMap<ItemField, String> me,
            HashMap<Long, CounterNode> targets) {
        String[] extraName = me.get(ItemField.EXTRA_NAME).split(",");
        String[] extraSchema = me.get(ItemField.EXTRA_SCHEMA_NAME).split(",");

        String otherName = me.get(ItemField.OTHER_ITEM_NAME);
        String otherSchema = me.get(ItemField.OTHER_SCHEMA_NAME);

        List<OtherRow> otherRows = new ArrayList<>();

        for (int i = 0; i < extraName.length; i++) {
            otherRows.add(
                    OtherRow.of(
                            extraName[i],
                            extraSchema[i]
                    )
            );
        }

        otherRows.add(OtherRow.of(otherName, otherSchema));

        targets.values().stream()
                .filter(vL
                        -> !vL.getItem()
                        .get(ItemField.ITEM_TYPE)
                        .equals(ItemType.CALL.toString())
                ).forEach(vL -> {
                    UpgradedEnumMap<ItemField, String> item = vL.getItem();
                    if (item.containsKey(ItemField.ITEM_NAME)//validate that the key exists before trying to extract it
                            && item.containsKey(ItemField.MY_SCHEMA_NAME)
                            && !item.containsKey(ItemField.MY_PACKAGE_NAME)//it should not have package. if the name matches but a package exist it may be something else 
                            && !item.containsKey(ItemField.LINK_NAME)) { //it should not have link name..
                        String myName = item.get(ItemField.ITEM_NAME);
                        String mySchema = item.get(ItemField.MY_SCHEMA_NAME);

                        otherRows.stream()
                                .forEach(row -> {
                                    if (myName.equals(row.getItemName())
                                            && mySchema.equals(row.getSchemaName())) {
                                        vL.increaseCalls();
                                    }
                                });
                    }
                });
    }

    @Override
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target) {
        return false;
    }  
}
