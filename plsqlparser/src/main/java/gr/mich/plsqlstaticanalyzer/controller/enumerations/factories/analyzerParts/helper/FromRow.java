package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.helper;

/**
 * Helper class for the {@link gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts.AnalyzerPart#exec execution} which needs 
 * a data holder class
 * 
 * @author KuroiKage
 */
public class FromRow {
    private final String itemName;
    private final String linkName;
    private final String schemaName;

    /**
     * This item is only useful if all the variables are filled.. 
     * So no default constructor
     * 
     * @param itemName The ItemName
     * @param schemaName The SchemaName
     * @param linkName The LinkName
     */
    public FromRow(String itemName, String schemaName, String linkName) {
        this.itemName = itemName;
        this.linkName = linkName;
        this.schemaName = schemaName;
    }

    /**
     * Constructor skip function to generate and get the item in one go!
     * @param itemName the ItemName
     * @param schemaName the SchemaName
     * @param linkName the LinkName
     * @return Return a new ItemHolder item
     */
    public static FromRow of (String itemName, String schemaName, String linkName) {
        return new FromRow(itemName, schemaName, linkName);
    }
    
    /**
     * The ItemName
     * @return Returns the ItemName
     */
    public String getItemName() {
        return itemName;
    }

    /**
     * The LinkName
     * @return Returns the LinkName
     */
    public String getLinkName() {
        return linkName;
    }

    /**
     * the SchemaName
     * @return Returns the SchemaName
     */
    public String getSchemaName() {
        return schemaName;
    }    
}
