package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class SynonymPartForSynonym implements SynonymPart{

    @Override
    public UpgradedEnumMap rename(UpgradedEnumMap otherValue, UpgradedEnumMap synonymValue) {
        String synonymItemName = synonymValue.get(ItemField.ITEM_NAME).toString();
        String synonymSchemaName = synonymValue.get(ItemField.MY_SCHEMA_NAME).toString();
        
        if (otherValue
                .get(ItemField.ITEM_TYPE)
                .toString()
                .equals(ItemType.SYNONYM.toString())) {
            
            if(synonymSchemaName.equals(ItemField.MY_SCHEMA_NAME.getDefault())) {
                synonymSchemaName = ItemField.OTHER_SCHEMA_NAME.getDefault();
            }
            
            String incomingOtherName = otherValue.get(ItemField.OTHER_ITEM_NAME).toString();
            String incomingOtherSchema = otherValue.get(ItemField.OTHER_SCHEMA_NAME).toString();
            
            if(synonymItemName.equals(incomingOtherName) && synonymSchemaName.equals(incomingOtherSchema)) {
                otherValue.put(ItemField.OTHER_ITEM_NAME, synonymValue.get(ItemField.OTHER_ITEM_NAME));
                otherValue.put(ItemField.OTHER_SCHEMA_NAME, synonymValue.get(ItemField.OTHER_SCHEMA_NAME));
                otherValue.put(ItemField.LINK_NAME, synonymValue.get(ItemField.LINK_NAME));
            }
        }
        
        return otherValue;
    }
    
}
