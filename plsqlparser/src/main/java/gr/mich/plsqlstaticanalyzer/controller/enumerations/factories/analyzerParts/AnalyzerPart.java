package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.HashMap;

/**
 * An interface to assist the {@link gr.mich.plsqlstaticanalyzer.analyzer.AnalysisCore AnalysisCore} to analyze the nodes
 * It makes the analysis part quite easy without complex code.
 * 
 * @author Michael Michailidis
 */
public interface AnalyzerPart {
    /**
     * Executes the analysis on a given node of the same type as the implementation and a 
     * HashMap of {@link CounterNode} 
     * @param me The node which will be the root of the analysis
     * @param targets The {@link CounterNode} on which the analysis will be  executed on
     */
    public void exec(UpgradedEnumMap<ItemField, String> me, HashMap<Long, CounterNode> targets);
    
    /**
     * Checks if the given node is being called by the target {@link AnalysisNode}
     * @param me the node which may be called
     * @param target the {@link AnalysisNode} that may call the node
     * @return Returns true only if the {@link AnalysisNode} calls the node. Else it 
     *         returns false.
     */
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target);
}
