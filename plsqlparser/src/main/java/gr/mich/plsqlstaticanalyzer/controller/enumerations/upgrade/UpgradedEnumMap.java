package gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade;

import java.util.EnumMap;
import java.util.Map;
import java.util.function.BiFunction;

/**
 *
 * @author Michael Michailidis
 * @param <K> The First type of the EnumMap - Key
 * @param <V> The second type of the EnumMap - Value
 */
public class UpgradedEnumMap<K extends Enum<K>, V extends Object> extends EnumMap<K,V>{
    
    private boolean shouldChange;
    
    
    public UpgradedEnumMap(Class type) {
        super(type);
        shouldChange = false;
    }

    public UpgradedEnumMap(EnumMap em) {
        super(em);
        shouldChange = false;
    }

    public UpgradedEnumMap(Map map) {
        super(map);
        shouldChange = false;
    }
    
    public void deActivateAdding() {
        shouldChange = true;
    }
    
    /**
     * 
     * @return Returns false if even one value is null
     */
    public boolean isValid(){
        for(V vLookUp:super.values()) {
            if (vLookUp == null) {
                return false;
            }
        }
        
        return true;
    }

    @Override
    public String toString() {
        String str = "{ \n";
        
        for (Map.Entry pair : entrySet()) {
            str = str + "\t" + pair.getKey() + " = " + pair.getValue() + ",\n";
        } 
        
        str = str + "}";
        
        return str;
    }
    
    public String toSerializedString(){
        String str = "";
        
        for (Map.Entry pair : entrySet()) {
            str = str + pair.getKey() + ":" + pair.getValue() + ",";
        }
        
        return str.substring(0, str.length() - 1);
    }
    

    @Override
    public void putAll(Map<? extends K, ? extends V> map) {
        if(shouldChange) {
            return;
        }
        
        super.putAll(map); 
    }

    @Override
    public V remove(Object o) {        
        if(shouldChange) {
            return null;
        }
        
        return super.remove(o); 
    }

    @Override
    public V put(K k, V v) {
        if(shouldChange && !super.containsKey(k)) {
            return null;
        }
        
        return super.put(k, v); 
    }

    @Override
    public V merge(K k, V v, BiFunction<? super V, ? super V, ? extends V> bf) {
        if(shouldChange) {
            return null;
        }
        
        return super.merge(k, v, bf); 
    }

    @Override
    public boolean remove(Object o, Object o1) {
        if(shouldChange) {
            return false;
        }
        
        return super.remove(o, o1); 
    }

    @Override
    public V putIfAbsent(K k, V v) {
        if(shouldChange) {
            return null;
        }
        
        return super.putIfAbsent(k, v); 
    }   
    
    public void updateList(UpgradedEnumMap v) {
        for(Object vLookUp:v.keySet()) {
            if (super.containsKey( (K)vLookUp ) ) {
                super.put((K)vLookUp, (V)v.get(vLookUp));
            }
        }
    }
}
