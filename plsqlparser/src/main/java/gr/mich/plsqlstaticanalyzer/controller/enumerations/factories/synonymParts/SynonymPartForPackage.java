package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class SynonymPartForPackage implements SynonymPart {

    @Override
    public UpgradedEnumMap rename(UpgradedEnumMap otherValue, UpgradedEnumMap synonymValue) {
        //getting the values on string variables for easier use
        String synonymOtherSchemaName = 
                synonymValue.get(ItemField.OTHER_SCHEMA_NAME).toString();
        
        String itemName = 
                synonymValue.get(ItemField.ITEM_NAME).toString();
        String schemaName = 
                synonymValue.get(ItemField.MY_SCHEMA_NAME).toString();
        
        String otherName = 
                otherValue.get(ItemField.ITEM_NAME).toString();
        String otherSchemaName = 
                otherValue.get(ItemField.MY_SCHEMA_NAME).toString();
        
        //checking if the name / schema are matching synonyms
        if( itemName.equals(otherName) && schemaName.equals(otherSchemaName) ) {
            //if they are matched they are replaced by the values that synonym 
            //points to
            otherValue.put(
                    ItemField.ITEM_NAME, 
                    synonymValue.get(ItemField.OTHER_ITEM_NAME)
            );
            
            if ( !synonymOtherSchemaName
                    .equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ||
                    !schemaName.equals(ItemField.MY_SCHEMA_NAME.getDefault())) {  
                otherValue.put(
                        ItemField.MY_SCHEMA_NAME, 
                        synonymOtherSchemaName
                );
            }
        }
        
        return otherValue;
    }
    
}