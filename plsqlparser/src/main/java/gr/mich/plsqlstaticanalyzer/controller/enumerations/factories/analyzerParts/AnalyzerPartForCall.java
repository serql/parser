package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.analyzerParts;

import gr.mich.plsqlstaticanalyzer.analyzer.inner.AnalysisNode;
import gr.mich.plsqlstaticanalyzer.analyzer.inner.CounterNode;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.HashMap;

/**
 *
 * @author Michael Michailidis
 */
public class AnalyzerPartForCall implements AnalyzerPart {

    @Override
    public void exec(final UpgradedEnumMap<ItemField, String> me,
            HashMap<Long, CounterNode> targets) {
        String otherName = me.get(ItemField.OTHER_ITEM_NAME);
        String otherPackage = me.get(ItemField.OTHER_PACKAGE_NAME);
        String otherSchema = me.get(ItemField.OTHER_SCHEMA_NAME);
        
        String myLink = me.get(ItemField.LINK_NAME);

        targets.values().stream().filter(vL
                -> !vL.getItem()
                .get(ItemField.ITEM_TYPE)
                .equals(ItemType.CALL.toString())
        ).forEach((vL) -> {
            UpgradedEnumMap<ItemField, String> item = vL.getItem();
            if (item.containsKey(ItemField.ITEM_NAME)
                    && item.containsKey(ItemField.MY_PACKAGE_NAME)
                    && item.containsKey(ItemField.MY_SCHEMA_NAME)) {
                String myName = item.get(ItemField.ITEM_NAME);
                String myPackage = item.get(ItemField.MY_PACKAGE_NAME);
                String mySchema = item.get(ItemField.MY_SCHEMA_NAME);
                
                if (myName.equals(otherName)
                        && myPackage.equals(otherPackage)
                        && mySchema.equals(otherSchema)) {
                    vL.increaseCalls();
                }                
            }
            
            if(item.get(ItemField.ITEM_TYPE).equals(ItemType.LINK.toString())) {                
                if(!myLink.equals(ItemField.LINK_NAME.getDefault()) 
                        && myLink.equals(item.get(ItemField.ITEM_NAME))) {
                    vL.increaseCalls();
                }
            }
            
        });
    }

    @Override
    public boolean inheritanceCheck(UpgradedEnumMap<ItemField, String> me, AnalysisNode target) {
        return false;
    }

}
