package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper.CheckListItem;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts.helper.Pair;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * An abstract implementation of the RenamePart that contains the basic methods
 * and their base code
 * Classes that extend this do not need to re-implement the below code. They only 
 * need to include a {@link CheckListItem} with the dependencies and the code 
 * below will handle the rest of the renaming
 * 
 * @author Michael Michailidis
 */
public abstract class AbstractRenamePart implements RenamePart {

    private UpgradedEnumMap map;
    private final HashMap<Integer, CheckListItem> checkList;

    public AbstractRenamePart() {  
        
        this.checkList = new HashMap<>();
    }

    protected final void addItemToList(CheckListItem item) {
        this.checkList.put(this.checkList.size(), item);
    }

    @Override
    public void setMap(UpgradedEnumMap map) {
        this.map = map;
    }

    @Override
    public UpgradedEnumMap getMap() {
        return map;
    }

    @Override
    public List<Pair> getFieldForUpdate() {
        List<Pair> tmp = new ArrayList<>();

        checkList.values().stream().filter((item) -> (item.isDependency()))
                .forEach((item) -> {
                    tmp.add(
                            new Pair(
                                    (String) map.get(item.getFieldForUpdate()),
                                    item.getFieldForUpdate(),
                                    item.hasFieldForMatchMask()
                                            ? (String) map.get(item.getFieldForMatchMask())
                                            : (String) map.get(item.getFieldForMatch()),
                                    item.getFieldForMatch()
                            )
                    );
                });

        return tmp;
    }

    @Override
    public void updateFields(List<Pair> fields) {
        
        for (Pair pair : fields) {
            boolean result;
            
            for (CheckListItem vL : checkList
                    .values()
                    .stream()
                    .filter(
                            (value)
                            -> !value.isDependency()
                    ).collect(
                            Collectors.toList()
                    )) {
                ItemField fieldForUpdate = pair.getFieldForUpdate();
                ItemField fieldForMatch = pair.getFieldForMatch();

                result = vL.hasFieldOfUpdateMask()
                        ? vL.getFieldForUpdateMask().equals(fieldForUpdate)
                        : vL.getFieldForUpdate().equals(fieldForUpdate);

                result = result && (vL.hasFieldForMatchMask()
                        ? vL.getFieldForMatchMask().equals(fieldForMatch)
                        : vL.getFieldForMatch().equals(fieldForMatch));

                result = result && (vL.hasFieldForMatchMask()
                        ? map.get(vL.getFieldForMatchMask()).toString().contains(pair.getFieldForMatchName())
                        : map.get(vL.getFieldForMatch()).toString().contains(pair.getFieldForMatchName()));

                if (result) {                    
                    String[] matcherFields = map.get(
                            vL.hasFieldForMatchMask()
                                    ? vL.getFieldForMatchMask()
                                    : vL.getFieldForMatch()
                    ).toString().split(",");

                    String[] updateFields = map.get(
                            vL.hasFieldOfUpdateMask()
                                    ? vL.getFieldForUpdateMask()
                                    : vL.getFieldForUpdate()
                    ).toString().split(",");

                                        
                    for (int i = 0; i < matcherFields.length; i++) {
                        if (matcherFields[i].equals(pair.getFieldForMatchName())) {                                
                            updateFields[i] = pair.getFieldForUpdateName();
                        }
                    }

                    String tmp = "";

                    for (String str : updateFields) {
                        tmp = tmp + str + ",";
                    }

                    tmp = tmp.substring(0, tmp.length() - 1);

                    map.replace(vL.getFieldForUpdate(), tmp);
                }
            }
        }

    }

    @Override
    public boolean idCheck(long id) {
        return Long.valueOf(this.map.get(ItemField.ID).toString()).equals(id);
    }

}
