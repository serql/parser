package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.renameParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;

/**
 *
 * @author Michael Michailidis
 */
public class RenamePartForCall extends AbstractRenamePart implements RenamePart {

    public RenamePartForCall() {
        addItemToList(
                // field that will change according if the input string match the "matcher" field
                ItemField.MY_SCHEMA_NAME.hasDependency(ItemField.MY_PACKAGE_NAME)
        );
        
        addItemToList(
                ItemField.OTHER_SCHEMA_NAME.hasDependency(ItemField.OTHER_PACKAGE_NAME, 
                        ItemField.MY_SCHEMA_NAME)
//                ItemField.OTHER_SCHEMA_NAME.hasDependency(ItemField.OTHER_PACKAGE_NAME, 
//                        ItemField.MY_SCHEMA_NAME, ItemField.MY_PACKAGE_NAME)
        );
    }

}
