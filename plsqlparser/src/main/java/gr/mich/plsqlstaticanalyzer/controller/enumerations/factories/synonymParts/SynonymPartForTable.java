package gr.mich.plsqlstaticanalyzer.controller.enumerations.factories.synonymParts;

import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
import gr.mich.plsqlstaticanalyzer.controller.enumerations.upgrade.UpgradedEnumMap;

/**
 *
 * @author Michael Michailidis
 */
public class SynonymPartForTable implements SynonymPart {

    @Override
    public UpgradedEnumMap rename(UpgradedEnumMap otherValue, UpgradedEnumMap synonymValue) {
        //getting the values on string variables for easier use
        String synonymOtherSchemaName
                = synonymValue.get(ItemField.OTHER_SCHEMA_NAME).toString();
        
        String itemName
                = synonymValue.get(ItemField.ITEM_NAME).toString();
        String schemaName
                = synonymValue.get(ItemField.MY_SCHEMA_NAME).toString();
        
        String otherName
                = otherValue.get(ItemField.ITEM_NAME).toString();
        String otherSchemaName
                = otherValue.get(ItemField.MY_SCHEMA_NAME).toString();

        //checking if the name / schema are matching synonyms
        if (itemName.equals(otherName) && schemaName.equals(otherSchemaName)) {
            //if they are matched they are replaced by the values that synonym 
            //points to
            otherValue.put(
                    ItemField.ITEM_NAME,
                    synonymValue.get(
                            ItemField.OTHER_ITEM_NAME
                    )
            );

            if ( !synonymOtherSchemaName
                    .equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ||
                    !schemaName.equals(ItemField.MY_SCHEMA_NAME.getDefault())) { 
                otherValue.put(
                        ItemField.MY_SCHEMA_NAME,
                        synonymOtherSchemaName
                );
            }
            
            return otherValue;
        }

        //then the schema default value is checked if needs to be changed again
        //to match the second possible outcome
        if (schemaName
                .equals(ItemField.MY_SCHEMA_NAME.getDefault())) {
            schemaName = ItemField.OTHER_SCHEMA_NAME.getDefault();
        }

        otherName
                = otherValue.get(ItemField.OTHER_ITEM_NAME).toString();
        otherSchemaName
                = otherValue.get(ItemField.OTHER_SCHEMA_NAME).toString();

        //checking if the name / schema of the other fields are matching synonyms
        if (itemName.equals(otherName) && schemaName.equals(otherSchemaName)) {
            otherValue.put(
                    ItemField.OTHER_ITEM_NAME,
                    synonymValue.get(
                            ItemField.OTHER_ITEM_NAME
                    )
            );

            if ( !synonymOtherSchemaName
                    .equals(ItemField.OTHER_SCHEMA_NAME.getDefault()) ||
                    !schemaName.equals(ItemField.OTHER_SCHEMA_NAME.getDefault())) { 
                otherValue.put(
                        ItemField.OTHER_SCHEMA_NAME,
                        synonymOtherSchemaName
                );
            }
            
            return otherValue;
        }

        //then the schema default value is checked if needs to be changed again
        //to match the second possible outcome
        if (schemaName
                .equals(ItemField.OTHER_SCHEMA_NAME.getDefault())) {
            schemaName = ItemField.EXTRA_SCHEMA_NAME.getDefault();
        }

        //getting the values of the other fields now
        otherName = otherValue.get(ItemField.EXTRA_NAME).toString();
        otherSchemaName = otherValue.get(ItemField.EXTRA_SCHEMA_NAME).toString();

        //setting a flag to check if any value matched and the item needs to be updated
        boolean flag = false;

        //splitting merged strings
        String[] itemNameSplitted = otherName.split(",");
        String[] schemaNameSplitted = otherSchemaName.split(",");

        // str.length and strSchema.lenght must be the same
        for (int i = 0; i < itemNameSplitted.length; i++) {
            // checking each element if its part of the synonym
            if (itemName.equals(itemNameSplitted[i]) && schemaName.equals(schemaNameSplitted[i])) {
                //for each "Hit" we replace the values
                itemNameSplitted[i] = synonymValue.get(ItemField.OTHER_ITEM_NAME).toString();
                
                //checking if the schema is the default value before trying to replace it
                if( !synonymOtherSchemaName
                        .equals( ItemField.OTHER_SCHEMA_NAME.getDefault() ) ||
                    !schemaName.equals(ItemField.EXTRA_SCHEMA_NAME.getDefault())) {
                    schemaNameSplitted[i] = synonymOtherSchemaName;
                }

                //updating the flag
                flag = true;
            }
        }

        // if the flag is true means a value was matched and the values must 
        // be updated in the item
        if (flag) {
            //initializing Strings to merge the tables
            String finishedItemName = "";
            String finishedSchemaName = "";

            //merging all 3 tables at the same time
            for (int j = 0; j < itemNameSplitted.length; j++) {
                finishedItemName = finishedItemName + itemNameSplitted[j] + ",";
                finishedSchemaName = finishedSchemaName + schemaNameSplitted[j] + ",";
            }

            // substring ( -1 ) to remove the extra comma from the end of the string
            otherValue.put(
                    ItemField.EXTRA_NAME,
                    finishedItemName.substring(0, finishedItemName.length() - 1)
            );

            otherValue.put(
                    ItemField.EXTRA_SCHEMA_NAME,
                    finishedSchemaName.substring(0, finishedSchemaName.length() - 1)
            );
        }
        return otherValue;
    }
    
}
