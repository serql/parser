package gr.mich.plsqlstaticanalyzer.output.config;

/**
 * The properties for the Gephi output.
 * 
 * @author Michael Michailidis
 */
public enum GephiProperties implements Properties,GephiExtraProperties{
    INSTANCE;

    @Override
    public boolean isHeaders() {
        return false;
    }

    @Override
    public String getFilePath() {
//        String userDesktop = System.getProperty("user.home");
//        userDesktop = userDesktop + "\\Desktop\\tei\\gephi.gexf";
//        return userDesktop;
        return "gephi.gexf";
    }

    @Override
    public String getDelimiter() {
        return ",";
    }
}
