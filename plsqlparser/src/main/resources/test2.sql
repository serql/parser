
CREATE OR REPLACE PACKAGE         repf_ede_dif
as
 
  type repf_dif_rec is record (
     src_id               number   (12),
     ref_id               number   (12),
     kathgoria            number   (5),
     aa                   varchar2 (40),
     metabolh             varchar2 (250),
     timh1                varchar2 (300),
     timh2                varchar2 (300),
     kodikos              varchar2 (100),
     table_name           varchar2 (50),
     column_name          varchar2 (50),
     stmt                 varchar2 (50),
     sqlsource            varchar2 (4000)
   );
   --
   type repf_dif_tbl is table of repf_dif_rec; 
   --
   --
   function repf_dif (i_src_id in number, i_ref_id in number)
      return repf_dif_tbl pipelined;
--
--   
end repf_ede_dif;
