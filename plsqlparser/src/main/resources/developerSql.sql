CREATE OR REPLACE PACKAGE schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS   
   PROCEDURE schema1.packageOne.procedureOne (deptno NUMBER) IS
   BEGIN
      schema2.packageTwo.procedureThree( 2 );
   END procedureOne;

END packageOne;

CREATE OR REPLACE PACKAGE schema2.packageTwo AS   
   PROCEDURE procedureThree (deptno NUMBER);
END packageTwo;

CREATE OR REPLACE PACKAGE BODY schema2.packageTwo AS   
   PROCEDURE schema2.packageTwo.procedureThree (deptno NUMBER) IS
   BEGIN
      schema1.packageOne.procedureOne( 2 );
   END procedureThree;
END packageTwo;
