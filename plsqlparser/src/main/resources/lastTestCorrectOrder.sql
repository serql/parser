CREATE OR REPLACE PACKAGE schema1.packageOne AS
   PROCEDURE procedureOne (id NUMBER);
   PROCEDURE procedureTwo (id NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS
   PROCEDURE procedureOne (id NUMBER) IS
   BEGIN
    packageTwo.functionOne( 2 );
    procedureTwo(3);
    packageTwo.procedureThree(1);
    packageTwo.procedureThree(1);
   END procedureOne;

   PROCEDURE procedureTwo (id NUMBER) IS
   BEGIN
     procedureOne(2);
     procedureOne(2);
     procedureOne(2);
     packageTwo.procedureThree(2);
   END procedureTwo;
END packageOne;

CREATE OR REPLACE PACKAGE schema1.packageTwo AS
  FUNCTION functionOne (id NUMBER) RETURN NUMBER;
  PROCEDURE procedureThree( id NUMBER);
END packageTwo;

CREATE OR REPLACE PACKAGE BODY schema1.packageTwo AS
  FUNCTION functionOne (id number) RETURN NUMBER AS  total NUMBER;
     BEGIN
      total:=id - 1;
      RETURN total;
  END functionOne;
  PROCEDURE procedureThree(id NUMBER) IS
  BEGIN
    packageOne.procedureOne(2);
    packageOne.procedureOne(2);
    packageOne.procedureTwo(3);
  END procedureThree;
END packageTwo;

CREATE OR REPLACE PACKAGE schema2.packageThree AS
   PROCEDURE procedureFour (id NUMBER);
   PROCEDURE procedureFive (id NUMBER);
END packageThree;

CREATE OR REPLACE PACKAGE BODY schema2.packageThree AS
   PROCEDURE procedureFour (id NUMBER) IS
   BEGIN
    packageFour.procedureSix( 2 );
    procedureFive(3);
    packageFour.procedureSeven(1);
    packageFour.procedureSeven(1);
   END procedureFour;

   PROCEDURE procedureFive (id NUMBER) IS
   BEGIN
     procedureFour(2);
     procedureFour(2);
     procedureFour(2);
     packageFour.procedureSix(2);
   END procedureFive;
END packageThree;

CREATE OR REPLACE PACKAGE schema2.packageFour AS
  PROCEDURE procedureSix (id NUMBER);
  PROCEDURE procedureSeven ( id NUMBER);
END packageFour;

CREATE OR REPLACE PACKAGE BODY schema2.packageFour AS
  PROCEDURE procedureSix(id NUMBER) IS
  BEGIN
    packageThree.procedureFour(2);
    packageThree.procedureFour(2);
    packageThree.procedureFive(3);
    procedureSeven(3);
  END procedureSix;
  
  PROCEDURE procedureSeven(id NUMBER) IS
  BEGIN
    packageThree.procedureFour(2);
    packageThree.procedureFour(2);
    packageThree.procedureFive(3);
  END procedureSeven;
END packageFour;

CREATE OR REPLACE PACKAGE packageFive AS
  PROCEDURE packageUniter (id NUMBER);
END packageFour;

CREATE OR REPLACE PACKAGE BODY schema3.packageFive AS
PROCEDURE packageUniter(id NUMBER) IS
BEGIN
	schema1.packageOne.procedureOne(2);
	schema2.packageThree.procedureFive(3);
END packageUniter
END packageFive
