--Granting Object Privileges to a Role
GRANT SELECT ON sh.sales TO warehouse_user;
--Granting a Role to a Role
GRANT warehouse_user TO dw_manager; 
--Granting an Object Privilege on a Directory
GRANT READ ON DIRECTORY bfile_dir TO hr
   WITH GRANT OPTION;
--Granting Object Privileges on a Table to a User
GRANT ALL ON bonuses TO hr 
   WITH GRANT OPTION; 
--Granting Object Privileges on a View
GRANT SELECT, UPDATE 
   ON emp_view TO PUBLIC; 
--Granting Object Privileges to a Sequence in Another Schema 
GRANT SELECT 
   ON oe.customers_seq TO hr; 
--Granting Multiple Object Privileges on Individual Columns 
GRANT REFERENCES (employee_id), 
      UPDATE (employee_id, salary, commission_pct) 
   ON hr.employees
   TO oe; 