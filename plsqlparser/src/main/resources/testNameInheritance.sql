CREATE OR REPLACE PACKAGE packageOne AS
   PROCEDURE procedureOne (deptno NUMBER);
   PROCEDURE procedureTwo (emp_id NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      sch2.pck2.proc2( 2 );
      randomPackage.proc3( 2 );
      procedureTwo( 2 );
   END procedureOne;

   PROCEDURE procedureTwo (emp_id NUMBER) IS
   BEGIN
  procedureOne( 2 );
     sch2.pck2.proc2( 2 );
     testSchema.procedureFour( 3 );
   END procedureTwo;
END packageOne;

CREATE OR REPLACE PACKAGE BODY testSchema.packageThree AS
   PROCEDURE procedureFour (deptno NUMBER) IS
   BEGIN
      packageTwo.procedureThree( 2 );
      other_package.rehire_empoyees( 3 );
      packageOne.procedureOne( 3 );
   END procedureThree;
   FUNCTION functtwo (first number, second number, third number ) RETURN number AS  total     number;
      BEGIN
       total:=first + second + third - 1;
       RETURN total;
     END functtwo;
END packageThree;
