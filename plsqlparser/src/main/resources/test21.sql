CREATE OR REPLACE PACKAGE packageOne AS  -- spec
   PROCEDURE procedureOne (deptno NUMBER);
   PROCEDURE procedureTwo (emp_id NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS  -- body
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      procedureTwo( 2 ); --new row
	  --other_package.rehire_empoyees( 3 );
      procedureTwo( 3 ); --new row
      packageFour.procedureFive( 2 ); --new row
      packageFour.procedureFive( 2 ); --new row
      packageFour.procedureFive( 2 ); --new row
      packageFour.procedureFive( 2 ); --new row
   END procedureOne;

   PROCEDURE procedureTwo (emp_id NUMBER) IS
   BEGIN
      packageThree.procedureFour( 2 ); --new row
	  procedureOne( 2 );
   END procedureTwo;
END packageOne;

CREATE OR REPLACE PACKAGE packageTwo AS  -- spec
   PROCEDURE procedureThree (deptno NUMBER);
END packageTwo;

CREATE OR REPLACE PACKAGE BODY packageTwo AS  -- body
   PROCEDURE procedureThree (deptno NUMBER) IS
   BEGIN
      packageOne.procedureTwo( 2 ); --new row
	  --other_package.rehire_empoyees( 3 );
      packageOne.procedureOne( 3 ); --new row
   END procedureThree;
END packageTwo;

CREATE OR REPLACE PACKAGE packageThree AS  -- spec
   PROCEDURE procedureFour (deptno NUMBER);
END packageThree;

CREATE OR REPLACE PACKAGE BODY packageThree AS  -- body
   PROCEDURE procedureFour (deptno NUMBER) IS
   BEGIN
      packageTwo.procedureThree( 2 ); --new row
	  --other_package.rehire_empoyees( 3 );
      packageOne.procedureOne( 3 ); --new row
   END procedureThree;
END packageThree;


CREATE OR REPLACE PACKAGE packageFour AS  -- spec
   PROCEDURE procedureFive (deptno NUMBER);
END packageFour;

CREATE OR REPLACE PACKAGE BODY packageFour AS  -- body
   PROCEDURE procedureFive (deptno NUMBER) IS
   BEGIN
      packageTwo.procedureThree( 2 ); --new row
	  --other_package.rehire_empoyees( 3 );
      packageThree.procedureFour( 3 ); --new row	  
      packageThree.procedureFour( 3 ); --new row  
      packageThree.procedureFour( 3 ); --new row	  
	  
      packageOne.procedureOne( 3 ); --new row
   END procedureFive;
END packageFour;

