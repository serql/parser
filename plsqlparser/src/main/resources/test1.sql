
CREATE OR REPLACE package          cheapiceert0
as
   -- table_name
    rt6lockflag      number (1) := 0;
    deleteflag      number (1) := 0;  
   ws_cee               varchar2 (30);
   ws_ceg               varchar2 (30);
   ws_ceert             varchar2 (30);
   ws_cef               varchar2 (30);
   ws_chp15             varchar2 (30);
   -- dblink
   ws_dblink            varchar2 (31);
   -- host_name
   ws_host_name         varchar2 (4000);
   type idsrectyp is record (
      table_id_rt0   number                                    --table id rt0
   );
   type idstabtyp is table of idsrectyp
      index by binary_integer;
   ids_table            idstabtyp;
   -- Ο πακάτω πίνακας δημιουργείται για την αρχικοποίηση των πινάκων που αντιγράφονται
   type tabrectyp is record (
      table_owner           varchar (30),                       --table owner
      table_name            varchar (30),                        --table name
      sequence_owner        varchar (30),                    --sequence owner
      sequence_name         varchar (30), --sequence name to getvalues for ID
      table_sequence        number,          --sequence in insert statements,
      table_index           number (5),                      -- record rownum
      table_id              number,                             --primary key
      default_values_type   number (2),
      ids_rt0               idstabtyp
   );
   type tabtabtyp is table of tabrectyp
      index by varchar2 (10);
   cee_tables           tabtabtyp;
   type tabrectypindx is record (
      table_owner           varchar (30),
      table_name            varchar (30),
      table_name_short      varchar (10),
      table_sequence        number            --sequence in insert statements
                                  ,
      default_values_type   number (2)
   );
   type tabtabtypindx is table of tabrectypindx
      index by binary_integer;
   cee_tables_indexed   tabtabtypindx;
   -- cee
   getcee_rec           chetepielehd%rowtype;
   type t_getcee is table of getcee_rec%rowtype
      index by binary_integer;
   getcee_array         t_getcee;
   -- ceg
   getceg_rec           chetepieleagroi%rowtype;
   type t_getceg is table of getceg_rec%rowtype
      index by binary_integer;
   getceg_array         t_getceg;
   --cef
   getcef_rec           chetepielefytiko%rowtype;
   type t_getcef is table of getcef_rec%rowtype
      index by binary_integer;
   getcef_array         t_getcef;
   getchp15_rec         crnt2015.chetepieleparcels%rowtype;
   type t_getchp15 is table of getchp15_rec%rowtype
      index by binary_integer;
   getchp15_array       t_getchp15;
   procedure init_tables (i_table_type in number default 0);
   --
   procedure set_values_to_default (i_table_name_short in varchar2);
   --
   procedure set_values_to_default;
   --
   procedure delete_cee2015 (i_cee_id in number);
   --
   procedure insert_2_db_tables (i_table_name_short in varchar2);
   --
   procedure insert_2_db_tables;
   --
   procedure copy_cee_rt1_2_rt0 (
      i_cee_id        in       number,
      i_gusr_id       in       number,
      i_bathmostype   in       number,
      i_epiloghtype   in       number,
      o_cee_id        out      number,
      i_rt0_flag      in       number default 1
   );
   --

   --
   function create_cee_rt0 (
      i_cee_id        in   number,
      i_gusr_id       in   number,
      i_bathmostype   in   number,
      i_epiloghtype   in   number,
      i_rt0_flag      in   number default 1
   )
      return number;
   function insertchetepielehd (
      i_afm         in       chetepielehd.afm%type,
      i_gusr_id     in       chetepielehd.gusr_id%type default 3040,
      i_leveltype   in       chetepielehd.leveltype%type default 2,
      i_cosr_id in number default null,
      i_inputflag            number default 0,
      o_cee_id      out      number
   )
      return number;
   procedure updektashkl (i_ceg_id in number);
   function upddelplusepielehd_f (i_id in chetepielehd.id%type)
      return number;
   function getlockedobjectinfo (i_object_name in varchar2)
      return varchar2;
   function insertchetepielehd_f (
      i_afm         in   chetepielehd.afm%type,
      i_gusr_id     in   chetepielehd.gusr_id%type,
      i_leveltype   in   chetepielehd.leveltype%type,
       i_cosr_id  in chetepielezhd.cosr_id%type default null
   )
      return number;
   function validateafm (i_afm in varchar2)
      return boolean;
   function validateceeafmprev_g_f (
      i_afm         in   chetepielehd.afm%type,
      i_leveltype   in   chetepielehd.leveltype%type,
      i_gusr_id     in   chetepielehd.gusr_id%type
   )
      return number;
--
   function getceecheckflagsum (i_id in chetepielehd.id%type)
      return number;
  procedure insertchetepielehdthlrt1 (
      i_afm         in       chetepielehd.afm%type,
      i_gusr_id     in       chetepielehd.gusr_id%type default 3040,
      i_leveltype   in       chetepielehd.leveltype%type default 1  --1 - τηλεπισκοπηση,3 - μετα απο τηλεπισκοπηση
   );
  function getepilektahsmet (
      i_ceg_id        in   number
   )
      return number;
     function getepilektahsmetall (
      i_ceg_id        in   number
   )
      return number;
       FUNCTION check_epilektashmet_episporh ( i_ceg_id  IN NUMBER)
      RETURN NUMBER;
      
         function insertchetepielehdthl_v1 (
      i_afm         in       chetepielehd.afm%type,
      i_gusr_id     in       chetepielehd.gusr_id%type default 3040,
      i_leveltype   in       chetepielehd.leveltype%type default 1,
      i_cosr_id in number default null,
      i_inputflag            number default 0,
      o_cee_id      out      number
   )
      return number;
end;

