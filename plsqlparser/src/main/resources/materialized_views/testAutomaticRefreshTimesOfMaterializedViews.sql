CREATE MATERIALIZED VIEW all_customers
   PCTFREE 5 PCTUSED 60 
   TABLESPACE example 
   STORAGE (INITIAL 50K NEXT 50K) --check this
   USING INDEX STORAGE (INITIAL 25K NEXT 25K) --wrong row as it seems
   REFRESH START WITH ROUND(SYSDATE + 1) + 11/24 --ROUND(SYSDATE + 1) + 11/24 = date :/
   NEXT NEXT_DAY(TRUNC(SYSDATE), 'MONDAY') + 15/24 --NEXT_DAY(TRUNC(SYSDATE), 'MONDAY') + 15/24  date again
   AS SELECT * FROM sh.customers@remote 
         UNION
      SELECT * FROM sh.customers@local; 