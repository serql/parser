CREATE TRIGGER schema1.trigger1 BEFORE 
DELETE ON schema2.view2 FOR EACH ROW
FOLLOWS schema3.trigger2, schema4.trigger3, trigger4
DECLARE
    sal_diff number;
BEGIN
    sal_diff  := :new.sal  - :old.sal;
    dbms_output.put('Old salary: ' || :old.sal);
    dbms_output.put('  New salary: ' || :new.sal);
    dbms_output.put_line('  Difference ' || sal_diff);
END;