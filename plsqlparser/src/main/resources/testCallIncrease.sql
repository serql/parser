CREATE OR REPLACE PACKAGE packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER);
   PROCEDURE procedureTwo (emp_id NUMBER);
END packageOne;

CREATE OR REPLACE PACKAGE BODY schema1.packageOne AS   
   PROCEDURE procedureOne (deptno NUMBER) IS
   BEGIN
      procedureTwo( 2 );  
      procedureTwo( 2 );  
      procedureTwo( 2 );  
      procedureTwo( 2 );  
      procedureTwo( 2 );  
   END procedureOne;

   PROCEDURE procedureTwo (emp_id NUMBER) IS
   BEGIN
      procedureOne( 2 );  
   END procedureTwo;
END packageOne;
