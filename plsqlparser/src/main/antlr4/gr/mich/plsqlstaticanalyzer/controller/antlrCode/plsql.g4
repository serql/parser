/**
 * Oracle(c) PL/SQL 11g Parser
 *
 * Copyright (c) 2009-2011 Alexandre Porcelli <alexandre.porcelli@gmail.com>
 * Copyright (c) 2015 Ivan Kochurkin (KvanTTT, kvanttt@gmail.com).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
grammar plsql;
options {language=Java;}

@header {
  import gr.mich.plsqlstaticanalyzer.controller.ControlCore;
  import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemField;
  import gr.mich.plsqlstaticanalyzer.controller.enumerations.source.ItemType;
}

@members {
  ControlCore controlCore  = new ControlCore();

  public ControlCore getCore() {
    return controlCore;
  }
}
//New Grammar Update File

swallow_to_semi
    : ~( ';' )+
    ;

compilation_unit
    : unit_statement* EOF
    ;

sql_script
    : (unit_statement | sql_plus_command)* EOF
    ;

unit_statement
    : ( alter_function
    | alter_package
    | alter_procedure
    | alter_sequence
    | alter_trigger
    | alter_type
    //| alter_table //TODO https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_3001.htm

    | create_function_body
    | create_procedure_body
    | create_package

    | create_index 
    | create_table
    | create_view
    | create_directory
    | create_materialized_view
    | create_synonym
   //| create_schema //cannot find it on 11g
   //| create_user https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_8003.htm#i2065278

    | create_sequence
    | create_trigger
    | create_type

    | drop_function
    | drop_package
    | drop_procedure
    | drop_sequence
    | drop_trigger
    | drop_type
    | drop_directory
    | data_manipulation_language_statements

    | grant //all tests pass
    ) ';'?
    ;

// UPDATE start -> working on Create Table
//Changes outside update area should have //NEW ADDITION to find them

//small change for commit
create_table
    : CREATE {
              controlCore.factory(ItemType.TABLE);
              controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
             }( GLOBAL TEMPORARY )? TABLE (schema_name {
                                                        controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);
                                                       } '.')? table_name {
                                                                           controlCore.factory(ItemField.ITEM_NAME, $table_name.text);
                                                                          }
      ( relational_table | object_table | xmltype_table) { controlCore.finalizeV(); }
    ;

// table_properties -> https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm#i2126725
// removed ? from table_properties so it will stop throwing null block error
// semicolon changed to optional so the create_table will work as statement also
relational_table
    : ( '(' relational_properties ')' )? (ON COMMIT (DELETE|PRESERVE) ROWS)?
      {
       controlCore.factory(ItemField.OTHER_ITEM_NAME,ItemField.OTHER_ITEM_NAME.getDefault());
       controlCore.factory(ItemField.OTHER_SCHEMA_NAME,ItemField.OTHER_SCHEMA_NAME.getDefault());
      }
      (physical_properties)? table_properties_workaround* ';'?
    ;

//reversed rule order. check testTableMultiConstrain. the check constrain was matching
//column definition equally to out_of_line_Constraint
relational_properties
    : ( ( out_of_line_constraint
        | out_of_line_ref_constraint
        | supplemental_logging_props
        )
      | column_definition
      | virtual_column_definition
      )
      (','
       (  ( out_of_line_constraint
          | out_of_line_ref_constraint
          | supplemental_logging_props
          )
        | column_definition
        | virtual_column_definition
        )
      )*
    ;

physical_properties
    : ( deferred_segment_creation? segment_attributes_clause (table_compression)?
      | deferred_segment_creation? ORGANIZATION ( HEAP (segment_attributes_clause)? (table_compression)?
                                                | INDEX (segment_attributes_clause | index_org_table_clause )+
                                                | EXTERNAL external_table_clause
                                                )
      | CLUSTER cluster '(' column (',' column)* ')'
      )
    ;
deferred_segment_creation
    : SEGMENT CREATION (IMMEDIATE | DEFERRED)
    ;
/*
//its optional rule..
//if error occurs try to remove the ? from the rule calling this one
table_properties
    : column_properties? table_partitioning_clauses? ( CACHE | NOCACHE )?
      parallel_clause? ( ROWDEPENDENCIES | NOROWDEPENDENCIES )? enable_disable_clause*
      row_movement_clause? flashback_arcive_clause? (AS subquery)?
    ;*/

table_properties_workaround
    : ( column_properties
      | table_partitioning_clauses
      | CACHE
      | NOCACHE
      | parallel_clause
      | ROWDEPENDENCIES
      | NOROWDEPENDENCIES
      | enable_disable_clause //*
      | row_movement_clause
      | flashback_arcive_clause
      | AS subquery
      )
    ;

column_properties
    : ( obj_type_col_properties
      | nested_table_col_properties
      | (varray_col_properties | lob_storage_clause) ( '(' lob_partition_storage (',' lob_partition_storage)* ')' )?
      | xmltype_column_properties
      )+
    ;

nested_table_col_properties
    : NESTED TABLE ( nested_item | COLUMN_VALUE ) substitutable_column_clause? STORE AS storage_table
      ( '(' ('(' object_properties ')'| physical_properties | column_properties )+ ')' )?
      (RETURN AS (LOCATOR|VALUE))?
    ;

enable_disable_clause
    : (ENABLE|DISABLE) (VALIDATE|NOVALIDATE)? ( UNIQUE '('column (',' column )* ')'
                                              | PRIMARY KEY
                                              | CONSTRAINT constraint_name
                                              )
      using_index_clause? exception_clause? CASCADE? ((KEEP|DROP) INDEX)?
     ;

table_partitioning_clauses
    : ( range_partitions
      | hash_partitions
      | list_partitions
      | reference_partitions
      | composite_range_partitions
      | composite_list_partitions
      | system_partitioning
      )
    ;

range_partitions
    : PARTITION BY RANGE '(' column (',' column )* ')'
      (INTERVAL expr (STORE IN '(' tablespace_name (',' tablespace_name )* ')' )? )?
      '(' PARTITION partition_name? range_values_clause table_partition_description
      (','PARTITION partition_name? range_values_clause table_partition_description  )* ')'
    ;

range_values_clause
    : VALUES LESS THAN '(' ( literal | MAXVALUE ) (','( literal | MAXVALUE ))* ')' //instead of literal
    ;

list_values_clause
    : VALUES '(' ((literal|NULL) (','(literal|NULL))*|DEFAULT)')'
    ;

//optional rule.. again..
table_partition_description
    : segment_attributes_clause? (table_compression|key_compression)?
      (OVERFLOW segment_attributes_clause? )? (lob_storage_clause|varray_col_properties)*
    ;

hash_partitions
    :  PARTITION BY HASH '(' column (',' column )* ')' (individual_hash_partitions|hash_partitions_by_quantity)
    ;

list_partitions
    : PARTITION BY LIST '(' column ')'
      '(' PARTITION partition_name? list_values_clause table_partition_description
      (','PARTITION partition_name? list_values_clause table_partition_description)* ')'
    ;

reference_partitions
    : PARTITION BY REFERENCE '(' constraint_name ')' ('(' reference_partition_desc (','reference_partition_desc)* ')')?
    ;

composite_range_partitions
    : PARTITION BY RANGE '(' column (',' column )* ')'
      (INTERVAL '(' expr ')' (STORE IN '('  tablespace_name (',' tablespace_name)* ')')? )?
      (subpartition_by_range|subpartition_by_list|subpartition_by_hash)
       '(' PARTITION partition_name? range_partition_desc
      (',' PARTITION partition_name? range_partition_desc)* ')'
    ;

composite_list_partitions
    : PARTITION BY LIST '(' column (',' column )* ')'
     (subpartition_by_range|subpartition_by_list|subpartition_by_hash)
       '(' PARTITION partition_name? list_partition_desc
      (',' PARTITION partition_name? list_partition_desc)* ')'
    ;

system_partitioning
    : PARTITION BY SYSTEM (PARTITIONS UNSIGNED_INTEGER | reference_partition_desc (','reference_partition_desc)* )? //instead of integer
    ;

range_partition_desc
    : range_values_clause table_partition_description
      (
      '('
            ( range_subpartition_desc (',' range_subpartition_desc)*
            | list_subpartition_desc (',' list_subpartition_desc)*
            | individual_hash_subparts (',' individual_hash_subparts)*
            )
      ')'
      |hash_subparts_by_quantily)?
    ;

list_partition_desc
    : list_values_clause table_partition_description
      (
      '('
            ( range_subpartition_desc (',' range_subpartition_desc)*
            | list_subpartition_desc (',' list_subpartition_desc)*
            | individual_hash_subparts (',' individual_hash_subparts)*
            )
      ')'
      |hash_subparts_by_quantily)?
    ;

subpartition_template
    : SUBPARTITION TEMPLATE (
      '('
            ( range_subpartition_desc (',' range_subpartition_desc)*
            | list_subpartition_desc (',' list_subpartition_desc)*
            | individual_hash_subparts (',' individual_hash_subparts)*
            )
      ')'
      |hash_subparts_by_quantily)
    ;

subpartition_by_range
    : SUBPARTITION BY RANGE '(' column (',' column )* ')' subpartition_template?
    ;

subpartition_by_list
    : SUBPARTITION BY LIST '(' column ')'  subpartition_template?
    ;

subpartition_by_hash
    : SUBPARTITION BY HASH '(' column (',' column )* ')'
      (SUBPARTITIONS UNSIGNED_INTEGER (STORE IN '(' tablespace_name (','tablespace_name)* ')' )? //instead of integer
      |subpartition_template)?
    ;

range_subpartition_desc
    : SUBPARTITION subpartiton_name? range_values_clause partitioning_storage_clause?
    ;

list_subpartition_desc
    : SUBPARTITION subpartiton_name? list_values_clause partitioning_storage_clause?
    ;

individual_hash_subparts
    : SUBPARTITION subpartition_name? partitioning_storage_clause?
    ;

hash_subparts_by_quantily
    : SUBPARTITIONS UNSIGNED_INTEGER (STORE IN '(' tablespace_name (',' tablespace_name )* ')')? //instead of integer
    ;

//removed ? from table_partition_description because of empty block
reference_partition_desc
    : PARTITION partition_name? table_partition_description
    ;
xmltype_column_properties
    : XMLTYPE COLUMN? column xmltype_storage? xmlschema_spec?
    ;

xmltype_storage
    : STORE AS ( OBJECT RELATIONAL | (BASICFILE|SECUREFILE)? (CLOB|BINARY XML)
               ( lob_segname ( '(' lob_parameters ')' )?| '(' lob_parameters ')' )?
               )
    ;

xmltype_virtual_columns
    : VIRTUAL COLUMNS '(' column AS '(' expr ')' (',' column AS '(' expr ')')* ')'
    ;

xmlschema_spec
    : ( single_xmlschema_spec | multiple_xmlschema_spec )
      (ALLOW (ANYSCHEMA|NONSCHEMA)|DISALLOW NONSCHEMA)?
    ;

single_xmlschema_spec
    : ( XMLSCHEMA xmlschema_url )? ELEMENT (element_spec| xmlschema_url '#' element) //not really sure about the element
    ;

multiple_xmlschema_spec
    : XMLSCHEMAS '(' single_xmlschema_spec (',' single_xmlschema_spec)* ')'
    ;

row_movement_clause
    : (ENABLE | DISABLE) ROW MOVEMENT
    ;

flashback_arcive_clause
    : ( FLASHBACK ARCHIVE flashback_arcive? | NO FLASHBACK ARCHIVE)
    ;

obj_type_col_properties
    : COLUMN column substitutable_column_clause
    ;

substitutable_column_clause
    : ( ELEMENT? IS OF TYPE? '(' ONLY type ')'
      | NOT? SUBSTITUTABLE AT ALL LEVELS
      )
    ;

// https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm#i2126768
// changed it from the documentation to 1 or more. works fine now.
object_properties
    : ( (column|attribute_name) (DEFAULT expr)?  (inline_constraint+ |inline_ref_constrain)?
      | ( out_of_line_constraint
        | out_of_line_ref_constraint
        | supplemental_logging_props
        )
      )
      (
      ','
        ( (column|attribute_name) (DEFAULT expr)?  (inline_constraint+ |inline_ref_constrain)?
        | ( out_of_line_constraint
          | out_of_line_ref_constraint
          | supplemental_logging_props
          )
        )
      )*
    ;

varray_col_properties
    : VARRAY varray_item
      ( substitutable_column_clause? varray_storage_clause
      |substitutable_column_clause
      )
    ;

varray_storage_clause
    : STORE AS ( SECUREFILE | BASICFILE ) LOB (lob_segname? '(' lob_storage_parameters ')'| lob_segname)
    ;

lob_storage_clause
    : LOB  ( '(' lob_item (',' lob_item)* ')' STORE AS ( ( SECUREFILE | BASICFILE ) | ( '(' lob_storage_parameters ')' ) )+
           | '(' lob_item ')' STORE AS ( ( SECUREFILE | BASICFILE ) | lob_segname | ( '(' lob_storage_parameters ')' ) )+
           )
    ;

lob_storage_parameters
    : (
        ( TABLESPACE tablespace_name
        | lob_parameters storage_clause?
        )+
      | storage_clause
      )
    ;


lob_parameters
    : ( (ENABLE|DISABLE) STORAGE IN ROW
      | CHUNK UNSIGNED_INTEGER //instead of integer
      | PCTVERSION UNSIGNED_INTEGER //instead of integer
      | FREEPOOLS UNSIGNED_INTEGER //instead of integer
      | lob_retention_clause
      | lob_dedublicate_clause
      | lob_compression_clause
      | (ENCRYPT encrypt_spec| DECRYPT )
      | ( CACHE | (NOCACHE|CACHE READS) logging_clause?)
      )+
    ;

lob_retention_clause
    : RETENTION (MAX|MIN UNSIGNED_INTEGER |AUTO|NONE)? //instead of integer
    ;

lob_dedublicate_clause
    : ( DEDUBLICATE | KEEP_DUBLICATES )
    ;

lob_compression_clause
    : ( COMPRESS (HIGH|MEDIUM)? | NOCOMPRESS )
    ;

lob_partition_storage
    : PARTITION partition_name (lob_storage_clause|varray_col_properties)+
      ( '(' SUBPARTITION subpartition_name ( lob_partitioning_storage | varray_col_properties)+ ')')?
    ;

lob_partitioning_storage
    : LOG '(' lob_item ')' STORE AS (SECUREFILE|BASICFILE)?
      ( lob_segname ('(' TABLESPACE tablespace_name ')')? |'(' TABLESPACE tablespace_name ')' )?
    ;

//https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm#i2129649
external_table_clause
    : '(' (TYPE access_driver_type)? external_data_properties ')' (REJECT LIMIT (UNSIGNED_INTEGER |UNLIMITED))? //instead of integer
    ;

//subquery is correct?
//https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm#i2129649
external_data_properties
    : DEFAULT DIRECTORY directory (ACCESS PARAMETERS ('(' opaque_format_spec ')' | USING CLOB subquery))?
      LOCATION '(' (directory':')? '\'' location_specifier '\''
              (',' (directory':')? '\'' location_specifier '\'')* ')'
    ;

table_compression
    : (COMPRESS (FOR (ALL | DIRECT_LOAD) OPERATIONS )? | NOCOMPRESS)
    ;

// test datatype if its the one i want
// https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm
// NEW ADDITION expression <-> expr
column_definition
    : column ( datatype | user_defined_datatype ) SORT? (DEFAULT expression)? (ENCRYPT encrypt_spec)?
      ( inline_constraint+ | inline_ref_constrain )?
    ;


//again optional rule.. check this if an error occurs
index_org_table_clause
    : ( mapping_table_clause
      | PCTTHRESHOLD UNSIGNED_INTEGER //instead of integer
      | key_compression
      | index_org_overflow_clause
      )
    ;

index_org_overflow_clause
    : (INCLUDING column)? OVERFLOW (segment_attributes_clause)?
    ;

mapping_table_clause
    : ( MAPPING TABLE | NOMAPPING )
    ;

//the whole rule is optional. it may cause problem
encrypt_spec
    : (USING '\'' encrypt_algorithm '\'')? (IDENTIFIED BY password)? (NO? SALT)?
    ;

//again datatype.
virtual_column_definition
    : column ((datatype|user_defined_datatype))? (GENERATED ALWAYS)? AS '(' column_expression ')'
      VIRTUAL? (inline_constraint)*
    ;

inline_ref_constrain
    : ( SCOPE IS (schema_name '.')? scope_table
      | WITH ROWID
      | (CONSTRAINT constraint_name)? references_clause constraint_state?
      )
    ;

out_of_line_ref_constraint
    : ( SCOPE FOR '(' ( ref_col | ref_attr ) ')' IS (schema_name '.')? scope_table
      | REF '(' ( ref_col | ref_attr ) ')' WITH ROWID
      | (CONSTRAINT constraint_name)? FOREIGN KEY '(' ( ref_col | ref_attr ) ')' references_clause constraint_state?
      )
    ;

supplemental_logging_props
    : SUPPLEMENTAL LOG ( supplemental_log_grp_clause | supplementa_id_key_clause )
    ;

supplementa_id_key_clause
    : DATA '(' (ALL | PRIMARY KEY | UNIQUE | FOREIGN KEY ) (',' (ALL | PRIMARY KEY | UNIQUE | FOREIGN KEY) )* ')' COLUMNS
    ;

supplemental_log_grp_clause
    : GROUP log_group '(' (column (NO LOG)?) (','column (NO LOG)?)* ')' ALWAYS?
    ;

//removed ? from table_properties to prevent empty block warning
//semicolon changed to optional so the create_table will work as statement also
object_table
    : OF {
          controlCore.factory(ItemField.OTHER_SCHEMA_NAME, ItemField.OTHER_SCHEMA_NAME.getDefault());
         }(schema_name{
                       controlCore.factory(ItemField.OTHER_SCHEMA_NAME, $schema_name.text);
                      } '.')? object_type {
                                          controlCore.factory(ItemField.OTHER_ITEM_NAME, $object_type.text);
                                         } object_type_substitution?
      
      ('(' object_properties ')')? (ON COMMIT (DELETE|PRESERVE) ROWS)?
      oid_clause? oid_index_clause? physical_properties? table_properties_workaround* ';'?
    ;

object_type_substitution
    : NOT? SUBSTITUTABLE AT ALL LEVELS
    ;

oid_clause
    : OBJECT IDENTIFIER IS (SYSTEM GENERATED|PRIMARY KEY)
    ;

oid_index_clause
    : OIDINDEX index_name? '(' (physical_attributes_clause|TABLESPACE tablespace_name)+ ')'
    ;

//removed ? from table_properties to prevent empty block warning
xmltype_table
    : OF XMLTYPE ( '(' object_properties ')' )? (XMLTYPE xmltype_storage)? xmlschema_spec?
      {
       controlCore.factory(ItemField.OTHER_ITEM_NAME,ItemField.OTHER_ITEM_NAME.getDefault());
       controlCore.factory(ItemField.OTHER_SCHEMA_NAME,ItemField.OTHER_SCHEMA_NAME.getDefault());
      }
      xmltype_virtual_columns? (ON COMMIT (DELETE|PRESERVE) ROWS )? oid_clause?
      oid_index_clause? physical_properties? table_properties_workaround
    ;

user_defined_datatype
    : id
    ;

object_type
    : id
    ;

subpartiton_name
    : id
    ;

xmlschema_url
    : id
    ;

element
    : id
    ;

flashback_arcive
    : id
    ;

location_specifier
    : id
    ;

opaque_format_spec
    : id
    ;

nested_item
    :id
    ;

type
    :id
    ;

storage_table
    :id
    ;

access_driver_type
    :id
    ;

directory
    : id
    ;

cluster
    : id
    ;

log_group
    : id
    ;

expr
    : id
    ;

column_expression
    : id
    ;

encrypt_algorithm
    : id
    ;

password
    : id
    ;

ref_col
    : id
    ;

ref_attr
    : id
    ;

scope_table
    : id
    ;

create_directory
    : CREATE (OR REPLACE)? DIRECTORY directory AS quoted_string ';'
    ;

drop_directory
    : DROP DIRECTORY directory ';'
    ;



//schema

create_schema
    : CREATE SCHEMA AUTHORIZATION schema_name (create_table | create_view | grant_statement)+ ';'
    ;

grant
    : grant_statement ';'
    ;

grant_statement
    : GRANT ( grant_system_privileges | grant_object_privileges)
    ;

grant_system_privileges
    : system_privilege_choice (',' system_privilege_choice)*
      TO  grantee_clause (WITH ADMIN OPTION)?
    ;

system_privilege_choice
    : ( system_privilege | role | ALL PRIVILEGES)
    ;

grant_object_privileges
    : object_privileges_choice (',' object_privileges_choice)* on_object_clause
      TO grantee_clause (WITH HIERARCHY OPTION)? (WITH GRANT OPTION)?
    ;

object_privileges_choice
    : (object_privilege | ALL PRIVILEGES?)  ( '(' column (',' column)* ')' )?
    ;

//changed schema to optional. unlike documentation
//https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_9013.htm#i2123618
on_object_clause
    : ON ( (schema_name '.')? object |
            (
              DIRECTORY directory
            | JAVA (SOURCE | RESOURCE ) (schema_name '.')? object
            )
         )
    ;

grantee_clause
    : grante_clause_choice (',' grante_clause_choice)*
    ;

grante_clause_choice
    : (  user (IDENTIFIED BY password)
      | role
      | PUBLIC
      )
    ;

user
    : id
    ;

object
    : id
    ;

role
    : id
    ;

object_privilege
: ALTER
| DELETE
| DEBUG
| INDEX
| INSERT
| REFERENCES
| SELECT
| UPDATE
| UNDER
| EXECUTE
| ON COMMIT REFRESH
| QUERY REWRITE
| READ
| WRITE
;

system_privilege
: ADVISOR
| ADMINISTER SQL TUNING SET
| ADMINISTER ANY SQL TUNING SET
| CREATE ANY SQL PROFILE
| DROP ANY SQL PROFILE
| ALTER ANY SQL PROFILE
| CREATE CLUSTER
| CREATE ANY CLUSTER
| ALTER ANY CLUSTER
| DROP ANY CLUSTER
| CREATE ANY CONTEXT
| DROP ANY CONTEXT
| ALTER DATABASE
| ALTER SYSTEM
| AUDIT SYSTEM
| CREATE DATABASE LINK
| CREATE PUBLIC DATABASE LINK
| DROP PUBLIC DATABASE LINK
| DEBUG
| CONNECT
| SESSION
| ANY
| PROCEDURE
| ANALYZE
| DICTIONARY
| CREATE DIMENSION
| CREATE ANY DIMENSION
| ALTER ANY DIMENSION
| DROP ANY DIMENSION
| CREATE ANY DIRECTORY
| DROP ANY DIRECTORY
| FLASHBACK
| ARCHIVE
| ADMINISTER
| CREATE INDEXTYPE
| CREATE ANY INDEXTYPE
| ALTER ANY INDEXTYPE
| DROP ANY INDEXTYPE
| EXECUTE ANY INDEXTYPE
| CREATE ANY INDEX
| ALTER ANY INDEX
| DROP ANY INDEX
| CREATE JOB
| CREATE ANY JOB
| CREATE EXTERNAL JOB
| EXECUTE ANY PROGRAM
| EXECUTE ANY CLASS
| MANAGE SCHEDULER
| CREATE LIBRARY
| CREATE ANY LIBRARY
| DROP ANY LIBRARY
| CREATE MATERIALIZED VIEW
| CREATE ANY MATERIALIZED VIEW
| ALTER ANY MATERIALIZED VIEW
| DROP ANY MATERIALIZED VIEW
| QUERY REWRITE
| GLOBAL QUERY REWRITE
| ON COMMIT REFRESH
| FLASHBACK ANY TABLE
| CREATE MINING MODEL
| CREATE ANY MINING MODEL
| ALTER ANY MINING MODEL
| DROP ANY MINING MODEL
| SELECT ANY MINING MODEL
| COMMENT ANY MINING MODEL
| CREATE CUBE
| CREATE ANY CUBE
| ALTER ANY CUBE
| DROP ANY CUBE
| SELECT ANY CUBE
| UPDATE ANY CUBE
| CREATE MEASURE FOLDER
| CREATE ANY MEASURE FOLDER
| DELETE ANY MEASURE FOLDER
| DROP ANY MEASURE FOLDER
| INSERT ANY MEASURE FOLDER
| CREATE CUBE DIMENSION
| CREATE ANY CUBE DIMENSION
| ALTER ANY CUBE DIMENSION
| DELETE ANY CUBE DIMENSION
| DROP ANY CUBE DIMENSION
| INSERT ANY CUBE DIMENSION
| SELECT ANY CUBE DIMENSION
| UPDATE ANY CUBE DIMENSION
| CREATE CUBE BUILD PROCESS
| CREATE ANY CUBE BUILD PROCESS
| DROP ANY CUBE BUILD PROCESS
| UPDATE ANY CUBE BUILD PROCESS
| CREATE OPERATOR
| CREATE ANY OPERATOR
| ALTER ANY OPERATOR
| DROP ANY OPERATOR
| EXECUTE ANY OPERATOR
| CREATE ANY OUTLINE
| ALTER ANY OUTLINE
| DROP ANY OUTLINE
| CREATE PROCEDURE
| CREATE ANY PROCEDURE
| ALTER ANY PROCEDURE
| DROP ANY PROCEDURE
| EXECUTE ANY PROCEDURE
| CREATE PROFILE
| ALTER PROFILE
| DROP PROFILE
| CREATE ROLE
| ALTER ANY ROLE
| DROP ANY ROLE
| GRANT ANY ROLE
| CREATE ROLLBACK SEGMENT
| ALTER ROLLBACK SEGMENT
| DROP ROLLBACK SEGMENT
| CREATE SEQUENCE
| CREATE ANY SEQUENCE
| ALTER ANY SEQUENCE
| DROP ANY SEQUENCE
| SELECT ANY SEQUENCE
| CREATE SESSION
| ALTER RESOURCE COST
| ALTER SESSION
| RESTRICTED SESSION
| CREATE SYNONYM
| CREATE ANY SYNONYM
| CREATE PUBLIC SYNONYM
| DROP ANY SYNONYM
| DROP PUBLIC SYNONYM
| CREATE TABLE
| CREATE ANY TABLE
| ALTER ANY TABLE
| BACKUP ANY TABLE
| DELETE ANY TABLE
| DROP ANY TABLE
| INSERT ANY TABLE
| LOCK ANY TABLE
| SELECT ANY TABLE
| UPDATE ANY TABLE
| CREATE TABLESPACE
| ALTER TABLESPACE
| DROP TABLESPACE
| MANAGE TABLESPACE
| UNLIMITED TABLESPACE
| CREATE TRIGGER
| CREATE ANY TRIGGER
| ALTER ANY TRIGGER
| DROP ANY TRIGGER
| ADMINISTER DATABASE TRIGGER
| CREATE TYPE
| CREATE ANY TYPE
| ALTER ANY TYPE
| DROP ANY TYPE
| EXECUTE ANY TYPE
| UNDER ANY TYPE
| CREATE USER
| ALTER USER
| DROP USER
| CREATE VIEW
| CREATE ANY VIEW
| DROP ANY VIEW
| UNDER ANY VIEW
| MERGE ANY VIEW
| ANALYZE ANY
| AUDIT ANY
| BECOME
| USER
| CHANGE
| NOTIFICATION
| COMMENT ANY TABLE
| EXEMPT ACCESS POLICY
| FORCE ANY TRANSACTION
| FORCE TRANSACTION
| GRANT
| OBJECT
| PRIVILEGE
| GRANT ANY PRIVILEGE
| RESUMABLE
| SELECT ANY DICTIONARY
| SELECT ANY TRANSACTION
| SYSDBA
| SYSOPER
| CONNECT
| RESOURCE
| DBA
| DELETE_CATALOG_ROLE
| EXECUTE_CATALOG_ROLE
| SELECT_CATALOG_ROLE
| DELETE_CATALOG_ROLE EXECUTE_CATALOG_ROLE SELECT_CATALOG_ROLE //not sure about this.. so i just added again
| EXP_FULL_DATABASE
| IMP_FULL_DATABASE
| AQ_USER_ROLE
| AQ_ADMINISTRATOR_ROLE
| SNMPAGENT
| RECOVERY_CATALOG_OWNER
;

//https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_6002.htm#i2116626 thats the one i made...
//https://docs.oracle.com/database/121/SQLRF/statements_6002.htm thats the new one :/
//should delete the extras
create_materialized_view
    : create_materialized_view_111_updated_workaround
    | create_materialized_view_112_updated_workaround //should be deleted
    | create_materialized_view_112 //should be deleted
    | create_materialized_view_121 //should be deleted
    | create_materialized_view_102 //should be deleted
    | create_materialized_vw_log
    ;

create_materialized_vw_log
    : CREATE MATERIALIZED VIEW LOG ON {
                                       controlCore.factory(ItemType.MATERIALIZED_VIEW_LOG);
                                       controlCore.factory(ItemField.MY_SCHEMA_NAME,ItemField.MY_SCHEMA_NAME.getDefault());                                       
                                      } (schema_name {
                                                      controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);                                                        
                                                     } '.')? table_name {
                                                                         controlCore.factory(ItemField.ITEM_NAME, $table_name.text);
                                                                        }
      create_materialized_vw_log_inner* parallel_clause? table_partitioning_clauses?
      (WITH ( create_materialized_vw_log_inner_with_part (',' create_materialized_vw_log_inner_with_part)* )?
      ( '(' column (',' column)* ')')? //changed to optional rule because of default values the testPeriodicRefreshOfMaterializedViews.sql works after this change
      new_values_clause?
      )? mv_log_purge_clauses? for_refresh_clause? ';' { controlCore.finalizeV(); }
    ;

create_materialized_view_102
    : CREATE MATERIALIZED VIEW (schema_name '.')? materialized_view ( '(' column_alias (',' column_alias)* ')')?
      ( OF (schema_name '.')? object_type )? ( '(' scoped_table_ref_constraint ')' )?
      ( ON PREBUILT TABLE ( ( WITH | WITHOUT ) REDUCED PRECISION)? | physical_properties materialized_view_props ) //? maybe it wants optional this rule
      ( ( USING INDEX ( physical_attributes_clause | TABLESPACE tablespace_name )* | USING NO INDEX ) )? create_mv_refresh?
      ( FOR UPDATE )? ( ( DISABLE | ENABLE ) QUERY REWRITE)? AS subquery ';'
    ;

create_materialized_view_121
    : CREATE MATERIALIZED VIEW (schema_name '.')? materialized_view (OF (schema_name '.')? object_type)?
      ( '(' create_materialized_view_121_inner (','create_materialized_view_121_inner)* ')' )?
      (ON PREBUILT TABLE ((WITH|WITHOUT) REDUCED PRECISION)?|physical_properties materialized_view_props) //? maybe it wants optional this rule
      (USING INDEX (physical_attributes_clause|TABLESPACE tablespace_name)*|USING NO INDEX)? create_mv_refresh?
      (FOR UPDATE)? evaluation_edition_caluse? query_rewrite_clause? AS subquery ';'
    ;

create_materialized_view_112
    : CREATE MATERIALIZED VIEW (schema_name '.')? materialized_view (OF (schema_name '.')? object_type)?
      ( '(' create_materialized_view_121_inner (','create_materialized_view_121_inner)* ')' )?
      (ON PREBUILT TABLE ((WITH|WITHOUT) REDUCED PRECISION)?|physical_properties materialized_view_props)? //? maybe it wants optional this rule
      (USING INDEX (physical_attributes_clause|TABLESPACE tablespace_name)*|USING NO INDEX)? create_mv_refresh?
      (FOR UPDATE)? ( ( DISABLE | ENABLE ) QUERY REWRITE)? AS subquery ';'
    ;

//trying to workaround optional problems and line priorities
create_materialized_view_112_updated_workaround
    : CREATE MATERIALIZED VIEW (schema_name '.')? materialized_view
      create_materialized_view_112_updated_workaround_inner*
      (FOR UPDATE)? ( ( DISABLE | ENABLE ) QUERY REWRITE)? AS subquery ';'
    ;

create_materialized_view_112_updated_workaround_inner
:( (OF (schema_name '.')? object_type)
 | ( '(' create_materialized_view_121_inner (','create_materialized_view_121_inner)* ')' )
 | ( ON PREBUILT TABLE ( (WITH|WITHOUT) REDUCED PRECISION)? )
 | physical_properties
 | materialized_view_props_workaround //its optional
 | ( USING INDEX (physical_attributes_clause|TABLESPACE tablespace_name)* |  USING NO INDEX ) //if choocen only one of the two will be applied
 | create_mv_refresh //its optional
 )
;

// 11g.. https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_6002.htm#i2116443
create_materialized_view_111_updated_workaround
    : CREATE MATERIALIZED VIEW {
                                controlCore.factory(ItemType.MATERIALIZED_VIEW);
                                controlCore.factory(ItemField.MY_SCHEMA_NAME,ItemField.MY_SCHEMA_NAME.getDefault());                                       
                                controlCore.factory(ItemField.OTHER_ITEM_NAME,ItemField.OTHER_ITEM_NAME.getDefault());                                       
                                controlCore.factory(ItemField.OTHER_SCHEMA_NAME,ItemField.OTHER_SCHEMA_NAME.getDefault());                                       
                               }  (schema_name {
                                                controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);
                                               } '.')? materialized_view {
                                                                          controlCore.factory(ItemField.ITEM_NAME, $materialized_view.text);
                                                                         } ('(' column_alias (','column_alias)* ')')?
      create_materialized_view_111_updated_workaround_inner*
      (FOR UPDATE)? ( ( DISABLE | ENABLE ) QUERY REWRITE)? AS subquery ';'  { controlCore.finalizeV(); }
    ;

create_materialized_view_111_updated_workaround_inner
:( (OF (schema_name{
                    controlCore.factory(ItemField.OTHER_SCHEMA_NAME,$schema_name.text);
                   } '.')? object_type {
                                        controlCore.factory(ItemField.OTHER_ITEM_NAME,$object_type.text);
                                       })
 | ( '(' scoped_table_ref_constraint ')' )
 | ( ON PREBUILT TABLE ( (WITH|WITHOUT) REDUCED PRECISION)? )
 | physical_properties
 | materialized_view_props_workaround //its optional
 | ( USING INDEX (physical_attributes_clause|TABLESPACE tablespace_name)* |  USING NO INDEX ) //if choocen only one of the two will be applied
 | create_mv_refresh //its optional
 )
;



materialized_view_props_workaround
    : ( column_properties
      | table_partitioning_clauses
      | ( CACHE | NOCACHE )
      | parallel_clause
      | build_clause
      )+
    ;
//trying to workaround optional problems and line priorities - end


create_materialized_vw_log_inner
    : ( physical_attributes_clause
      | TABLESPACE tablespace_name
      | logging_clause
      | ( CACHE | NOCACHE )
      )
    ;

create_materialized_vw_log_inner_with_part
    : ( OBJECT ID
      | PRIMARY KEY
      | ROWID
      | SEQUENCE
      | COMMIT SCN
      )
    ;

//Belongs to 12.1
evaluation_edition_caluse
    : EVALUATE USING (CURRENT EDITION|EDITION id| NULL EDITION)
    ;

//Belongs to 12.1
query_rewrite_clause
    : (ENABLE | DISABLE) QUERY REWRITE unusuable_editions_clause
    ;

//Belongs to 12.1
unusuable_editions_clause
    :( UNUSUABLE BEFORE (CURRENT EDITION | EDITION id) )?
     ( UNUSUABLE BEGINNING WITH (CURRENT EDITION|EDITION id| NULL EDITION))?
    ;

// scoped_table_ref_constraint_inner cause of 12.1 version
create_materialized_view_121_inner
    : (scoped_table_ref_constraint_inner | column_alias (ENCRYPT encryption_spec? )? )
    ;

//not sure tho
encryption_spec
    : id
    ;

staging_log_name
    : id
    ;

new_values_clause
    : (INCLUDING | EXCLUDING ) NEW VALUES
    ;

for_refresh_clause
    : FOR ( SYNCHRONOUS REFRESH USING staging_log_name
          |FAST REFRESH)
    ;

mv_log_purge_clauses
    : PURGE ( IMMEDIATE ( SYNCHRONOUS | ASYNCHRONOUS )?
            | START WITH datetime_expression
              ( NEXT datetime_expression
              | REPEAT INTERVAL interval_expression
              )?
            | (START WITH datetime_expression )? ( NEXT datetime_expression
                                                 | REPEAT INTERVAL interval_expression
                                                 )
            )
    ;

scoped_table_ref_constraint
    : scoped_table_ref_constraint_inner ( ',' scoped_table_ref_constraint_inner)*
    ;

scoped_table_ref_constraint_inner
    : SCOPE FOR '(' ( ref_column | ref_attribute  ) ')' IS (schema_name '.')? ( scope_table_name | c_alias )
    ;

//Optional rule
materialized_view_props
    : column_properties? table_partitioning_clauses? ( CACHE | NOCACHE )? parallel_clause? build_clause?
    ;

create_mv_refresh
    : ( REFRESH create_mv_refresh_inner+
      | NEVER REFRESH )
    ;

create_mv_refresh_inner
    : ( ( FAST | COMPLETE | FORCE )
      | ON ( DEMAND | COMMIT )
      | ( START WITH | NEXT ) date
      | WITH ( PRIMARY KEY | ROWID )
      | USING ( DEFAULT ( MASTER | LOCAL ) ROLLBACK SEGMENT
              | ( MASTER | LOCAL ) ROLLBACK SEGMENT rollback_segment_name
              )+
      | USING ( ENFORCED | TRUSTED ) CONSTRAINTS
      )
    ;

date
    : expression //can properly get even calculations for date see testAutomaticRefreshTimesOfMaterializedViews.sql
    ;

build_clause
    : BUILD ( IMMEDIATE | DEFERRED )
    ;

ref_column
    : id
    ;

ref_attribute
    : id
    ;

scope_table_name
    : id
    ;

c_alias
    : id
    ;

materialized_view
    : id
    ;

//oracle functions not supported
//only the below are implemented
oracle_functions
    : ( TO_DATE '(' quoted_string (',' quoted_string)? (',' quoted_string)? ')'  //(testTableSubpartition2.sql) (string1,format_mask,nls_lang)
      | ROUND '(' ( expression ) ')'
      | SYSDATE
      | TRUNC  '(' (expression | quoted_string ) (',' quoted_string)? ')'
      | XMLTABLE '(' my_xml_namespaces_clause? xquery_string xmltable_options')'
      | XMLEXISTS '(' xquery_string xml_passing_clause? ')' //has no tests
      )
    ;

my_xml_namespaces_clause
    : XMLNAMESPACES '(' quoted_string AS identifier (',' quoted_string AS identifier)* //instead of string
      (DEFAULT quoted_string)? ')'
    ;

xmltable_options
    : xml_passing_clause? (COLUMNS xml_table_column ( ',' xml_table_column )* )?
    ;

xquery_string
    : quoted_string // no diagram on what this is.
    ;

//create_synonym
create_synonym
    : CREATE ( OR REPLACE )? (PUBLIC)? SYNONYM {
                                                controlCore.factory(ItemType.SYNONYM);
                                                controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
                                                controlCore.factory(ItemField.OTHER_SCHEMA_NAME,ItemField.OTHER_SCHEMA_NAME.getDefault());
                                                controlCore.factory(ItemField.LINK_NAME,ItemField.LINK_NAME.getDefault());
                                               } (schema_name {
                                                                controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);
                                                              } '.')? synonym_name {
                                                                                      controlCore.factory(ItemField.ITEM_NAME, $synonym_name.text);
                                                                                     } FOR (schema_name {
                                                                                                         controlCore.factory(ItemField.OTHER_SCHEMA_NAME,$schema_name.text);
                                                                                                        }'.')? object_name {
                                                                                                                            controlCore.factory(ItemField.OTHER_ITEM_NAME, $object_name.text);
                                                                                                                           } ('@' dblink_name{
                                                                                                                                              controlCore.factory(ItemField.LINK_NAME,$dblink_name.text);
                                                                                                                                             })? ';' {controlCore.finalizeV();}
    ;

dblink_name
    : id ('.' id)*
    ;


object_name
    : id
    ;

synonym_name
    : id
    ;
//end_synonym

drop_function
    : DROP FUNCTION function_name ';'
    ;

alter_function
    : ALTER FUNCTION function_name COMPILE DEBUG? compiler_parameters_clause* (REUSE SETTINGS)? ';'
    ;

create_function_body
    : (CREATE (OR REPLACE)?)? FUNCTION {
                                        controlCore.factory(ItemType.FUNCTION);
                                        controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
                                        controlCore.factory(ItemField.MY_PACKAGE_NAME,controlCore.getPackageName());
                                       } (schema_name { controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);} '.')? function_name {
                                                         controlCore.factory(ItemField.ITEM_NAME, $function_name.text);
                                                        } ('(' parameter (',' parameter)* ')')?
      RETURN type_spec (invoker_rights_clause|parallel_enable_clause|result_cache_clause|DETERMINISTIC)*
       | (( PIPELINED? (IS | AS) (DECLARE? declare_spec* body | call_spec))
       | (PIPELINED | AGGREGATE) USING implementation_type_name) ';' {
                                                                      controlCore.finalizeV();
                                                                     }
  ;


// $<Creation Function - Specific Clauses

parallel_enable_clause
    : PARALLEL_ENABLE partition_by_clause?
    ;

partition_by_clause
    : '(' PARTITION expression BY (ANY | (HASH | RANGE) '(' column_name (',' column_name)* ')')streaming_clause? ')'
    ;

result_cache_clause
    : RESULT_CACHE relies_on_part?
    ;

relies_on_part
    : RELIES_ON '(' tableview_name (',' tableview_name)* ')'
    ;

streaming_clause
    : (ORDER | CLUSTER) expression BY '(' column_name (',' column_name)* ')'
    ;

// index rules

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm
 */
create_index
	: create_index_statement ';'
	;

//added this if create_index would be used as statement
create_index_statement
	: CREATE ( UNIQUE | BITMAP )? INDEX (schema_name '.')? index_name
		ON  ( cluster_index_clause
			| table_index_clause
			| bitmap_join_index_clause
			)
                 UNUSUABLE? //11g
	;

cluster_index_clause
	: CLUSTER (schema_name '.')? cluster_name index_attributes?
	;

cluster_name
	: id
	;

index_attributes
	:  ( (
         physical_attributes_clause
         | logging_clause
	 | ONLINE
	 | COMPUTE STATISTICS
	 | TABLESPACE ( tablespace_name /* TODO warning. i think its name */ | DEFAULT )
	 | key_compression
	 | ( SORT | NOSORT )
	 | REVERSE
	 | parallel_clause
	 )+ )
	;

// #toLook auto 8elw na rwtisw
physical_attributes_clause
	:(  PCTFREE UNSIGNED_INTEGER  //was integer
	 | PCTUSED UNSIGNED_INTEGER  //was integer
	 | INITRANS UNSIGNED_INTEGER //was integer
	 | storage_clause
	 )+
	;

logging_clause
	: ( LOGGING | NOLOGGING | FILESYSTEM_LIKE_LOGGING)
	;

key_compression
	: ( COMPRESS (UNSIGNED_INTEGER)? | NOCOMPRESS ) //was integer
	;

parallel_clause
	: ( NOPARALLEL | PARALLEL (UNSIGNED_INTEGER)? ) //was integer
	;

/**
 * table_alias , index_expr TODO what are those? what do i need to implement
 */
table_index_clause
	: (schema_name '.')? table_name (table_alias)?
	   '(' index_expr (ASC | DESC )? (',' index_expr (ASC | DESC )? )* ')'
	   ( index_properties )?
	;

/**
 * http://www.toadworld.com/platforms/oracle/w/wiki/5458.index-expr-list-clause-syntax
 * TODO http://www.techonthenet.com/oracle/indexes.php column_expression -> function index
 */
index_expr
    :  column /*| column_expression */
    ;

//updated to 11g
index_properties
	:  ( index_properties_inner | INDEXTYPE IS (domain_index_clause|xmlindex_clause))
	;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2126404
 * ODCI_parameters dunno what it is
 */
domain_index_clause
    : indextype (parallel_clause)? (PARAMETERS '('  '\''  odci_parameters  '\'' ')')?
    ;
//Need to test all those https://docs.oracle.com/cd/B28359_01/appdev.111/b28369/xdb_indexing.htm#ADXDB0500
xmlindex_clause
    : (XDB '.')? XMLINDEX local_xmlindex_clause? parallel_clause?
      (PARAMETERS '('  '\''  xmlindex_parameters  '\'' ')')?
    ;

xmlindex_parameters
    : xmlindex_parameters_clause+
    ;

local_xmlindex_clause
    : LOCAL ('(' local_xmlindex_clause_inner (',' local_xmlindex_clause_inner)* ')')
    ;

local_xmlindex_clause_inner
    : PARTITION partition_name (PARAMETERS '\'' xmlindex_parameters '\'')?
    ;

xmlindex_parameters_clause
    : ( PATHS (create_index_paths_clause | alter_index_paths_clause)
      | ( path_table_clause | path_id_clause | order_key_clause | xml_index_value_clause )parallel_clause?
      | ASYNC '(' SYNC (ALWAYS|MANUAL|EVERY interval_expression| ON COMMIT) (STALE '(' (FALSE | TRUE )')')? ')'
      )
    ;

create_index_paths_clause
    : '(' ( INCLUDE | EXCLUDE ) '(' xpath_list ')' namespace_mapping_clause? ')'
    ;

alter_index_paths_clause
    : '(' (INDEX_ALL_PATHS| ( INCLUDE | EXCLUDE )( ADD | REMOVE ) ) '(' xpath_list ')' namespace_mapping_clause? ')'
    ;

namespace_mapping_clause
    : NAMESPACE MAPPING '(' namespace+ ')'
    ;

path_table_clause
    : PATH TABLE identifier? ( '('segment_attributes_clause table_properties_workaround ')' )?
    ;

path_id_clause
    : PATH ID (INDEX identifier? ('('index_attributes')')?)?
    ;

order_key_clause
    : ORDER KEY (INDEX identifier? ('('index_attributes')')?)?
    ;

xml_index_value_clause
    : VALUE (INDEX identifier? ('('index_attributes')')?)?
    ;

//i wanna believe its like this.
xpath_list
    : id (',' id)*
    ;

identifier
    :id
    ;

namespace
    :id
    ;

/**
 * not really sure https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2138869
 */
index_properties_inner
    : ( ( global_partitioned_index | local_partitioned_index ) | index_attributes )+
    ;

/**
 * That link is the reason i used this approach
 * http://www.oraclebuffer.com/general-discussions/which-index-to-choose-global-or-local-index-for-partitioned-table/
 */
global_partitioned_index
	: GLOBAL PARTITION BY ( RANGE '(' column_list ')' '(' index_partitioning_clause ')'
						  | HASH'(' column_list ')' ( individual_hash_partitions | hash_partitions_by_quantity )
						  )
	;

column_list
    : id
    ;


/*
i think odci_parameters text within single quotes
https://docs.oracle.com/cd/B28359_01/appdev.111/b28425/dom_idx.htm
http://stackoverflow.com/questions/22626579/regex-match-a-string-enclosed-in-single-quotes-but-dont-match-those-inside-do
doesnt seem like i need this

*/
odci_parameters
    : quoted_string
    ;

indextype
    : id ('.' id)* //i think  see test testOracleTextIndex.sql
    ;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/clauses009.htm#CJACEJGB
 */
storage_clause
    : STORAGE '(' ( INITIAL size_clause
                  | NEXT size_clause
                  | MINEXTENTS UNSIGNED_INTEGER //was integer
                  | MAXEXTENTS ( UNSIGNED_INTEGER | UNLIMITED ) //was integer
                  | PCTINCREASE UNSIGNED_INTEGER //was integer
                  | FREELISTS UNSIGNED_INTEGER //was integer
                  | FREELIST GROUPS UNSIGNED_INTEGER //was integer
                  | OPTIMAL (( size_clause | NULL ))?
                  | BUFFER_POOL ( KEEP | RECYCLE | DEFAULT)
                  )+ ')'
    ;

size_clause
    : UNSIGNED_INTEGER SIZE_LETTER?
    ;

/**
 * TODO
 * literal / segment_attributes_clause
 */
index_partitioning_clause
	: PARTITION (partition_name)? VALUES LESS THAN '(' literal (',' literal)* ')' (segment_attributes_clause)?
	;

partition_name
	: id
	;

individual_hash_partitions
	: '(' PARTITION (partition_name partitioning_storage_clause)?
		(',' PARTITION (partition_name partitioning_storage_clause)? )* ')'
	;

/**
 * TODO varray_item, LOB_segname, lob_item are names and nothing more right?
removed ? from the rule.
https://docs.oracle.com/cd/B28359_01/server.111/b28286/statements_7002.htm#CJADDEEH
 */
partitioning_storage_clause
    : ( ( TABLESPACE tablespace_name
		| OVERFLOW (TABLESPACE tablespace_name)?
		| LOB '(' lob_item ')' STORE AS (lob_segname ('(' TABLESPACE table_name')')? | '(' TABLESPACE table_name')')
		| VARRAY varray_item STORE AS LOB lob_segname
	)+ )
	;

varray_item
	: id
	;

lob_segname
	: id
	;

lob_item
	: id
	;
/**
 * TODO not sure about tablespace_name
 * ITS WRONG . RECHECK IT
 */
hash_partitions_by_quantity
	: PARTITIONS hash_partition_quantity (STORE IN '(' tablespace_name (',' tablespace_name )*')' )?
	   (OVERFLOW STORE IN '('  tablespace_name (',' tablespace_name )* ')' ) ?
	;

hash_partition_quantity
	: id
	;

table_name
	: id
	;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#sthref6528
 * column is a name also?
 */
bitmap_join_index_clause
	: (schema_name '.')? table_name '('
		( (schema_name'.')? table_name'.'|table_alias '.')? column ( ASC | DESC)?
		  (',' ((schema_name'.')? table_name'.'|table_alias '.')? column ( ASC | DESC)? )*
	')' FROM (schema_name '.')? table_name (table_alias)? ( ',' (schema_name '.')? table_name (table_alias)?)*
	 WHERE condition (local_partitioned_index)? index_attributes? //typo . forgot index_attributes that are optional
	;

column //its redirection to column_name
    : column_name
    ;

local_partitioned_index
    : LOCAL (on_range_partitioned_table | on_list_partitioned_table | on_hash_partinioned_table | on_comp_partitioned_table)?
	;

on_range_partitioned_table
	: '(' PARTITION (partition_name ((segment_attributes_clause|key_compression)+)?)?
	   (',' PARTITION (partition_name ((segment_attributes_clause|key_compression)+)?)?)*
	   ')'
	;
segment_attributes_clause
    :   ( physical_attributes_clause
        | TABLESPACE tablespace_name
        | logging_clause
        )+
    ;
/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2127615
 */
on_list_partitioned_table
	: '(' PARTITION (partition_name ((segment_attributes_clause|key_compression)+)?)?
	   (',' PARTITION (partition_name ((segment_attributes_clause|key_compression)+)?)?)*
	   ')'
	;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2127630
 */
on_hash_partinioned_table
    :   ( STORE IN '(' tablespace_name (',' tablespace_name)* ')'
		| '(' PARTITION (partition_name (TABLESPACE tablespace_name)?)? (',' PARTITION (partition_name (TABLESPACE tablespace_name)?)?)* ')'
		)
	;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2127707
 */
index_subpartition_clause
	: ( STORE IN '(' tablespace_name (',' tablespace_name)? ')'
	  | '(' SUBPARTITION (subpartition_name (TABLESPACE tablespace_name)?)? (',' SUBPARTITION (subpartition_name (TABLESPACE tablespace_name)?)?)* ')'
	  )
	;

subpartition_name
    : id
    ;

/**
 * https://docs.oracle.com/cd/B19306_01/server.102/b14200/statements_5010.htm#i2127641
 */
on_comp_partitioned_table
	/*: (STORE IN '(' tablespace_name (',' tablespace_name)? ')')?
	  '(' ( PARTITION (partition_name ( segment_attributes_clause | key_compression )* (index_subpartition_clause)?)? )
               (',' PARTITION (partition_name ( segment_attributes_clause | key_compression )* (index_subpartition_clause)?)?)* ')' */
    : (STORE IN '(' tablespace_name (',' tablespace_name)? ')')?
      '('
         PARTITION (partition_name ( segment_attributes_clause | key_compression )* (index_subpartition_clause)? )?
         (',' PARTITION (partition_name ( segment_attributes_clause | key_compression )* (index_subpartition_clause)? )? )*
      ')'
    ;

tablespace_name
	: id
	;

// view rules

create_view
    : CREATE (OR REPLACE)? ((NO)? FORCE)? VIEW {
                                                controlCore.factory(ItemType.VIEW); 
                                                controlCore.factory(ItemField.MY_SCHEMA_NAME,ItemField.MY_SCHEMA_NAME.getDefault());
                                               } view_spec view_restriction? ';' {controlCore.finalizeV();}
    ;

view_spec
    : (schema_name{
                   controlCore.factory(ItemField.MY_SCHEMA_NAME, $schema_name.text);
                  }'.')? view_name { controlCore.factory(ItemField.ITEM_NAME, $view_name.text); } (view_extras)? AS view_body
    ;

view_name
    : id
    ;

view_body
    : data_manipulation_language_statements
    ;

view_extras
    : ( object_view_clause | xmltype_view_clause | other_view_extra )
    ;

other_view_extra
    : '(' view_propeprty ( ',' view_propeprty )* ')'
    ;

view_propeprty
    : ( out_of_line_constraint | alias inline_constraint*  )
    ;

alias
    : id
    ;

inline_constraint
    : ( CONSTRAINT constraint_name)? ( (NOT)? NULL
                                     | UNIQUE
                                     | PRIMARY KEY
                                     | references_clause
                                     | CHECK '(' condition ')'
                                     ) constraint_state?
    ;

out_of_line_constraint
    : (CONSTRAINT constraint_name)? ( UNIQUE '(' column (',' column)* ')'
                                     | PRIMARY KEY '(' column (',' column)* ')'
                                     | FOREIGN KEY '(' column (',' column)* ')'  references_clause
                                     | CHECK '(' condition ')'
                                     ) constraint_state?
    ;

constraint_state
    //: constraint_state_inner (constraint_state_inner)*
    : constraint_state_inner+
    ;

constraint_state_inner
    : ( (NOT)? DEFERRABLE
      | INITIALLY (IMMEDIATE | DEFERRED )
      | ( ENABLE | DISABLE )
      | ( VALIDATE | NOVALIDATE )
      | (RELY | NORELY)
      | using_index_clause //https://docs.oracle.com/cd/B28359_01/server.111/b28286/clauses002.htm#CJAIHHGC -> 8elei to create index apo pano.. lgka
      | exception_clause
      )
    ;

using_index_clause
    : USING INDEX ( (schema_name'.')? index_name
                  | '(' create_index_statement ')'
                  | index_properties)
    ;

references_clause
    : REFERENCES {
                  controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,ItemField.EXTRA_SCHEMA_NAME.getDefault(),Boolean.TRUE);
                 } (schema_name {
                                  controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,$schema_name.text,Boolean.TRUE);
                                } '.')? ref_object {
                                                    controlCore.factory(ItemField.EXTRA_NAME,$ref_object.text,Boolean.TRUE);                                                    
                                                   } ( '(' column_name ')' )? ( ON DELETE ( CASCADE | SET NULL ) )?
    ;

ref_object //i think its like this.
    //: id ('.' id_expression)* ('@' link_name)?
    : id // the ()* is not needed cause of schema_name above. 
         //as for @link_name the documentation do not same something for this
    ;

xmltype_view_clause
    : OF XMLTYPE /*(xmlschema_spec)?*/ WITH OBJECT IDENTIFIER (DEFAULT | '(' id_expression (',' id_expression)* ')') //TODO the doc says "expr" . did i chooce the correct rule?
    ;
/*
xmlschema_spec
    : (single_xmlschema_spec | multiple_xmlschema_spec) (ALLOW ( ANYSCHEMA | NONSCHEMA )? | DISALLOW NONSCHEMA )?
    ;
*/
object_view_clause
    : OF (schema_name'.')? type_name (WITH OBJECT IDENTIFIER
                                      (DEFAULT | '(' ATTRIBUTE ( ',' ATTRIBUTE)* ')') | UNDER (schema_name'.')? superview )
      '(' (out_of_line_constraint | attribute_name (inline_constraint)* ) (',' (out_of_line_constraint | attribute_name (inline_constraint)* ))* ')'
    ;

view_restriction
    : WITH ( READ ONLY | CHECK OPTION ( CONSTRAINT constraint_name)? )
    ;

superview
    : id
    ;


// $<Package DDLs

drop_package
    : DROP PACKAGE BODY? package_name ';'
    ;

alter_package
    : ALTER PACKAGE package_name COMPILE DEBUG? (PACKAGE | BODY | SPECIFICATION)? compiler_parameters_clause* (REUSE SETTINGS)? ';'
    ;

create_package
    : CREATE (OR REPLACE)? PACKAGE (package_spec | package_body)? ';'
    ;

// $<Create Package - Specific Clauses

package_body
    : BODY  {
             controlCore.factory(ItemType.PACKAGE);
             controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
            } (schema_name{
                            controlCore.factory(ItemField.MY_SCHEMA_NAME, $schema_name.text);
                           }'.')? package_name {
                            controlCore.factory(ItemField.ITEM_NAME, $package_name.text);
                           } (IS | AS) package_obj_body* (BEGIN seq_of_statements | END {
                                                                                         controlCore.finalizeV();
                                                                                        } package_name?)
    ;

package_spec
    : (schema_name'.')? package_name invoker_rights_clause? (IS | AS) package_obj_spec* END package_name?
    ;

package_obj_spec
    : variable_declaration
    | subtype_declaration
    | cursor_declaration
    | exception_declaration
    | record_declaration
    | table_declaration
    | procedure_spec
    | function_spec
    ;

procedure_spec
    : PROCEDURE procedure_name ('(' parameter ( ',' parameter )* ')')? ';' //To procedure dn einai idio me to function?
    ;

function_spec
    : FUNCTION function_name('(' parameter ( ',' parameter)* ')')?
        RETURN type_spec
        (DETERMINISTIC | PIPELINED | RESULT_CACHE)? // NP:Added see Test2.sq;
    ';'
    ;

package_obj_body
    : variable_declaration
    | subtype_declaration
    | cursor_declaration
    | exception_declaration
    | record_declaration
    | table_declaration
    | create_procedure_body
    | create_function_body
    ;

// $<Procedure DDLs

drop_procedure
    : DROP PROCEDURE procedure_name ';'
    ;

alter_procedure
    : ALTER PROCEDURE procedure_name COMPILE DEBUG? compiler_parameters_clause* (REUSE SETTINGS)? ';'
    ;

create_procedure_body
    : (CREATE (OR REPLACE)?)? PROCEDURE {
                                         controlCore.factory(ItemType.PROCEDURE);
                                         controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
                                         controlCore.factory(ItemField.MY_PACKAGE_NAME,controlCore.getPackageName());
                                        }(schema_name { controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);} '.')? procedure_name {
                                                           controlCore.factory(ItemField.ITEM_NAME, $procedure_name.text);
                                                         } ('(' parameter (',' parameter)* ')')?
      invoker_rights_clause? (IS | AS)
      (DECLARE? declare_spec* body | call_spec | EXTERNAL) ';'{
                                                               controlCore.finalizeV();
                                                              }
    ;

// $>

// $<Trigger DDLs

drop_trigger
    : DROP TRIGGER trigger_name ';'
    ;

alter_trigger
    : ALTER TRIGGER tn1=trigger_name
      ((ENABLE | DISABLE) | RENAME TO tn2=trigger_name | COMPILE DEBUG? compiler_parameters_clause* (REUSE SETTINGS)?) ';'
    ;

create_trigger
    : CREATE ( OR REPLACE )? TRIGGER {
                                      controlCore.factory(ItemType.TRIGGER);
                                      controlCore.factory(ItemField.MY_SCHEMA_NAME,controlCore.getSchemaName());
                                     }(schema_name {
                                                    controlCore.factory(ItemField.MY_SCHEMA_NAME,$schema_name.text);
                                                   }'.')? trigger_name {
                                                                        controlCore.factory(ItemField.ITEM_NAME, $trigger_name.text);
                                                                       }
    (simple_dml_trigger | compound_dml_trigger | non_dml_trigger)
    trigger_follows_clause? (ENABLE | DISABLE)? trigger_when_clause? trigger_body { controlCore.finalizeV();} ';' 
    ;

trigger_follows_clause
    : (FOLLOWS|PRECEDES) {
                          controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,ItemField.EXTRA_SCHEMA_NAME.getDefault(), Boolean.TRUE);
                         } (schema_name{
                                        controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,$schema_name.text ,Boolean.TRUE);
                                       }'.')? trigger_name {
                                                            controlCore.factory(ItemField.EXTRA_NAME, $trigger_name.text, Boolean.TRUE);
                                                           } (',' {
                                                                   controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,ItemField.EXTRA_SCHEMA_NAME.getDefault(), Boolean.TRUE);
                                                                   } (schema_name{
                                                                                  controlCore.factory(ItemField.EXTRA_SCHEMA_NAME,$schema_name.text ,Boolean.TRUE);
                                                                                  }'.')? trigger_name{
                                                                                                      controlCore.factory(ItemField.EXTRA_NAME, $trigger_name.text, Boolean.TRUE);
                                                                                                      })*
    ;

trigger_when_clause
    : WHEN '(' condition ')'
    ;

// $<Create Trigger- Specific Clauses
simple_dml_trigger
    : (BEFORE | AFTER | INSTEAD OF) dml_event_clause referencing_clause? for_each_row?
    ;

for_each_row
    : FOR EACH ROW
    ;

compound_dml_trigger
    : FOR dml_event_clause referencing_clause?
    ;

non_dml_trigger
    : (BEFORE | AFTER | INSTEAD OF) non_dml_event (OR non_dml_event)* ON {
                                                                          controlCore.factory(ItemField.OTHER_SCHEMA_NAME, ItemField.OTHER_SCHEMA_NAME.getDefault());
                                                                         }(DATABASE |(schema_name {
                                                                                                   controlCore.factory(ItemField.OTHER_SCHEMA_NAME, $schema_name.text);
                                                                                                  } '.')? SCHEMA) {
                                                                                                                   controlCore.factory(ItemField.OTHER_ITEM_NAME, ItemField.OTHER_ITEM_NAME.getDefault());
                                                                                                                  }
    ;

trigger_body
    : COMPOUND TRIGGER
    | CALL id
    | trigger_block
    ;

routine_clause
    : routine_name function_argument?
    ;

compound_trigger_block
    : COMPOUND TRIGGER declare_spec* timing_point_section+ END trigger_name
    ;

timing_point_section
    : bk=BEFORE STATEMENT IS trigger_block BEFORE STATEMENT ';'
    | bk=BEFORE EACH ROW IS trigger_block BEFORE EACH ROW ';'
    | ak=AFTER STATEMENT IS trigger_block AFTER STATEMENT ';'
    | ak=AFTER EACH ROW IS trigger_block AFTER EACH ROW ';'
    ;

non_dml_event
    : ALTER
    | ANALYZE
    | ASSOCIATE STATISTICS
    | AUDIT
    | COMMENT
    | CREATE
    | DISASSOCIATE STATISTICS
    | DROP
    | GRANT
    | NOAUDIT
    | RENAME
    | REVOKE
    | TRUNCATE
    | DDL
    | STARTUP
    | SHUTDOWN
    | DB_ROLE_CHANGE
    | LOGON
    | LOGOFF
    | SERVERERROR
    | SUSPEND
    | DATABASE
    | SCHEMA
    | FOLLOWS
    ;

dml_event_clause
    : dml_event_element (OR dml_event_element)* ON dml_event_nested_clause?  {
                                                                              controlCore.factory(ItemField.OTHER_SCHEMA_NAME,ItemField.OTHER_SCHEMA_NAME.getDefault());
                                                                             }(schema_name {
                                                                                            controlCore.factory(ItemField.OTHER_SCHEMA_NAME,$schema_name.text);
                                                                                           } '.')? my_tableview_name {
                                                                                                                      controlCore.factory(ItemField.OTHER_ITEM_NAME,$my_tableview_name.text);
                                                                                                                     }
    ;

dml_event_element
    : (DELETE|INSERT|UPDATE) (OF column_name (',' column_name)*)?
    ;

dml_event_nested_clause
    : NESTED TABLE my_tableview_name OF
    ;

my_tableview_name
    : id
    ;

referencing_clause
    : REFERENCING referencing_element+
    ;

referencing_element
    : (NEW | OLD | PARENT) column_alias
    ;

// $>
// $>

// $<Type DDLs

drop_type
    : DROP TYPE BODY? type_name (FORCE | VALIDATE)? ';'
    ;

alter_type
    : ALTER TYPE type_name
    (compile_type_clause
    | replace_type_clause
    //TODO | {input.LT(2).getText().equalsIgnoreCase("attribute")}? alter_attribute_definition
    | alter_method_spec
    | alter_collection_clauses
    | modifier_clause
    ) dependent_handling_clause? ';'
    ;

// $<Alter Type - Specific Clauses
compile_type_clause
    : COMPILE DEBUG? (SPECIFICATION | BODY)? compiler_parameters_clause* (REUSE SETTINGS)?
    ;

replace_type_clause
    : REPLACE invoker_rights_clause? AS OBJECT '(' object_member_spec (',' object_member_spec)* ')'
    ;

alter_method_spec
    : alter_method_element (',' alter_method_element)*
    ;

alter_method_element
    : (ADD|DROP) (map_order_function_spec | subprogram_spec)
    ;

alter_attribute_definition
    : (ADD | MODIFY | DROP) ATTRIBUTE (attribute_definition | '(' attribute_definition (',' attribute_definition)* ')')
    ;

attribute_definition
    : attribute_name type_spec?
    ;

alter_collection_clauses
    : MODIFY (LIMIT expression | ELEMENT TYPE type_spec)
    ;

dependent_handling_clause
    : INVALIDATE
    | CASCADE (CONVERT TO SUBSTITUTABLE | NOT? INCLUDING TABLE DATA)? dependent_exceptions_part?
    ;

dependent_exceptions_part
    : FORCE? EXCEPTIONS INTO tableview_name
    ;

create_type //#lookHere
    : CREATE (OR REPLACE)? TYPE (type_definition | type_body) ';'
    ;

literal
    : quoted_string
    | oracle_functions // NEW ADDITION
    | numeric
    ;

// $<Create Type - Specific Clauses
type_definition
    : type_name (OID CHAR_STRING)? object_type_def?
    ;

object_type_def
    : invoker_rights_clause? (object_as_part | object_under_part) sqlj_object_type?
      ('(' object_member_spec (',' object_member_spec)* ')')? modifier_clause*
    ;

object_as_part
    : (IS | AS) (OBJECT | varray_type_def | nested_table_type_def)
    ;

object_under_part
    : UNDER type_spec
    ;

nested_table_type_def
    : TABLE OF type_spec (NOT NULL)?
    ;

sqlj_object_type
    : EXTERNAL NAME expression LANGUAGE JAVA USING (SQLDATA|CUSTOMDATUM|ORADATA)
    ;

type_body
    : BODY type_name (IS | AS) type_body_elements (',' type_body_elements)* END
    ;

type_body_elements
    : map_order_func_declaration
    | subprog_decl_in_type
    ;

map_order_func_declaration
    : (MAP | ORDER) MEMBER func_decl_in_type
    ;

subprog_decl_in_type
    : (MEMBER | STATIC) (proc_decl_in_type | func_decl_in_type | constructor_declaration)
    ;

proc_decl_in_type
    : PROCEDURE procedure_name '(' type_elements_parameter (',' type_elements_parameter)* ')'
      (IS | AS) (call_spec | DECLARE? declare_spec* body ';')
    ;

func_decl_in_type
    : FUNCTION function_name ('(' type_elements_parameter (',' type_elements_parameter)* ')')?
      RETURN type_spec (IS | AS) (call_spec | DECLARE? declare_spec* body ';')
    ;

constructor_declaration
    : FINAL? INSTANTIABLE? CONSTRUCTOR FUNCTION type_spec
      ('(' (SELF IN OUT type_spec ',') type_elements_parameter (',' type_elements_parameter)*  ')')?
      RETURN SELF AS RESULT (IS | AS) (call_spec | DECLARE? declare_spec* body ';')
    ;

// $>

// $<Common Type Clauses

modifier_clause
    : NOT? (INSTANTIABLE | FINAL | OVERRIDING)
    ;

object_member_spec
    : id type_spec sqlj_object_type_attr?
    | element_spec
    ;

sqlj_object_type_attr
    : EXTERNAL NAME expression
    ;

element_spec
    : modifier_clause? element_spec_options+ (',' pragma_clause)?
    ;

element_spec_options
    : subprogram_spec
    | constructor_spec
    | map_order_function_spec
    ;

subprogram_spec
    : (MEMBER|STATIC) (type_procedure_spec|type_function_spec)
    ;

type_procedure_spec
    : PROCEDURE procedure_name '(' type_elements_parameter (',' type_elements_parameter)* ')' ((IS | AS) call_spec)?
    ;

type_function_spec
    : FUNCTION function_name ('(' type_elements_parameter (',' type_elements_parameter)* ')')?
      RETURN (type_spec | SELF AS RESULT) ((IS | AS) call_spec | EXTERNAL VARIABLE? NAME expression)?
    ;

constructor_spec
    : FINAL? INSTANTIABLE? CONSTRUCTOR FUNCTION type_spec ('(' (SELF IN OUT type_spec ',') type_elements_parameter (',' type_elements_parameter)*  ')')?  RETURN SELF AS RESULT ((IS | AS) call_spec)?
    ;

map_order_function_spec
    : (MAP | ORDER) MEMBER type_function_spec
    ;

pragma_clause
    : PRAGMA RESTRICT_REFERENCES '(' pragma_elements (',' pragma_elements)* ')'
    ;

pragma_elements
    : id
    | DEFAULT
    ;

type_elements_parameter
    : parameter_name type_spec
    ;

// $<Sequence DDLs

drop_sequence
    : DROP SEQUENCE sequence_name ';'
    ;

alter_sequence
    : ALTER SEQUENCE sequence_name sequence_spec+ ';'
    ;

create_sequence
    : CREATE SEQUENCE sequence_name (sequence_start_clause | sequence_spec)* ';'
    ;

// $<Common Sequence

sequence_spec
    : INCREMENT BY UNSIGNED_INTEGER
    | MAXVALUE UNSIGNED_INTEGER
    | NOMAXVALUE
    | MINVALUE UNSIGNED_INTEGER
    | NOMINVALUE
    | CYCLE
    | NOCYCLE
    | CACHE UNSIGNED_INTEGER
    | NOCACHE
    | ORDER
    | NOORDER
    ;

sequence_start_clause
    : START WITH UNSIGNED_INTEGER
    ;

// $<Common DDL Clauses

invoker_rights_clause
    : AUTHID (CURRENT_USER|DEFINER)
    ;

compiler_parameters_clause
    : id '=' expression
    ;

call_spec
    : LANGUAGE (java_spec | c_spec)
    ;

// $<Call Spec - Specific Clauses

java_spec
    : JAVA NAME CHAR_STRING
    ;

c_spec
    : C_LETTER (NAME CHAR_STRING)? LIBRARY id c_agent_in_clause? (WITH CONTEXT)? c_parameters_clause?
    ;

c_agent_in_clause
    : AGENT IN '(' expression (',' expression)* ')'
    ;

c_parameters_clause
    : PARAMETERS '(' (expression (',' expression)* | '.' '.' '.') ')'
    ;

// $>

parameter
    : parameter_name ( IN | OUT | INOUT | NOCOPY)* type_spec? default_value_part?
    ;

default_value_part
    : (ASSIGN_OP | DEFAULT) expression
    ;

// $<PL/SQL Elements Declarations

declare_spec
    : variable_declaration
    | subtype_declaration
    | cursor_declaration
    | exception_declaration
    | pragma_declaration
    | record_declaration
    | table_declaration
    | create_procedure_body
    | create_function_body
    ;

//incorporates constant_declaration
variable_declaration
    : variable_name CONSTANT? type_spec (NOT NULL)? default_value_part? ';'
    | table_var_dec ';' //NP: Changed Test5.sql
    ;

subtype_declaration
    : SUBTYPE type_name IS type_spec (RANGE expression '..' expression)? (NOT NULL)? ';'
    ;

//cursor_declaration incorportates curscursor_body and cursor_spec
cursor_declaration
    : CURSOR cursor_name ('(' parameter_spec (',' parameter_spec)* ')' )? (RETURN type_spec)? (IS select_statement)? ';'
    ;

parameter_spec
    : parameter_name (IN? type_spec)? default_value_part?
    ;

exception_declaration
    : exception_name EXCEPTION ';'
    ;

pragma_declaration
    : PRAGMA (SERIALLY_REUSABLE
    | AUTONOMOUS_TRANSACTION
    | EXCEPTION_INIT '(' exception_name ',' error_code ')' //NP: Added test7.sql
    | INLINE '(' id1=id ',' expression ')'
    | RESTRICT_REFERENCES '(' (id | DEFAULT) (',' id)+ ')') ';'
    ;

record_declaration
    : record_type_dec
    | record_var_dec
    ;

// $<Record Declaration - Specific Clauses

//incorporates ref_cursor_type_definition
record_type_dec //#lookHere
    : TYPE type_name IS (RECORD '(' field_spec (',' field_spec)* ')' | REF CURSOR (RETURN type_spec)?) ';'
    ;

field_spec
    : column_name type_spec? (NOT NULL)? default_value_part?
    ;

record_var_dec
    : record_name type_name (PERCENT_ROWTYPE | PERCENT_TYPE) ';'
    ;

// $>

table_declaration
    : (table_type_dec ) ';' //NP: Changed test5.sql
    ;

table_type_dec
    : TYPE type_name IS (TABLE OF type_spec table_indexed_by_part? (NOT NULL)? | varray_type_def)
    ;

table_indexed_by_part
    : (idx1=INDEXED | idx2=INDEX) BY type_spec
    ;

varray_type_def
    : (VARRAY | VARYING ARRAY) '(' expression ')' OF type_spec (NOT NULL)?
    ;

table_var_dec
    : table_var_name table_type_dec
    ;

// $>

// $<PL/SQL Statements

seq_of_statements
    : (statement (';' | EOF) | label_declaration)+
    ;

label_declaration
    : ltp1= '<' '<' label_name '>' '>'
    ;

statement
    : CREATE swallow_to_semi
    | ALTER swallow_to_semi
    | GRANT swallow_to_semi
    | TRUNCATE swallow_to_semi
    | body
    | block
    | assignment_statement
    | continue_statement
    | exit_statement
    | goto_statement
    | if_statement
    | loop_statement
    | forall_statement
    | null_statement
    | raise_statement
    | return_statement
    | case_statement/*[true]*/
    | sql_statement
    | function_call
    | piperow_statement //NP: Added Test11.sql Normally this is only allowed in pipelined table functions
    | table_type_attribute_call //NP: Added Test5.sql
    ;

piperow_statement:
    PIPE ROW '(' expression ')'
;

table_type_attribute_call: (schema_name '.')? table_var_name '.'  DELETE ( '(' (expression',')?expression')')?;

assignment_statement
    : (general_element | bind_variable) ASSIGN_OP expression
    ;

continue_statement
    : CONTINUE label_name? (WHEN condition)?
    ;

exit_statement
    : EXIT label_name? (WHEN condition)?
    ;

goto_statement
    : GOTO label_name
    ;

if_statement
    : IF condition THEN seq_of_statements elsif_part* else_part? END IF
    ;

elsif_part
    : ELSIF condition THEN seq_of_statements
    ;

else_part
    : ELSE seq_of_statements
    ;

loop_statement
    : label_name? (WHILE condition | FOR cursor_loop_param)? LOOP seq_of_statements END LOOP label_name?
    ;

// $<Loop - Specific Clause

cursor_loop_param
    : index_name IN REVERSE? lower_bound '..' upper_bound
    | record_name IN ( cursor_name expression_list? | '(' select_statement ')')
    ;
// $>

forall_statement
    : FORALL index_name IN bounds_clause sql_statement (SAVE EXCEPTIONS)?
    ;

bounds_clause
    : lower_bound '..' upper_bound
    | INDICES OF collection_name between_bound?
    | VALUES OF index_name
    ;

between_bound
    : BETWEEN lower_bound AND upper_bound
    ;

lower_bound
    : concatenation
    ;

upper_bound
    : concatenation
    ;

null_statement
    : NULL
    ;

raise_statement
    : RAISE exception_name?
    ;

return_statement
    : RETURN cn1=condition?
    ;

function_call //Keeping note
    : CALL? {
             controlCore.factory(ItemType.CALL); 
             controlCore.factory(ItemField.ITEM_NAME, controlCore.getItemName()); 
             controlCore.factory(ItemField.MY_PACKAGE_NAME, controlCore.getPackageName()); 
             controlCore.factory(ItemField.MY_SCHEMA_NAME, controlCore.getSchemaName()); 
             controlCore.validateCall();
            } routine_name function_argument? {controlCore.finalizeV();}
    ;

body
    : BEGIN seq_of_statements exception_clause? END label_name?
    ;

// $<Body - Specific Clause

exception_clause
    : EXCEPTION exception_handler+
    ;

exception_handler
    : WHEN exception_name (OR exception_name)* THEN seq_of_statements
    ;

// $>

trigger_block
    : (DECLARE? declare_spec+)? body
    ;

block
    : DECLARE? declare_spec+ body
    ;

// $>

// $<SQL PL/SQL Statements

sql_statement
    : execute_immediate
    | data_manipulation_language_statements
    | cursor_manipulation_statements
    | transaction_control_statements
    ;

execute_immediate
    : EXECUTE IMMEDIATE expression (into_clause using_clause? | using_clause dynamic_returning_clause? | dynamic_returning_clause)?
    ;

// $<Execute Immediate - Specific Clause
dynamic_returning_clause
    : (RETURNING | RETURN) into_clause
    ;
// $>


// $<DML SQL PL/SQL Statements

data_manipulation_language_statements
    : merge_statement
    | lock_table_statement
    | select_statement
    | update_statement
    | delete_statement
    | insert_statement
    | explain_statement
    ;

// $>

// $<Cursor Manipulation SQL PL/SQL Statements

cursor_manipulation_statements
    : close_statement
    | open_statement
    | fetch_statement
    | open_for_statement
    ;

close_statement
    : CLOSE cursor_name
    ;

open_statement
    : OPEN cursor_name expression_list?
    ;

fetch_statement
    : FETCH cursor_name (it1=INTO variable_name (',' variable_name )* | BULK COLLECT INTO variable_name (',' variable_name )*) (LIMIT expression)?
    ;

open_for_statement
    : OPEN variable_name FOR (select_statement | expression) using_clause?
    ;

// $>

// $<Transaction Control SQL PL/SQL Statements

transaction_control_statements
    : set_transaction_command
    | set_constraint_command
    | commit_statement
    | rollback_statement
    | savepoint_statement
    ;

set_transaction_command
    : SET TRANSACTION
      (READ (ONLY | WRITE) | ISOLATION LEVEL (SERIALIZABLE | READ COMMITTED) | USE ROLLBACK SEGMENT rollback_segment_name)?
      (NAME quoted_string)?
    ;

set_constraint_command
    : SET (CONSTRAINT | CONSTRAINTS) (ALL | constraint_name (',' constraint_name)*) (IMMEDIATE | DEFERRED)
    ;

commit_statement
    : COMMIT WORK?
      (COMMENT expression | FORCE (CORRUPT_XID expression| CORRUPT_XID_ALL | expression (',' expression)?))?
      write_clause?
    ;

write_clause
    : WRITE (WAIT|NOWAIT)? (IMMEDIATE|BATCH)?
    ;

rollback_statement
    : ROLLBACK WORK? (TO SAVEPOINT? savepoint_name | FORCE quoted_string)?
    ;

savepoint_statement
    : SAVEPOINT savepoint_name
    ;

// Dml

/* TODO
//SHOULD BE OVERRIDEN!
compilation_unit
    :  seq_of_statements* EOF
    ;

//SHOULD BE OVERRIDEN!
seq_of_statements
    : select_statement
    | update_statement
    | delete_statement
    | insert_statement
    | lock_table_statement
    | merge_statement
    | explain_statement
//    | case_statement[true]
    ;
*/

explain_statement
    : EXPLAIN PLAN (SET STATEMENT_ID '=' quoted_string)? (INTO tableview_name)?
      FOR (select_statement | update_statement | delete_statement | insert_statement | merge_statement)
    ;

select_statement
    : subquery_factoring_clause? subquery (for_update_clause | order_by_clause)*
    ;

// $<Select - Specific Clauses
subquery_factoring_clause
    : WITH factoring_element (',' factoring_element)*
    ;

factoring_element
    : query_name ('(' column_name (',' column_name)* ')')? AS '(' subquery order_by_clause? ')'
      search_clause? cycle_clause?
    ;

search_clause
    : SEARCH (DEPTH | BREADTH) FIRST BY column_name ASC? DESC? (NULLS FIRST)? (NULLS LAST)?
      (',' column_name ASC? DESC? (NULLS FIRST)? (NULLS LAST)?)* SET column_name
    ;

cycle_clause
    : CYCLE column_name (',' column_name)* SET column_name TO expression DEFAULT expression
    ;

subquery
    : subquery_basic_elements subquery_operation_part*
    ;

subquery_operation_part
    : (UNION ALL? | INTERSECT | MINUS) subquery_basic_elements
    ;

subquery_basic_elements
    : query_block
    | '(' subquery ')'
    ;
/*
query_block
    : SELECT (DISTINCT | UNIQUE | ALL)? ('*' | selected_element (',' selected_element)*)
      into_clause? from_clause where_clause? hierarchical_query_clause? group_by_clause? model_clause?
    ;
*/
query_block
    : SELECT  (DISTINCT | UNIQUE | ALL)? ('*' | selected_element (',' selected_element)*)
      into_clause? from_clause where_clause? hierarchical_query_clause? group_by_clause? model_clause?
    ;

//NEW ADDITION
selected_element
    :  /*( (table_alias '.')? '*'
        | ( ( query_name
            | (schema_name '.')? ( table_name
                                 | view_name
                                 | materialized_view
                                 ) ('.' '*')? //added optional rule
            )
          | expression (AS? c_alias)?
          )
       )*/
       /*(schema_name '.')?*/ select_list_elements column_alias?// old one
    ;

from_clause
    : FROM table_ref_list
    ;

select_list_elements
    : tableview_name '.' '*'
    | expression
    ;

table_ref_list
    : table_ref (',' table_ref)*
    ;

// NOTE to PIVOT clause
// according the SQL reference this should not be possible
// according to he reality it is. Here we probably apply pivot/unpivot onto whole join clause
// eventhough it is not enclosed in parenthesis. See pivot examples 09,10,11
table_ref
    : table_ref_aux join_clause* (pivot_clause | unpivot_clause)?
    ;

table_ref_aux
    : (dml_table_expression_clause (pivot_clause | unpivot_clause)?
      | '(' table_ref subquery_operation_part* ')' (pivot_clause | unpivot_clause)?
      | ONLY '(' dml_table_expression_clause ')'
      | dml_table_expression_clause (pivot_clause | unpivot_clause)?
      | oracle_functions) //NEW ADDITION
    flashback_query_clause* (/*{isTableAlias()}?*/ table_alias)?
    ;

join_clause
    : query_partition_clause? (CROSS | NATURAL)? (INNER | outer_join_type)?
      JOIN table_ref_aux query_partition_clause? (join_on_part | join_using_part)*
    ;

join_on_part
    : ON condition
    ;

join_using_part
    : USING '(' column_name (',' column_name)* ')'
    ;

outer_join_type
    : (FULL | LEFT | RIGHT) OUTER?
    ;

query_partition_clause
    : PARTITION BY ('(' subquery ')' | expression_list | expression (',' expression)*)
    ;

flashback_query_clause
    : VERSIONS BETWEEN (SCN | TIMESTAMP) expression
    | AS OF (SCN | TIMESTAMP | SNAPSHOT) expression
    ;

pivot_clause
    : PIVOT XML? '(' pivot_element (',' pivot_element)* pivot_for_clause pivot_in_clause ')'
    ;

pivot_element
    : aggregate_function_name '(' expression ')' column_alias?
    ;

pivot_for_clause
    : FOR (column_name | '(' column_name (',' column_name)* ')')
    ;

pivot_in_clause
    : IN '(' (subquery | ANY (',' ANY)* | pivot_in_clause_element (',' pivot_in_clause_element)*) ')'
    ;

pivot_in_clause_element
    : pivot_in_clause_elements column_alias?
    ;

pivot_in_clause_elements
    : expression
    | expression_list
    ;

unpivot_clause
    : UNPIVOT ((INCLUDE | EXCLUDE) NULLS)?
    '(' (column_name | '(' column_name (',' column_name)* ')') pivot_for_clause unpivot_in_clause ')'
    ;

unpivot_in_clause
    : IN '(' unpivot_in_elements (',' unpivot_in_elements)* ')'
    ;

unpivot_in_elements
    : (column_name | '(' column_name (',' column_name)* ')')
      (AS (constant | '(' constant (',' constant)* ')'))?
    ;

hierarchical_query_clause
    : CONNECT BY NOCYCLE? condition start_part?
    | start_part CONNECT BY NOCYCLE? condition
    ;

start_part
    : START WITH condition
    ;

group_by_clause
    : GROUP BY group_by_elements (',' group_by_elements)* having_clause?
    | having_clause (GROUP BY group_by_elements (',' group_by_elements)*)?
    ;

group_by_elements
    : grouping_sets_clause
    | rollup_cube_clause
    | expression
    ;

rollup_cube_clause
    : (ROLLUP|CUBE) '(' grouping_sets_elements (',' grouping_sets_elements)* ')'
    ;

grouping_sets_clause
    : GROUPING SETS '(' grouping_sets_elements (',' grouping_sets_elements)* ')'
    ;

grouping_sets_elements
    : rollup_cube_clause
    | expression_list
    | expression
    ;

having_clause
    : HAVING condition
    ;

model_clause
    : MODEL cell_reference_options* return_rows_clause? reference_model* main_model
    ;

cell_reference_options
    : (IGNORE | KEEP) NAV
    | UNIQUE (DIMENSION | SINGLE REFERENCE)
    ;

return_rows_clause
    : RETURN (UPDATED | ALL) ROWS
    ;

reference_model
    : REFERENCE reference_model_name ON '(' subquery ')' model_column_clauses cell_reference_options*
    ;

main_model
    : (MAIN main_model_name)? model_column_clauses cell_reference_options* model_rules_clause
    ;

model_column_clauses
    : model_column_partition_part? DIMENSION BY model_column_list MEASURES model_column_list
    ;

model_column_partition_part
    : PARTITION BY model_column_list
    ;

model_column_list
    : '(' model_column (',' model_column)*  ')'
    ;

model_column
    : expression table_alias?
    ;

model_rules_clause
    : model_rules_part? '(' model_rules_element (',' model_rules_element)* ')'
    ;

model_rules_part
    : RULES (UPDATE | UPSERT ALL?)? ((AUTOMATIC | SEQUENTIAL) ORDER)? model_iterate_clause?
    ;

model_rules_element
    : (UPDATE | UPSERT ALL?)? cell_assignment order_by_clause? '=' expression
    ;

cell_assignment
    : model_expression
    ;

model_iterate_clause
    : ITERATE '(' expression ')' until_part?
    ;

until_part
    : UNTIL '(' condition ')'
    ;

order_by_clause
    : ORDER SIBLINGS? BY order_by_elements (',' order_by_elements)*
    ;

order_by_elements
    : expression (ASC | DESC)? (NULLS (FIRST | LAST))?
    ;

for_update_clause
    : FOR UPDATE for_update_of_part? for_update_options?
    ;

for_update_of_part
    : OF column_name (',' column_name)*
    ;

for_update_options
    : MYSKIP LOCKED
    | NOWAIT
    | WAIT expression
    ;

// $>

update_statement
    : UPDATE general_table_ref update_set_clause where_clause? static_returning_clause? error_logging_clause?
    ;

// $<Update - Specific Clauses
update_set_clause
    : SET
      (column_based_update_set_clause (',' column_based_update_set_clause)* | VALUE '(' id ')' '=' expression)
    ;

column_based_update_set_clause
    : column_name '=' expression
    | '(' column_name (',' column_name)* ')' '=' subquery
    ;

// $>

delete_statement
    : DELETE FROM? general_table_ref where_clause? static_returning_clause? error_logging_clause?
    ;

insert_statement
    : INSERT (single_table_insert | multi_table_insert)
    ;

// $<Insert - Specific Clauses

single_table_insert
    : insert_into_clause (values_clause static_returning_clause? | select_statement) error_logging_clause?
    ;

multi_table_insert
    : (ALL multi_table_element+ | conditional_insert_clause) select_statement
    ;

multi_table_element
    : insert_into_clause values_clause? error_logging_clause?
    ;

conditional_insert_clause
    : (ALL | FIRST)? conditional_insert_when_part+ conditional_insert_else_part?
    ;

conditional_insert_when_part
    : WHEN condition THEN multi_table_element+
    ;

conditional_insert_else_part
    : ELSE multi_table_element+
    ;

insert_into_clause
    : INTO general_table_ref ('(' column_name (',' column_name)* ')')?
    ;

values_clause
    : VALUES expression_list | VALUES record_name //NP: See test12.sql Added although the oracle syntax does not seem to allow it. Maybe it is syntactic sugar?
    ;

// $>
merge_statement
    : MERGE INTO tableview_name table_alias? USING selected_tableview ON '(' condition ')'
      (merge_update_clause merge_insert_clause? | merge_insert_clause merge_update_clause?)?
      error_logging_clause?
    ;

// $<Merge - Specific Clauses

merge_update_clause
    : WHEN MATCHED THEN UPDATE SET merge_element (',' merge_element)* where_clause? merge_update_delete_part?
    ;

merge_element
    : column_name '=' expression
    ;

merge_update_delete_part
    : DELETE where_clause
    ;

merge_insert_clause
    : WHEN NOT MATCHED THEN INSERT ('(' column_name (',' column_name)* ')')? VALUES expression_list where_clause?
    ;

selected_tableview
    : (tableview_name | '(' select_statement ')') table_alias?
    ;

// $>

lock_table_statement
    : LOCK TABLE lock_table_element (',' lock_table_element)* IN lock_mode MODE wait_nowait_part?
    ;

wait_nowait_part
    : WAIT expression
    | NOWAIT
    ;

// $<Lock - Specific Clauses

lock_table_element
    : tableview_name partition_extension_clause?
    ;

lock_mode
    : ROW SHARE
    | ROW EXCLUSIVE
    | SHARE UPDATE?
    | SHARE ROW EXCLUSIVE
    | EXCLUSIVE
    ;

// $<Common DDL Clauses

general_table_ref
    : (dml_table_expression_clause | ONLY '(' dml_table_expression_clause ')') table_alias?
    ;

static_returning_clause
    : (RETURNING | RETURN) expression (',' expression)* into_clause
    ;

error_logging_clause
    : LOG ERRORS error_logging_into_part? expression_wrapper? error_logging_reject_part?
    ;

error_logging_into_part
    : INTO tableview_name
    ;

error_logging_reject_part
    : REJECT LIMIT (UNLIMITED | expression_wrapper)
    ;

dml_table_expression_clause
    : table_collection_expression
    | '(' select_statement subquery_restriction_clause? ')'
    | tableview_name sample_clause?
    ;

table_collection_expression
    : (TABLE | THE) ('(' subquery ')' | '(' expression ')' ('(' '+' ')')?)
    ;

subquery_restriction_clause
    : WITH (READ ONLY | CHECK OPTION (CONSTRAINT constraint_name)?)
    ;

sample_clause
    : SAMPLE BLOCK? '(' expression (',' expression)? ')' seed_part?
    ;

seed_part
    : SEED '(' expression ')'
    ;

// $>

// $<Expression & Condition
cursor_expression
    : CURSOR '(' subquery ')'
    ;

expression_list
    : '(' expression? (',' expression)* ')'
    ;

condition
    : expression
    ;

condition_wrapper
    : expression
    ;

expression
    : cursor_expression
    | logical_and_expression ( OR logical_and_expression )*
    ;

expression_wrapper
    : expression
    ;

logical_and_expression
    : negated_expression ( AND negated_expression )*
    ;

negated_expression
    : NOT negated_expression
    | equality_expression
    | other_boolean_expression //NP: Added Test 4
    ;

other_boolean_expression: (cursor_name | SQL) '%' ( FOUND | ISOPEN | NOTFOUND );


equality_expression
    : multiset_expression (IS NOT?
      (NULL | NAN | PRESENT | INFINITE | A_LETTER SET | EMPTY | OF TYPE? '(' ONLY? type_spec (',' type_spec)* ')'))*
    ;


multiset_expression
    : relational_expression (multiset_type OF? concatenation)?
    ;

multiset_type
    : MEMBER
    | SUBMULTISET
    ;

relational_expression
    : compound_expression
      (( '=' | not_equal_op | '<' | '>' | less_than_or_equals_op | greater_than_or_equals_op ) compound_expression)*
    ;

compound_expression
    : concatenation
      (NOT? (IN in_elements | BETWEEN between_elements | like_type concatenation like_escape_part?))?
    ;

like_type
    : LIKE
    | LIKEC
    | LIKE2
    | LIKE4
    ;

like_escape_part
    : ESCAPE concatenation
    ;

in_elements
    : '(' subquery ')'
    | '(' concatenation_wrapper (',' concatenation_wrapper)* ')'
    | constant
    | bind_variable
    | general_element
    ;

between_elements
    : concatenation AND concatenation
    ;

concatenation
    : additive_expression (concatenation_op additive_expression)*
    ;

concatenation_wrapper
    : concatenation
    ;

additive_expression
    : multiply_expression (('+' | '-') multiply_expression)*
    ;

multiply_expression
    : datetime_expression (('*' | '/' | MOD) datetime_expression)* //NP: Added for test13.sql
    ;

datetime_expression
    : model_expression
      (AT (LOCAL | TIME ZONE concatenation_wrapper) | interval_expression)?
    ;

interval_expression
    : DAY ('(' concatenation_wrapper ')')? TO SECOND ('(' concatenation_wrapper ')')?
    | YEAR ('(' concatenation_wrapper ')')? TO MONTH
    ;

model_expression
    : unary_expression ('[' model_expression_element ']')?
    ;

model_expression_element
    : (ANY | condition_wrapper) (',' (ANY | condition_wrapper))*
    | single_column_for_loop (',' single_column_for_loop)*
    | multi_column_for_loop
    ;

single_column_for_loop
    : FOR column_name
      (IN expression_list | for_like_part? FROM ex1=expression TO ex2=expression for_increment_decrement_type ex3=expression)
    ;

for_like_part
    : LIKE expression
    ;

for_increment_decrement_type
    : INCREMENT
    | DECREMENT
    ;

multi_column_for_loop
    : FOR
      '(' column_name (',' column_name)* ')' IN '(' (subquery | '(' expression_list (',' expression_list)* ')') ')'
    ;

unary_expression
    : '-' unary_expression
    | '+' unary_expression
    | PRIOR unary_expression
    | CONNECT_BY_ROOT unary_expression
    | /*TODO {input.LT(1).getText().equalsIgnoreCase("new") && !input.LT(2).getText().equals(".")}?*/ NEW unary_expression
    |  DISTINCT unary_expression
    |  ALL unary_expression
    |  /*TODO{(input.LA(1) == SQL92_RESERVED_CASE || input.LA(2) == SQL92_RESERVED_CASE)}?*/ case_statement/*[false]*/
    |  quantified_expression
    |  standard_function
    |  atom
    ;

case_statement /*TODO [boolean isStatementParameter]
TODO scope    {
    boolean isStatement;
}
@init    {$case_statement::isStatement = $isStatementParameter;}*/
    : searched_case_statement
    | simple_case_statement
    ;

// $<CASE - Specific Clauses

simple_case_statement
    : label_name? ck1=CASE atom simple_case_when_part+  case_else_part? END CASE? label_name?
    ;

simple_case_when_part
    : WHEN expression_wrapper THEN (/*TODO{$case_statement::isStatement}?*/ seq_of_statements | expression_wrapper)
    ;

searched_case_statement
    : label_name? ck1=CASE searched_case_when_part+ case_else_part? END CASE? label_name?
    ;

searched_case_when_part
    : WHEN condition_wrapper THEN (/*TODO{$case_statement::isStatement}?*/ seq_of_statements | expression_wrapper)
    ;

case_else_part
    : ELSE (/*{$case_statement::isStatement}?*/ seq_of_statements | expression_wrapper)
    ;
// $>

atom
    : table_element outer_join_sign
    | bind_variable
    | (schema_name '.')? table_var_name '.' ( EXISTS '(' expression ')' | COUNT | FIRST | LAST |PRIOR '(' expression ')' | NEXT '(' expression ')') //NP: Added see test5.sql
    | (cursor_name | SQL) '%' ROWCOUNT //NP: Added test9.sql Added SQL for test10.sql
    | constant
    | general_element
    | '(' (subquery ')' subquery_operation_part* | expression_or_vector ')')
    | oracle_functions //NEW ADDITION maybe...
    ;

expression_or_vector
    : expression (vector_expr)?
    ;

vector_expr
    : ',' expression (',' expression)*
    ;

quantified_expression
    : (SOME | EXISTS | ALL | ANY) ('(' subquery ')' | '(' expression_wrapper ')')
    ;

standard_function
    : over_clause_keyword function_argument_analytic over_clause?
    | /*TODO stantard_function_enabling_using*/ regular_id function_argument_modeling using_clause?
    | MOD '(' additive_expression',' additive_expression ')' //NP: Added to handle MOD operator
    | COUNT '(' ( '*' | (DISTINCT | UNIQUE | ALL)? concatenation_wrapper) ')' over_clause?
    | (CAST | XMLCAST) '(' (MULTISET '(' subquery ')' | concatenation_wrapper) AS type_spec ')'
    | CHR '(' concatenation_wrapper USING NCHAR_CS ')'
    | COLLECT '(' (DISTINCT | UNIQUE)? concatenation_wrapper collect_order_by_part? ')'
    | within_or_over_clause_keyword function_argument within_or_over_part+
    | DECOMPOSE '(' concatenation_wrapper (CANONICAL | COMPATIBILITY)? ')'
    | EXTRACT '(' regular_id FROM concatenation_wrapper ')'
    | (FIRST_VALUE | LAST_VALUE) function_argument_analytic respect_or_ignore_nulls? over_clause
    | standard_prediction_function_keyword
      '(' expression_wrapper (',' expression_wrapper)* cost_matrix_clause? using_clause? ')'
    | TRANSLATE '(' expression_wrapper (USING (CHAR_CS | NCHAR_CS))? (',' expression_wrapper)* ')'
    | TREAT '(' expression_wrapper AS REF? type_spec ')'
    | TRIM '(' ((LEADING | TRAILING | BOTH)? quoted_string? FROM)? concatenation_wrapper ')'
    | XMLAGG '(' expression_wrapper order_by_clause? ')' ('.' general_element_part)?
    | (XMLCOLATTVAL|XMLFOREST)
      '(' xml_multiuse_expression_element (',' xml_multiuse_expression_element)* ')' ('.' general_element_part)?
    | XMLELEMENT
      '(' (ENTITYESCAPING | NOENTITYESCAPING)? (NAME | EVALNAME)? expression_wrapper
       (/*TODO{input.LT(2).getText().equalsIgnoreCase("xmlattributes")}?*/ ',' xml_attributes_clause)?
       (',' expression_wrapper column_alias?)* ')' ('.' general_element_part)?
    | XMLEXISTS '(' expression_wrapper xml_passing_clause? ')'
    | XMLPARSE '(' (DOCUMENT | CONTENT) concatenation_wrapper WELLFORMED? ')' ('.' general_element_part)?
    | XMLPI
      '(' (NAME id | EVALNAME concatenation_wrapper) (',' concatenation_wrapper)? ')' ('.' general_element_part)?
    | XMLQUERY
      '(' concatenation_wrapper xml_passing_clause? RETURNING CONTENT (NULL ON EMPTY)? ')' ('.' general_element_part)?
    | XMLROOT
      '(' concatenation_wrapper xmlroot_param_version_part (',' xmlroot_param_standalone_part)? ')' ('.' general_element_part)?
    | XMLSERIALIZE
      '(' (DOCUMENT | CONTENT) concatenation_wrapper (AS type_spec)?
      xmlserialize_param_enconding_part? xmlserialize_param_version_part? xmlserialize_param_ident_part? ((HIDE | SHOW) DEFAULTS)? ')'
      ('.' general_element_part)?
    | XMLTABLE
      '(' xml_namespaces_clause? concatenation_wrapper xml_passing_clause? (COLUMNS xml_table_column (',' xml_table_column))? ')' ('.' general_element_part)?
    ;

over_clause_keyword
    : AVG
    | CORR
    | LAG
    | LEAD
    | MAX
    | MEDIAN
    | MIN
    | NTILE
    | RATIO_TO_REPORT
    | ROW_NUMBER
    | SUM
    | VARIANCE
    | REGR_
    | STDDEV
    | VAR_
    | COVAR_
    ;

/*TODO
stantard_function_enabling_using
    : {enablesUsingClause(input.LT(1).getText())}? REGULAR_ID
    ;
*/

within_or_over_clause_keyword
    : CUME_DIST
    | DENSE_RANK
    | LISTAGG
    | PERCENT_RANK
    | PERCENTILE_CONT
    | PERCENTILE_DISC
    | RANK
    ;

standard_prediction_function_keyword
    : PREDICTION
    | PREDICTION_BOUNDS
    | PREDICTION_COST
    | PREDICTION_DETAILS
    | PREDICTION_PROBABILITY
    | PREDICTION_SET
    ;

over_clause
    : OVER '(' query_partition_clause? (order_by_clause windowing_clause?)? ')'
    ;

windowing_clause
    : windowing_type
      (BETWEEN windowing_elements AND windowing_elements | windowing_elements)
    ;

windowing_type
    : ROWS
    | RANGE
    ;

windowing_elements
    : UNBOUNDED PRECEDING
    | CURRENT ROW
    | concatenation_wrapper (PRECEDING|FOLLOWING)
    ;

using_clause
    : USING ('*' | using_element (',' using_element)*)
    ;

using_element
    : (IN OUT? | OUT)? select_list_elements column_alias?
    ;

collect_order_by_part
    : ORDER BY concatenation_wrapper
    ;

within_or_over_part
    : WITHIN GROUP '(' order_by_clause ')'
    | over_clause
    ;

cost_matrix_clause
    : COST (MODEL AUTO? | '(' cost_class_name (',' cost_class_name)* ')' VALUES expression_list)
    ;

xml_passing_clause
    : PASSING (BY VALUE)? expression_wrapper column_alias? (',' expression_wrapper column_alias?)* //* was missing
    ;

xml_attributes_clause
    : XMLATTRIBUTES
     '(' (ENTITYESCAPING | NOENTITYESCAPING)? (SCHEMACHECK | NOSCHEMACHECK)?
     xml_multiuse_expression_element (',' xml_multiuse_expression_element)* ')'
    ;

xml_namespaces_clause
    : XMLNAMESPACES
      '(' (concatenation_wrapper column_alias)? (',' concatenation_wrapper column_alias)*
      xml_general_default_part? ')'
    ;

xml_table_column
    : xml_column_name
      (FOR ORDINALITY | type_spec (PATH concatenation_wrapper)? xml_general_default_part?)
    ;

xml_general_default_part
    : DEFAULT concatenation_wrapper
    ;

xml_multiuse_expression_element
    : expression (AS (id_expression | EVALNAME concatenation))?
    ;

xmlroot_param_version_part
    : VERSION (NO VALUE | expression_wrapper)
    ;

xmlroot_param_standalone_part
    : STANDALONE (YES | NO VALUE?)
    ;

xmlserialize_param_enconding_part
    : ENCODING concatenation_wrapper
    ;

xmlserialize_param_version_part
    : VERSION concatenation_wrapper
    ;

xmlserialize_param_ident_part
    : NO INDENT
    | INDENT (SIZE '=' concatenation_wrapper)?
    ;

// SqlPlus

sql_plus_command
    : ('/' | whenever_command | exit_command | prompt_command | set_command) ';'?
    ;

whenever_command
    : WHENEVER (SQLERROR | OSERROR)
      (EXIT (SUCCESS | FAILURE | WARNING) (COMMIT | ROLLBACK) | CONTINUE (COMMIT|ROLLBACK|NONE))
    ;

set_command
    : SET regular_id (CHAR_STRING | ON | OFF | /*EXACT_NUM_LIT*/numeric | regular_id) // TODO
    ;

exit_command
    : EXIT
    ;

prompt_command
    : PROMPT
    ;

// Common

partition_extension_clause
    : (SUBPARTITION | PARTITION) FOR? expression_list
    ;

column_alias
    : AS? (id | alias_quoted_string)
    | AS
    ;

table_alias
    : (id | alias_quoted_string)
    ;

alias_quoted_string
    : quoted_string
    ;

where_clause
    : WHERE (current_of_clause | condition_wrapper)
    ;

current_of_clause
    : CURRENT OF cursor_name
    ;

into_clause
    : INTO (variable_name (',' variable_name)* | //NP: Changes. see test6.sql
            (schema_name '.')? table_var_name '(' expression ')' .id) //NP: Added see test5.sql


    | BULK COLLECT INTO variable_name (',' variable_name)*
    ;

// $>

// $<Common PL/SQL Named Elements

xml_column_name
    : id
    | quoted_string
    ;

cost_class_name
    : id
    ;

attribute_name
    : id
    ;

savepoint_name
    : id
    ;

rollback_segment_name
    : id
    ;

table_var_name
    : id
    ;

schema_name
    : id
    ;

routine_name
    : id {
          controlCore.registerItemForDistribution($id.text);
         } ('.' id_expression {
                               controlCore.registerItemForDistribution($id_expression.text);
                              } )* {
                                    controlCore.distributeItems();
                                    controlCore.factory(ItemField.LINK_NAME,ItemField.LINK_NAME.getDefault());
                                   } ('@' link_name {
                                                    controlCore.factory(ItemField.LINK_NAME,$link_name.text);
                                                   } )?
    ;

package_name
    : id
    ;

implementation_type_name
    : id ('.' id_expression)?
    ;

parameter_name
    : id
    ;

reference_model_name
    : id
    ;

main_model_name
    : id
    ;

aggregate_function_name
    : id ('.' id_expression)*
    ;

query_name
    : id
    ;

constraint_name
    : id ('.' id_expression)* ('@' link_name)?
    ;

label_name
    : id_expression
    ;

type_name
    : id_expression ('.' id_expression)*
    ;

sequence_name
    : id_expression ('.' id_expression)*
    ;

exception_name
    : id ('.' id_expression)*
    ;

function_name
    : id //('.' id_expression)?
    ;

procedure_name
    : id //('.' id_expression)?
    ;

trigger_name
    : id //('.' id_expression)?
    ;

variable_name
    : (INTRODUCER char_set_name)? id_expression ('.' id_expression)?
    | bind_variable
    ;

index_name
    : id
    ;

cursor_name
    : id
    | bind_variable
    ;

record_name
    : id
    | bind_variable
    ;

collection_name
    : id ('.' id_expression)? ('.' id_expression)? //NP: Changed. See test3 to include lined db
    ;

link_name
    : id
    ;

column_name
    : id ('.' id_expression)* ('(' (id ('.' id_expression)*)? ')')? // NEW ADDITION ->not sure if it contains something
    ;

tableview_name
    : //id ('.' id_expression)?
      {
       controlCore.factory(ItemField.FROM_CLAUSE_SCHEMA, ItemField.FROM_CLAUSE_SCHEMA.getDefault(), Boolean.TRUE);
      }
      (schema_name {
                   controlCore.factory(ItemField.FROM_CLAUSE_SCHEMA, $schema_name.text, Boolean.TRUE);
                   } '.')? id {
                               controlCore.factory(ItemField.FROM_CLAUSE_NAME, $id.text, Boolean.TRUE);
                               controlCore.factory(ItemField.FROM_CLAUSE_LINK, ItemField.FROM_CLAUSE_LINK.getDefault(), Boolean.TRUE);
                              }  //reversed the rule
      ('@' link_name {
                       controlCore.factory(ItemField.FROM_CLAUSE_LINK, $link_name.text, Boolean.TRUE);
                     } | /*TODO{!(input.LA(2) == SQL92_RESERVED_BY)}?*/ partition_extension_clause)?
    ;

char_set_name
    : id_expression ('.' id_expression)*
    ;

// $>

// $<Common PL/SQL Specs

// NOTE: In reality this applies to aggregate functions only
keep_clause
    : KEEP '(' DENSE_RANK (FIRST | LAST) order_by_clause ')' over_clause?
    ;

function_argument
    : '(' argument? (',' argument )* ')' keep_clause?
    ;

function_argument_analytic
    : '(' (argument respect_or_ignore_nulls?)? (',' argument respect_or_ignore_nulls?)* ')' keep_clause?
    ;

function_argument_modeling
    : '(' column_name (',' (numeric | NULL) (',' (numeric | NULL))?)?
      USING (tableview_name '.' '*' | '*' | expression column_alias? (',' expression column_alias?)*)
      ')' keep_clause?
    ;

respect_or_ignore_nulls
    : (RESPECT | IGNORE) NULLS
    ;

argument
    : (id '=' '>')? expression_wrapper
    ;

type_spec
    : datatype
    | REF? type_name (PERCENT_ROWTYPE | PERCENT_TYPE)?
    ;

datatype
    : native_datatype_element precision_part? (WITH LOCAL? TIME ZONE)?
    | collection_name '%' TYPE // NP: Added //See Test1.sql
    | db_table_name '%' ROWTYPE //NP: Added //See Test1.sql
    | INTERVAL (YEAR | DAY) ('(' expression_wrapper ')')? TO (MONTH | SECOND) ('(' expression_wrapper ')')?
    ;

db_table_name: (schema_name '.')? table_var_name // NP: Added //See Test1.sql
               | cursor_name// NP: Added //See Test1.sql
            ;

precision_part
    : '(' numeric (',' numeric)? (CHAR | BYTE)? ')'
    ;

native_datatype_element
    : BINARY_INTEGER
    | PLS_INTEGER
    | NATURAL
    | BINARY_FLOAT
    | BINARY_DOUBLE
    | NATURALN
    | POSITIVE
    | POSITIVEN
    | SIGNTYPE
    | SIMPLE_INTEGER
    | NVARCHAR2
    | DEC
    | INTEGER
    | INT
    | NUMERIC
    | SMALLINT
    | NUMBER
    | DECIMAL
    | DOUBLE PRECISION?
    | FLOAT
    | REAL
    | NCHAR
    | LONG RAW?
    | CHAR
    | CHARACTER
    | VARCHAR2
    | VARCHAR
    | STRING
    | RAW
    | BOOLEAN
    | DATE
    | ROWID
    | UROWID
    | YEAR
    | MONTH
    | DAY
    | HOUR
    | MINUTE
    | SECOND
    | TIMEZONE_HOUR
    | TIMEZONE_MINUTE
    | TIMEZONE_REGION
    | TIMEZONE_ABBR
    | TIMESTAMP
    | TIMESTAMP_UNCONSTRAINED
    | TIMESTAMP_TZ_UNCONSTRAINED
    | TIMESTAMP_LTZ_UNCONSTRAINED
    | YMINTERVAL_UNCONSTRAINED
    | DSINTERVAL_UNCONSTRAINED
    | BFILE
    | BLOB
    | CLOB
    | NCLOB
    | MLSLABEL
    ;

bind_variable
    : (BINDVAR | ':' UNSIGNED_INTEGER)
      (INDICATOR? (BINDVAR | ':' UNSIGNED_INTEGER))?
      ('.' general_element_part)*
    ;

general_element
    : general_element_part ('.' general_element_part)*
    ;

general_element_part
    : (INTRODUCER char_set_name)? id_expression
      ('.' id_expression)* function_argument?
    ;

table_element
    : (INTRODUCER char_set_name)? id_expression ('.' id_expression)*
    ;

// $>

// $<Lexer Mappings

constant
    : TIMESTAMP (quoted_string | bind_variable) (AT TIME ZONE quoted_string)?
    | INTERVAL (quoted_string | bind_variable | general_element_part)
      (DAY | HOUR | MINUTE | SECOND)
      ('(' (UNSIGNED_INTEGER | bind_variable) (',' (UNSIGNED_INTEGER | bind_variable) )? ')')?
      (TO ( DAY | HOUR | MINUTE | SECOND ('(' (UNSIGNED_INTEGER | bind_variable) ')')?))?
    | numeric
    | DATE quoted_string
    | quoted_string
    | NULL
    | TRUE
    | FALSE
    | DBTIMEZONE
    | SESSIONTIMEZONE
    | MINVALUE
    | MAXVALUE
    | DEFAULT
    ;

numeric
    : UNSIGNED_INTEGER
    | APPROXIMATE_NUM_LIT
    ;

error_code:
              '-' UNSIGNED_INTEGER //NP: Added for test7.sql
          | UNSIGNED_INTEGER //NP: Added for test8.sql
          ;
quoted_string
    : CHAR_STRING
    //| CHAR_STRING_PERL
    | NATIONAL_CHAR_STRING_LIT
    ;

id
    : (INTRODUCER char_set_name)? id_expression
    ;

id_expression
    : regular_id
    | DELIMITED_ID
    ;

not_equal_op
    : NOT_EQUAL_OP
    | '<' '>'
    | '!' '='
    | '^' '='
    ;

greater_than_or_equals_op
    : '>='
    | '>' '='
    ;

less_than_or_equals_op
    : '<='
    | '<' '='
    ;

concatenation_op
    : '||'
    | '|' '|'
    ;

outer_join_sign
    : '(' '+' ')'
    ;

regular_id_tester
    : regular_id* EOF
    ;

SIZE_LETTER : K | M | G | T | P | E;//not working while the implict tokens are active
//SIZE_LETTER : 'k' | 'K'; //not working while the implict tokens are active
//size_letter : 'k' | 'K';

regular_id
    : REGULAR_ID
    // | A_LETTER
    |  ACCESS
    |  ADD
    |  ADMIN
    |  ADMINISTER
    |  ADVISOR
    |  AFTER
    |  AGENT
    |  AGGREGATE
    |  ALIAS
    |  ALL
    |  ALLOW
    |  ALTER
    |  ALWAYS
    |  ANALYZE
    |  AND
    |  ANY
    |  ANYSCHEMA
    |  AQ_ADMINISTRATOR_ROLE
    |  AQ_USER_ROLE
    |  ARCHIVE
    |  ARRAY
    |  AS
    |  ASC
    |  ASSOCIATE
    |  ASYNC
    |  ASYNCHRONOUS
    |  AT
    |  ATTRIBUTE
    |  AUDIT
    |  AUTHID
    |  AUTHORIZATION
    |  AUTO
    |  AUTOMATIC
    |  AUTONOMOUS_TRANSACTION
    |  AVG
    |  BACKUP
    |  BASICFILE
    |  BATCH
    |  BECOME
    |  BEFORE
    |  BEGIN
    |  BEGINNING
    |  BETWEEN
    |  BFILE
    |  BINARY
    |  BINARY_DOUBLE
    |  BINARY_FLOAT
    |  BINARY_INTEGER
    |  BITMAP
    |  BLOB
    |  BLOCK
    |  BODY
    |  BOOLEAN
    |  BOTH
    |  BREADTH
    |  BUFFER_POOL
    |  BUILD
    |  BULK
    |  BY
    |  BYTE
    |  CACHE
    |  CALL
    |  CANONICAL
    |  CASCADE
    |  CASE
    |  CAST
    |  CHANGE
    |  CHAR
    |  CHARACTER
    |  CHAR_CS
    |  CHECK
    |  CHR
    |  CHUNK
    |  CLASS
    |  CLOB
    |  CLOSE
    |  CLUSTER
    |  COLLECT
    |  COLUMN
    |  COLUMNS
    |  COLUMN_VALUE
    |  COMMENT
    |  COMMIT
    |  COMMITTED
    |  COMPATIBILITY
    |  COMPILE
    |  COMPLETE
    |  COMPOUND
    |  COMPRESS
    |  COMPUTE
    |  CONNECT
    |  CONNECT_BY_ROOT
    |  CONSTANT
    |  CONSTRAINT
    |  CONSTRAINTS
    |  CONSTRUCTOR
    |  CONTENT
    |  CONTEXT
    |  CONTINUE
    |  CONVERT
    |  CORR
    |  CORRUPT_XID
    |  CORRUPT_XID_ALL
    |  COST
    |  COUNT
    |  COVAR_
    |  CREATE
    |  CREATION
    |  CROSS
    |  CUBE
    |  CUME_DIST
    |  CURRENT
    |  CURRENT_USER
    |  CURSOR
    |  CUSTOMDATUM
    |  CYCLE
    |  DATA
    |  DATABASE
    |  DATE
    |  DAY
    |  DBA
    |  DBTIMEZONE
    |  DB_ROLE_CHANGE
    |  DDL
    |  DEBUG
    |  DEC
    |  DECIMAL
    |  DECLARE
    |  DECOMPOSE
    |  DECREMENT
    |  DECRYPT
    |  DEDUBLICATE
    |  DEFAULT
    |  DEFAULTS
    |  DEFERRABLE
    |  DEFERRED
    |  DEFINER
    |  DELETE
    |  DELETE_CATALOG_ROLE
    |  DEMAND
    |  DENSE_RANK
    |  DEPTH
    |  DESC
    |  DETERMINISTIC
    |  DICTIONARY
    |  DIMENSION
    |  DIRECTORY
    |  DIRECT_LOAD
    |  DISABLE
    |  DISALLOW
    |  DISASSOCIATE
    |  DISTINCT
    |  DOCUMENT
    |  DOUBLE
    |  DROP
    |  DSINTERVAL_UNCONSTRAINED
    |  EACH
    |  EDITION
    |  ELEMENT
    |  ELSE
    |  ELSIF
    |  EMPTY
    |  ENABLE
    |  ENCODING
    |  ENCRYPT
    |  END
    |  ENFORCED
    |  ENTITYESCAPING
    |  ERRORS
    |  ESCAPE
    |  EVALNAME
    |  EVALUATE
    |  EVERY
    |  EXCEPTION
    |  EXCEPTIONS
    |  EXCEPTION_INIT
    |  EXCLUDE
    |  EXCLUDING
    |  EXCLUSIVE
    |  EXECUTE
    |  EXECUTE_CATALOG_ROLE
    |  EXEMPT
    |  EXISTS
    |  EXIT
    |  EXPLAIN
    |  EXP_FULL_DATABASE
    |  EXTERNAL
    |  EXTRACT
    |  FAILURE
    |  FALSE
    |  FAST
    |  FETCH
    |  FILESYSTEM_LIKE_LOGGING
    |  FINAL
    |  FIRST
    |  FIRST_VALUE
    |  FLASHBACK
    |  FLOAT
    |  FOLDER
    |  FOLLOWING
    |  FOLLOWS
    |  FOR
    |  FORALL
    |  FORCE
    |  FOREIGN
    |  FOUND
    |  FREELIST
    |  FREELISTS
    |  FREEPOOLS
    |  FROM
    |  FULL
    |  FUNCTION
    |  GENERATED
    |  GLOBAL
    |  GOTO
    |  GRANT
    |  GROUP
    |  GROUPING
    |  GROUPS
    |  HASH
    |  HAVING
    |  HEAP
    |  HIDE
    |  HIERARCHY
    |  HIGH
    |  HOUR
    |  ID
    |  IDENTIFIED
    |  IDENTIFIER
    |  IF
    |  IGNORE
    |  IMMEDIATE
    |  IMP_FULL_DATABASE
    |  IN
    |  INCLUDE
    |  INCLUDING
    |  INCREMENT
    |  INDENT
    |  INDEX
    |  INDEXED
    |  INDEXTYPE
    |  INDEX_ALL_PATHS
    |  INDICATOR
    |  INDICES
    |  INFINITE
    |  INITIAL
    |  INITIALLY
    |  INITRANS
    |  INLINE
    |  INNER
    |  INOUT
    |  INSERT
    |  INSTANTIABLE
    |  INSTEAD
    |  INT
    |  INTEGER
    |  INTERSECT
    |  INTERVAL
    |  INTO
    |  INVALIDATE
    |  IS
    |  ISOLATION
    |  ISOPEN
    |  ITERATE
    |  JAVA
    |  JOB
    |  JOIN
    |  KEEP
    |  KEEP_DUBLICATES
    |  KEY
    |  LAG
    |  LANGUAGE
    |  LAST
    |  LAST_VALUE
    |  LEAD
    |  LEADING
    |  LEFT
    |  LESS
    |  LEVEL
    |  LEVELS
    |  LIBRARY
    |  LIKE
    |  LIKE2
    |  LIKE4
    |  LIKEC
    |  LIMIT
    |  LINK
    |  LIST
    |  LISTAGG
    |  LOB
    |  LOCAL
    |  LOCATION
    |  LOCATOR
    |  LOCK
    |  LOCKED
    |  LOG
    |  LOGGING
    |  LOGOFF
    |  LOGON
    |  LONG
    |  LOOP
    |  MAIN
    |  MANAGE
    |  MANUAL
    |  MAP
    |  MAPPING
    |  MASTER
    |  MATCHED
    |  MATERIALIZED
    |  MAX
    |  MAXEXTENTS
    |  MAXVALUE
    |  MEASURE
    |  MEASURES
    |  MEDIAN
    |  MEDIUM
    |  MEMBER
    |  MERGE
    |  MIN
    |  MINEXTENTS
    |  MINING
    |  MINUS
    |  MINUTE
    |  MINVALUE
    |  MLSLABEL
    |  MOD
    |  MODE
    |  MODEL
    |  MODIFY
    |  MONTH
    |  MOVEMENT
    |  MULTISET
    |  MYSKIP
    |  NAME
    |  NAMESPACE
    |  NAN
    |  NATURAL
    |  NATURALN
    |  NAV
    |  NCHAR
    |  NCHAR_CS
    |  NCLOB
    |  NESTED
    |  NEVER
    |  NEW
    |  NEXT
    |  NO
    |  NOAUDIT
    |  NOCACHE
    |  NOCOMPRESS
    |  NOCOPY
    |  NOCYCLE
    |  NOENTITYESCAPING
    |  NOLOGGING
    |  NOMAPPING
    |  NOMAXVALUE
    |  NOMINVALUE
    |  NONE
    |  NONSCHEMA
    |  NOORDER
    |  NOPARALLEL
    |  NORELY
    |  NOROWDEPENDENCIES
    |  NOSCHEMACHECK
    |  NOSORT
    |  NOT
    |  NOTFOUND
    |  NOTIFICATION
    |  NOVALIDATE
    |  NOWAIT
    |  NTILE
    |  NULL
    |  NULLS
    |  NUMBER
    |  NUMERIC
    |  NVARCHAR2
    |  OBJECT
    |  OF
    |  OFF
    |  OID
    |  OIDINDEX
    |  OLD
    |  ON
    |  ONLINE
    |  ONLY
    |  OPEN
    |  OPERATIONS
    |  OPERATOR
    |  OPTIMAL
    |  OPTION
    |  OR
    |  ORADATA
    |  ORDER
    |  ORDINALITY
    |  ORGANIZATION
    |  OSERROR
    |  OUT
    |  OUTER
    |  OUTLINE
    |  OVER
    |  OVERFLOW
    |  OVERRIDING
    |  PACKAGE
    |  PARALLEL
    |  PARALLEL_ENABLE
    |  PARAMETERS
    |  PARENT
    |  PARTITION
    |  PARTITIONS
    |  PASSING
    |  PATH
    |  PATHS
    |  PCTFREE
    |  PCTINCREASE
    |  PCTTHRESHOLD
    |  PCTUSED
    |  PCTVERSION
    |  PERCENTILE_CONT
    |  PERCENTILE_DISC
    |  PERCENT_RANK
    |  PERCENT_ROWTYPE
    |  PERCENT_TYPE
    |  PIPE
    |  PIPELINED
    |  PIVOT
    |  PLAN
    |  PLS_INTEGER
    |  POLICY
    |  POSITIVE
    |  POSITIVEN
    |  PRAGMA
    |  PREBUILT 
    |  PRECEDES
    |  PRECEDING
    |  PRECISION
    |  PREDICTION
    |  PREDICTION_BOUNDS
    |  PREDICTION_COST
    |  PREDICTION_DETAILS
    |  PREDICTION_PROBABILITY
    |  PREDICTION_SET
    |  PRESENT
    |  PRESERVE
    |  PRIMARY
    |  PRIOR
    |  PRIVILEGE
    |  PRIVILEGES
    |  PROCEDURE
    |  PROCESS
    |  PROFILE
    |  PROGRAM
    |  PUBLIC
    |  PURGE
    |  QUERY
    |  RAISE
    |  RANGE
    |  RANK
    |  RATIO_TO_REPORT
    |  RAW
    |  READ
    |  READS
    |  REAL
    |  RECORD
    |  RECOVERY_CATALOG_OWNER
    |  RECYCLE
    |  REDUCED
    |  REF
    |  REFERENCE
    |  REFERENCES
    |  REFERENCING
    |  REFRESH
    |  REGR_
    |  REJECT
    |  RELATIONAL
    |  RELIES_ON
    |  RELY
    |  REMOVE
    |  RENAME
    |  REPEAT
    |  REPLACE
    |  RESOURCE
    |  RESPECT
    |  RESTRICTED
    |  RESTRICT_REFERENCES
    |  RESULT
    |  RESULT_CACHE
    |  RESUMABLE
    |  RETENTION
    |  RETURN
    |  RETURNING
    |  REUSE
    |  REVERSE
    |  REVOKE
    |  REWRITE
    |  RIGHT
    |  ROLE
    |  ROLLBACK
    |  ROLLUP
    |  ROUND
    |  ROW
    |  ROWCOUNT
    |  ROWDEPENDENCIES
    |  ROWID
    |  ROWS
    |  ROWTYPE
    |  ROW_NUMBER
    |  RULES
    |  SALT
    |  SAMPLE
    |  SAVE
    |  SAVEPOINT
    |  SCHEDULER
    |  SCHEMA
    |  SCHEMACHECK
    |  SCN
    |  SCOPE
    |  SEARCH
    |  SECOND
    |  SECUREFILE
    |  SEED
    |  SEGMENT
    |  SELECT
    |  SELECT_CATALOG_ROLE
    |  SELF
    |  SEQUENCE
    |  SEQUENTIAL
    |  SERIALIZABLE
    |  SERIALLY_REUSABLE
    |  SERVERERROR
    |  SESSION
    |  SESSIONTIMEZONE
    |  SET
    |  SETS
    |  SETTINGS
    |  SHARE
    |  SHOW
    |  SHUTDOWN
    |  SIBLINGS
    |  SIGNTYPE
    |  SIMPLE_INTEGER
    |  SINGLE
    |  SIZE
    |  SMALLINT
    |  SNAPSHOT
    |  SNMPAGENT
    |  SOME
    |  SORT
    |  SOURCE
    |  SPECIFICATION
    |  SQL
    |  SQLDATA
    |  SQLERROR
    |  STALE
    |  STANDALONE
    |  START
    |  STARTUP
    |  STATEMENT
    |  STATEMENT_ID
    |  STATIC
    |  STATISTICS
    |  STDDEV
    |  STORAGE
    |  STORE
    |  STRING
    |  SUBMULTISET
    |  SUBPARTITION
    |  SUBPARTITIONS
    |  SUBSTITUTABLE
    |  SUBTYPE
    |  SUCCESS
    |  SUM
    |  SUPPLEMENTAL
    |  SUSPEND
    |  SYNC
    |  SYNCHRONOUS
    |  SYNONYM
    |  SYSDATE
    |  SYSDBA
    |  SYSOPER
    |  SYSTEM
    |  TABLE
    |  TABLESPACE
    |  TEMPLATE
    |  TEMPORARY
    |  THAN
    |  THE
    |  THEN
    |  TIME
    |  TIMESTAMP
    |  TIMESTAMP_LTZ_UNCONSTRAINED
    |  TIMESTAMP_TZ_UNCONSTRAINED
    |  TIMESTAMP_UNCONSTRAINED
    |  TIMEZONE_ABBR
    |  TIMEZONE_HOUR
    |  TIMEZONE_MINUTE
    |  TIMEZONE_REGION
    |  TO
    |  TO_DATE
    |  TRAILING
    |  TRANSACTION
    |  TRANSLATE
    |  TREAT
    |  TRIGGER
    |  TRIM
    |  TRUE
    |  TRUNC
    |  TRUNCATE
    |  TRUSTED
    |  TUNING
    |  TYPE
    |  UNBOUNDED
    |  UNDER
    |  UNION
    |  UNIQUE
    |  UNLIMITED
    |  UNPIVOT
    |  UNTIL
    |  UNUSUABLE
    |  UPDATE
    |  UPDATED
    |  UPSERT
    |  UROWID
    |  USE
    |  USER
    |  USING
    |  VALIDATE
    |  VALUE
    |  VALUES
    |  VARCHAR
    |  VARCHAR2
    |  VARIABLE
    |  VARIANCE
    |  VARRAY
    |  VARYING
    |  VAR_
    |  VERSION
    |  VERSIONS
    |  VIEW
    |  VIRTUAL
    |  WAIT
    |  WARNING
    |  WELLFORMED
    |  WHEN
    |  WHENEVER
    |  WHERE
    |  WHILE
    |  WITH
    |  WITHIN
    |  WITHOUT
    |  WORK
    |  WRITE
    |  XDB
    |  XML
    |  XMLAGG
    |  XMLATTRIBUTES
    |  XMLCAST
    |  XMLCOLATTVAL
    |  XMLELEMENT
    |  XMLEXISTS
    |  XMLFOREST
    |  XMLINDEX
    |  XMLNAMESPACES
    |  XMLPARSE
    |  XMLPI
    |  XMLQUERY
    |  XMLROOT
    |  XMLSCHEMA
    |  XMLSCHEMAS
    |  XMLSERIALIZE
    |  XMLTABLE
    |  XMLTYPE
    |  YEAR
    |  YES
    |  YMINTERVAL_UNCONSTRAINED
    |  ZONE
    //| ALL
    //| ALTER
    //| AND
    //| ANY
    // | AS
    //| ASC
    //| BEGIN
    // | BETWEEN
    // | BREADTH
    // | BY
    // | C_LETTER
    // | CACHE
    //| CASE
    //| CHECK
    //| CONNECT
    //| CONNECT_BY_ROOT
    //| CREATE
    //| CURRENT
    //| DATE
    //| DECLARE
    //| DEFAULT
    // | DELETE
    // | DEPTH
    //| DESC
    //| DISTINCT
    //| DROP
    //| ELSE
    //| ELSIF
    //| END
    //| EXCLUSIVE
    //| EXISTS
    //| FALSE
    //| FETCH
    //| FOR
    // | FROM
    //| GOTO
    //| GRANT
    //| GROUP
    //| HAVING
    //| IF
    // | IN
    //| INDEX
    //| INSERT
    //| INTERSECT
    // | INTO
    //| IS
    // | LIKE
    //| LOCK
    //| MINUS
    //| MODE
    // | NOCACHE
    //| NOMAXVALUE
    //| NOMINVALUE
    // | NOORDER
    //| NOT
    //| NOWAIT
    // | NULL
    //| OF
    //| ON
    //| OPTION
    //| OR
    //| ORDER
    //| PERCENT_ROWTYPE
    //| PERCENT_TYPE
    //| PIVOT
    //| PRIOR
    //| PROCEDURE
    //| REVOKE
    // | SEARCH
    // | SELECT
    // | SEQUENCE
    //| SHARE
    //| SIZE
    //| START
    //| TABLE
    //| THE
    //| THEN
    //| TO
    //| TRUE
    //| UNION
    //| UNIQUE
    //| UNPIVOT
    //| UPDATE
    //| USING
    //| VALUES
    // | WHEN
    // | WHERE
    //| WITH
   ;

//none can match t
//T_SMALL : [t] | 't' | '\u0074';

//NEW ADDITION
//made this lexer rule for single letter problems
//CHAR_2: ['\u0069'..'\u0080'];

/*
// i made char_2 to fix the single letter problems
REGULAR_ID
    : (SIMPLE_LETTER) (SIMPLE_LETTER | '$' | '_' | '#' | '0'..'9')*
    ;
*/

A_LETTER:                     A;
ACCESS:                       A C C E S S;
ADD:                          A D D;
ADMIN:                        A D M I N;
ADMINISTER:                   A D M I N I S T E R;
ADVISOR:                      A D V I S O R;
AFTER:                        A F T E R;
AGENT:                        A G E N T;
AGGREGATE:                    A G G R E G A T E;
ALIAS:                        A L I A S;
ALL:                          A L L;
ALLOW:                        A L L O W;
ALTER:                        A L T E R;
ALWAYS:                       A L W A Y S;
ANALYZE:                      A N A L Y Z E;
AND:                          A N D;
ANY:                          A N Y;
ANYSCHEMA:                    A N Y S C H E M A;
AQ_ADMINISTRATOR_ROLE:        A Q '_' A D M I N I S T R A T O R '_' R O L E;
AQ_USER_ROLE:                 A Q '_' U S E R '_' R O L E;
ARCHIVE:                      A R C H I V E;
ARRAY:                        A R R A Y;
AS:                           A S;
ASC:                          A S C;
ASSOCIATE:                    A S S O C I A T E;
ASYNC:                        A S Y N C;
ASYNCHRONOUS:                 A S Y N C H R O N O U S;
AT:                           A T;
ATTRIBUTE:                    A T T R I B U T E;
AUDIT:                        A U D I T;
AUTHID:                       A U T H I D;
AUTHORIZATION:                A U T H O R I Z A T I O N;
AUTO:                         A U T O;
AUTOMATIC:                    A U T O M A T I C;
AUTONOMOUS_TRANSACTION:       A U T O N O M O U S '_' T R A N S A C T I O N;
AVG:                          A V G;
BACKUP:                       B A C K U P;
BASICFILE:                    B A S I C F I L E;
BATCH:                        B A T C H;
BECOME:                       B E C O M E;
BEFORE:                       B E F O R E;
BEGIN:                        B E G I N;
BEGINNING:                    B E G I N N I N G;
BETWEEN:                      B E T W E E N;
BFILE:                        B F I L E;
BINARY:                       B I N A R Y;
BINARY_DOUBLE:                B I N A R Y '_' D O U B L E;
BINARY_FLOAT:                 B I N A R Y '_' F L O A T;
BINARY_INTEGER:               B I N A R Y '_' I N T E G E R;
BITMAP:                       B I T M A P;
BLOB:                         B L O B;
BLOCK:                        B L O C K;
BODY:                         B O D Y;
BOOLEAN:                      B O O L E A N;
BOTH:                         B O T H;
BREADTH:                      B R E A D T H;
BUFFER_POOL:                  B U F F E R '_' P O O L;
BUILD:                        B U I L D;
BULK:                         B U L K;
BY:                           B Y;
BYTE:                         B Y T E;
C_LETTER:                     C;
CACHE:                        C A C H E;
CALL:                         C A L L;
CANONICAL:                    C A N O N I C A L;
CASCADE:                      C A S C A D E;
CASE:                         C A S E;
CAST:                         C A S T;
CHANGE:                       C H A N G E;
CHAR:                         C H A R;
CHAR_CS:                      C H A R '_' C S;
CHARACTER:                    C H A R A C T E R;
CHECK:                        C H E C K;
CHR:                          C H R;
CHUNK:                        C H U N K;
CLASS:                        C L A S S;
CLOB:                         C L O B;
CLOSE:                        C L O S E;
CLUSTER:                      C L U S T E R;
COLLECT:                      C O L L E C T;
COLUMN:                       C O L U M N;
COLUMN_VALUE:                 C O L U M N '_' V A L U E;
COLUMNS:                      C O L U M N S;
COMMENT:                      C O M M E N T;
COMMIT:                       C O M M I T;
COMMITTED:                    C O M M I T T E D;
COMPATIBILITY:                C O M P A T I B I L I T Y;
COMPILE:                      C O M P I L E;
COMPLETE:                     C O M P L E T E;
COMPOUND:                     C O M P O U N D;
COMPRESS:                     C O M P R E S S;
COMPUTE:                      C O M P U T E;
CONNECT:                      C O N N E C T;
CONNECT_BY_ROOT:              C O N N E C T '_' B Y '_' R O O T;
CONSTANT:                     C O N S T A N T;
CONSTRAINT:                   C O N S T R A I N T;
CONSTRAINTS:                  C O N S T R A I N T S;
CONSTRUCTOR:                  C O N S T R U C T O R;
CONTENT:                      C O N T E N T;
CONTEXT:                      C O N T E X T;
CONTINUE:                     C O N T I N U E;
CONVERT:                      C O N V E R T;
CORR:                         C O R R;
CORRUPT_XID:                  C O R R U P T '_' X I D;
CORRUPT_XID_ALL:              C O R R U P T '_' X I D '_' A L L;
COST:                         C O S T;
COUNT:                        C O U N T;
COVAR_:                       C O V A R '_';
CREATE:                       C R E A T E;
CREATION:                     C R E A T I O N;
CROSS:                        C R O S S;
CUBE:                         C U B E;
CUME_DIST:                    C U M E '_' D I S T;
CURRENT:                      C U R R E N T;
CURRENT_USER:                 C U R R E N T '_' U S E R;
CURSOR:                       C U R S O R;
CUSTOMDATUM:                  C U S T O M D A T U M;
CYCLE:                        C Y C L E;
DATA:                         D A T A;
DATABASE:                     D A T A B A S E;
DATE:                         D A T E;
DAY:                          D A Y;
DB_ROLE_CHANGE:               D B '_' R O L E '_' C H A N G E;
DBA:                          D B A;
DBTIMEZONE:                   D B T I M E Z O N E;
DDL:                          D D L;
DEBUG:                        D E B U G;
DEC:                          D E C;
DECIMAL:                      D E C I M A L;
DECLARE:                      D E C L A R E;
DECOMPOSE:                    D E C O M P O S E;
DECREMENT:                    D E C R E M E N T;
DECRYPT:                      D E C R Y P T;
DEDUBLICATE:                  D E D U B L I C A T E;
DEFAULT:                      D E F A U L T;
DEFAULTS:                     D E F A U L T S;
DEFERRABLE:                   D E F E R R A B L E;
DEFERRED:                     D E F E R R E D;
DEFINER:                      D E F I N E R;
DELETE:                       D E L E T E;
DELETE_CATALOG_ROLE:          D E L E T E '_' C A T A L O G '_' R O L E;
DEMAND:                       D E M A N D;
DENSE_RANK:                   D E N S E '_' R A N K;
DEPTH:                        D E P T H;
DESC:                         D E S C;
DETERMINISTIC:                D E T E R M I N I S T I C;
DICTIONARY:                   D I C T I O N A R Y;
DIMENSION:                    D I M E N S I O N;
DIRECT_LOAD:                  D I R E C T '_' L O A D;
DIRECTORY:                    D I R E C T O R Y;
DISABLE:                      D I S A B L E;
DISALLOW:                     D I S A L L O W;
DISASSOCIATE:                 D I S A S S O C I A T E;
DISTINCT:                     D I S T I N C T;
DOCUMENT:                     D O C U M E N T;
DOUBLE:                       D O U B L E;
DROP:                         D R O P;
DSINTERVAL_UNCONSTRAINED:     D S I N T E R V A L '_' U N C O N S T R A I N E D;
EACH:                         E A C H;
EDITION:                      E D I T I O N;
ELEMENT:                      E L E M E N T;
ELSE:                         E L S E;
ELSIF:                        E L S I F;
EMPTY:                        E M P T Y;
ENABLE:                       E N A B L E;
ENCODING:                     E N C O D I N G;
ENCRYPT:                      E N C R Y P T;
END:                          E N D;
ENFORCED:                     E N F O R C E D;
ENTITYESCAPING:               E N T I T Y E S C A P I N G;
ERRORS:                       E R R O R S;
ESCAPE:                       E S C A P E;
EVALNAME:                     E V A L N A M E;
EVALUATE:                     E V A L U A T E;
EVERY:                        E V E R Y;
EXCEPTION:                    E X C E P T I O N;
EXCEPTION_INIT:               E X C E P T I O N '_' I N I T;
EXCEPTIONS:                   E X C E P T I O N S;
EXCLUDE:                      E X C L U D E;
EXCLUDING:                    E X C L U D I N G;
EXCLUSIVE:                    E X C L U S I V E;
EXECUTE:                      E X E C U T E;
EXECUTE_CATALOG_ROLE:         E X E C U T E '_' C A T A L O G '_' R O L E;
EXEMPT:                       E X E M P T;
EXISTS:                       E X I S T S;
EXIT:                         E X I T;
EXP_FULL_DATABASE:            E X P '_' F U L L '_' D A T A B A S E;
EXPLAIN:                      E X P L A I N;
EXTERNAL:                     E X T E R N A L;
EXTRACT:                      E X T R A C T;
FAILURE:                      F A I L U R E;
FALSE:                        F A L S E;
FAST:                         F A S T;
FETCH:                        F E T C H;
FILESYSTEM_LIKE_LOGGING:      F I L E S Y S T E M '_' L I K E '_' L O G G I N G;
FINAL:                        F I N A L;
FIRST:                        F I R S T;
FIRST_VALUE:                  F I R S T '_' V A L U E;
FLASHBACK:                    F L A S H B A C K;
FLOAT:                        F L O A T;
FOLDER:                       F O L D E R;
FOLLOWING:                    F O L L O W I N G;
FOLLOWS:                      F O L L O W S;
FOR:                          F O R;
FORALL:                       F O R A L L;
FORCE:                        F O R C E;
FOREIGN:                      F O R E I G N;
FOUND:                        F O U N D;    //NP: ADDED TEST 4
FREELIST:                     F R E E L I S T;
FREELISTS:                    F R E E L I S T S;
FREEPOOLS:                    F R E E P O O L S;
FROM:                         F R O M;
FULL:                         F U L L;
FUNCTION:                     F U N C T I O N;
GENERATED:                    G E N E R A T E D;
GLOBAL:                       G L O B A L;
GOTO:                         G O T O;
GRANT:                        G R A N T;
GROUP:                        G R O U P;
GROUPING:                     G R O U P I N G;
GROUPS:                       G R O U P S;
HASH:                         H A S H;
HAVING:                       H A V I N G;
HEAP:                         H E A P;
HIDE:                         H I D E;
HIERARCHY:                    H I E R A R C H Y;
HIGH:                         H I G H;
HOUR:                         H O U R;
ID:                           I D;
IDENTIFIED:                   I D E N T I F I E D;
IDENTIFIER:                   I D E N T I F I E R;
IF:                           I F;
IGNORE:                       I G N O R E;
IMMEDIATE:                    I M M E D I A T E;
IMP_FULL_DATABASE:            I M P '_' F U L L '_' D A T A B A S E;
IN:                           I N;
INCLUDE:                      I N C L U D E;
INCLUDING:                    I N C L U D I N G;
INCREMENT:                    I N C R E M E N T;
INDENT:                       I N D E N T;
INDEX:                        I N D E X;
INDEX_ALL_PATHS:              I N D E X '_' A L L '_' P A T H S;
INDEXED:                      I N D E X E D;
INDEXTYPE:                    I N D E X T Y P E;
INDICATOR:                    I N D I C A T O R;
INDICES:                      I N D I C E S;
INFINITE:                     I N F I N I T E;
INITIAL:                      I N I T I A L;
INITIALLY:                    I N I T I A L L Y;
INITRANS:                     I N I T R A N S;
INLINE:                       I N L I N E;
INNER:                        I N N E R;
INOUT:                        I N O U T;
INSERT:                       I N S E R T;
INSTANTIABLE:                 I N S T A N T I A B L E;
INSTEAD:                      I N S T E A D;
INT:                          I N T;
INTEGER:                      I N T E G E R;
INTERSECT:                    I N T E R S E C T;
INTERVAL:                     I N T E R V A L;
INTO:                         I N T O;
INVALIDATE:                   I N V A L I D A T E;
IS:                           I S;
ISOLATION:                    I S O L A T I O N;
ISOPEN:                       I S O P E N;    //NP: ADDED TEST 4
ITERATE:                      I T E R A T E;
JAVA:                         J A V A;
JOB:                          J O B;
JOIN:                         J O I N;
KEEP:                         K E E P;
KEEP_DUBLICATES:              K E E P '_' D U B L I C A T E S;
KEY:                          K E Y;
LAG:                          L A G;
LANGUAGE:                     L A N G U A G E;
LAST:                         L A S T;
LAST_VALUE:                   L A S T '_' V A L U E;
LEAD:                         L E A D;
LEADING:                      L E A D I N G;
LEFT:                         L E F T;
LESS:                         L E S S;
LEVEL:                        L E V E L;
LEVELS:                       L E V E L S;
LIBRARY:                      L I B R A R Y;
LIKE:                         L I K E;
LIKE2:                        L I K E '2';
LIKE4:                        L I K E '4';
LIKEC:                        L I K E C;
LIMIT:                        L I M I T;
LINK:                         L I N K;
LIST:                         L I S T;
LISTAGG:                      L I S T A G G;
LOB:                          L O B;
LOCAL:                        L O C A L;
LOCATION:                     L O C A T I O N;
LOCATOR:                      L O C A T O R;
LOCK:                         L O C K;
LOCKED:                       L O C K E D;
LOG:                          L O G;
LOGGING:                      L O G G I N G;
LOGOFF:                       L O G O F F;
LOGON:                        L O G O N;
LONG:                         L O N G;
LOOP:                         L O O P;
MAIN:                         M A I N;
MANAGE:                       M A N A G E;
MANUAL:                       M A N U A L;
MAP:                          M A P;
MAPPING:                      M A P P I N G;
MASTER:                       M A S T E R;
MATCHED:                      M A T C H E D;
MATERIALIZED:                 M A T E R I A L I Z E D;
MAX:                          M A X;
MAXEXTENTS:                   M A X E X T E N T S;
MAXVALUE:                     M A X V A L U E;
MEASURE:                      M E A S U R E;
MEASURES:                     M E A S U R E S;
MEDIAN:                       M E D I A N;
MEDIUM:                       M E D I U M;
MEMBER:                       M E M B E R;
MERGE:                        M E R G E;
MIN:                          M I N;
MINEXTENTS:                   M I N E X T E N T S;
MINING:                       M I N I N G;
MINUS:                        M I N U S;
MINUTE:                       M I N U T E;
MINVALUE:                     M I N V A L U E;
MLSLABEL:                     M L S L A B E L;
MOD:                          M O D;
MODE:                         M O D E;
MODEL:                        M O D E L;
MODIFY:                       M O D I F Y;
MONTH:                        M O N T H;
MOVEMENT:                     M O V E M E N T;
MULTISET:                     M U L T I S E T;
MYSKIP:                       S K I P;
NAME:                         N A M E;
NAMESPACE:                    N A M E S P A C E;
NAN:                          N A N;
NATURAL:                      N A T U R A L;
NATURALN:                     N A T U R A L N;
NAV:                          N A V;
NCHAR:                        N C H A R;
NCHAR_CS:                     N C H A R '_' C S;
NCLOB:                        N C L O B;
NESTED:                       N E S T E D;
NEVER:                        N E V E R;
NEW:                          N E W;
NEXT:                         N E X T;
NO:                           N O;
NOAUDIT:                      N O A U D I T;
NOCACHE:                      N O C A C H E;
NOCOMPRESS:                   N O C O M P R E S S;
NOCOPY:                       N O C O P Y;
NOCYCLE:                      N O C Y C L E;
NOENTITYESCAPING:             N O E N T I T Y E S C A P I N G;
NOLOGGING:                    N O L O G G I N G;
NOMAPPING:                    N O M A P P I N G;
NOMAXVALUE:                   N O M A X V A L U E;
NOMINVALUE:                   N O M I N V A L U E;
NONE:                         N O N E;
NONSCHEMA:                    N O N S C H E M A;
NOORDER:                      N O O R D E R;
NOPARALLEL:                   N O P A R A L L E L;
NORELY:                       N O R E L Y;
NOROWDEPENDENCIES:            N O R O W D E P E N D E N C I E S;
NOSCHEMACHECK:                N O S C H E M A C H E C K;
NOSORT:                       N O S O R T;
NOT:                          N O T;
NOTFOUND:                     N O T F O U N D;    //NP: ADDED TEST 4
NOTIFICATION:                 N O T I F I C A T I O N;
NOVALIDATE:                   N O V A L I D A T E;
NOWAIT:                       N O W A I T;
NTILE:                        N T I L E;
NULL:                         N U L L;
NULLS:                        N U L L S;
NUMBER:                       N U M B E R;
NUMERIC:                      N U M E R I C;
NVARCHAR2:                    N V A R C H A R '2';
OBJECT:                       O B J E C T;
OF:                           O F;
OFF:                          O F F;
OID:                          O I D;
OIDINDEX:                     O I D I N D E X;
OLD:                          O L D;
ON:                           O N;
ONLINE:                       O N L I N E;
ONLY:                         O N L Y;
OPEN:                         O P E N;
OPERATIONS:                   O P E R A T I O N S;
OPERATOR:                     O P E R A T O R;
OPTIMAL:                      O P T I M A L;
OPTION:                       O P T I O N;
OR:                           O R;
ORADATA:                      O R A D A T A;
ORDER:                        O R D E R;
ORDINALITY:                   O R D I N A L I T Y;
ORGANIZATION:                 O R G A N I Z A T I O N;
OSERROR:                      O S E R R O R;
OUT:                          O U T;
OUTER:                        O U T E R;
OUTLINE:                      O U T L I N E;
OVER:                         O V E R;
OVERFLOW:                     O V E R F L O W;
OVERRIDING:                   O V E R R I D I N G;
PACKAGE:                      P A C K A G E;
PARALLEL:                     P A R A L L E L;
PARALLEL_ENABLE:              P A R A L L E L '_' E N A B L E;
PARAMETERS:                   P A R A M E T E R S;
PARENT:                       P A R E N T;
PARTITION:                    P A R T I T I O N;
PARTITIONS:                   P A R T I T I O N S;
PASSING:                      P A S S I N G;
PATH:                         P A T H;
PATHS:                        P A T H S;
PCTFREE:                      P C T F R E E;
PCTINCREASE:                  P C T I N C R E A S E;
PCTTHRESHOLD:                 P C T T H R E S H O L D;
PCTUSED:                      P C T U S E D;
PCTVERSION:                   P C T V E R S I O N;
PERCENT_RANK:                 P E R C E N T '_' R A N K;
PERCENT_ROWTYPE:              '%' P E R C E N T '_' R O W T Y P E;
PERCENT_TYPE:                 '%' P E R C E N T '_' T Y P E;
PERCENTILE_CONT:              P E R C E N T I L E '_' C O N T;
PERCENTILE_DISC:              P E R C E N T I L E '_' D I S C;
PIPE:                         P I P E;
PIPELINED:                    P I P E L I N E D;
PIVOT:                        P I V O T;
PLAN:                         P L A N;
PLS_INTEGER:                  P L S '_' I N T E G E R;
POLICY:                       P O L I C Y;
POSITIVE:                     P O S I T I V E;
POSITIVEN:                    P O S I T I V E N;
PRAGMA:                       P R A G M A;
PREBUILT:                     P R E B U I L T;
PRECEDES:                     P R E C E D E S;
PRECEDING:                    P R E C E D I N G;
PRECISION:                    P R E C I S I O N;
PREDICTION:                   P R E D I C T I O N;
PREDICTION_BOUNDS:            P R E D I C T I O N '_' B O U N D S;
PREDICTION_COST:              P R E D I C T I O N '_' C O S T;
PREDICTION_DETAILS:           P R E D I C T I O N '_' D E T A I L S;
PREDICTION_PROBABILITY:       P R E D I C T I O N '_' P R O B A B I L I T Y;
PREDICTION_SET:               P R E D I C T I O N '_' S E T;
PRESENT:                      P R E S E N T;
PRESERVE:                     P R E S E R V E;
PRIMARY:                      P R I M A R Y;
PRIOR:                        P R I O R;
PRIVILEGE:                    P R I V I L E G E;
PRIVILEGES:                   P R I V I L E G E S;
PROCEDURE:                    P R O C E D U R E;
PROCESS:                      P R O C E S S;
PROFILE:                      P R O F I L E;
PROGRAM:                      P R O G R A M;
PUBLIC:                       P U B L I C;
PURGE:                        P U R G E;
QUERY:                        Q U E R Y;
RAISE:                        R A I S E;
RANGE:                        R A N G E;
RANK:                         R A N K;
RATIO_TO_REPORT:              R A T I O '_' T O '_' R  E P O R T;
RAW:                          R A W;
READ:                         R E A D;
READS:                        R E A D S;
REAL:                         R E A L;
RECORD:                       R E C O R D;
RECOVERY_CATALOG_OWNER:       R E C O V E R Y '_' C A T A L O G '_' O W N E R;
RECYCLE:                      R E C Y C L E;
REDUCED:                      R E D U C E D;
REF:                          R E F;
REFERENCE:                    R E F E R E N C E;
REFERENCES:                   R E F E R E N C E S;
REFERENCING:                  R E F E R E N C I N G;
REFRESH:                      R E F R E S H;
REGR_:                        R E G R '_';
REJECT:                       R E J E C T;
RELATIONAL:                   R E L A T I O N A L;
RELIES_ON:                    R E L I E S '_' O N;
RELY:                         R E L Y;
REMOVE:                       R E M O V E;
RENAME:                       R E N A M E;
REPEAT:                       R E P E A T;
REPLACE:                      R E P L A C E;
RESOURCE:                     R E S O U R C E;
RESPECT:                      R E S P E C T;
RESTRICT_REFERENCES:          R E S T R I C T '_' R E F E R E N C E S;
RESTRICTED:                   R E S T R I C T E D;
RESULT:                       R E S U L T;
RESULT_CACHE:                 R E S U L T '_' C A C H E;
RESUMABLE:                    R E S U M A B L E;
RETENTION:                    R E T E N T I O N;
RETURN:                       R E T U R N;
RETURNING:                    R E T U R N I N G;
REUSE:                        R E U S E;
REVERSE:                      R E V E R S E;
REVOKE:                       R E V O K E;
REWRITE:                      R E W R I T E;
RIGHT:                        R I G H T;
ROLE:                         R O L E;
ROLLBACK:                     R O L L B A C K;
ROLLUP:                       R O L L U P;
ROUND:                        R O U N D;
ROW:                          R O W;
ROW_NUMBER:                   R O W '_' N U M B E R;
ROWCOUNT:                     R O W C O U N T;
ROWDEPENDENCIES:              R O W D E P E N D E N C I E S;
ROWID:                        R O W I D;
ROWS:                         R O W S;
ROWTYPE:                      R O W T Y P E;
RULES:                        R U L E S;
SALT:                         S A L T;
SAMPLE:                       S A M P L E;
SAVE:                         S A V E;
SAVEPOINT:                    S A V E P O I N T;
SCHEDULER:                    S C H E D U L E R;
SCHEMA:                       S C H E M A;
SCHEMACHECK:                  S C H E M A C H E C K;
SCN:                          S C N;
SCOPE:                        S C O P E;
SEARCH:                       S E A R C H;
SECOND:                       S E C O N D;
SECUREFILE:                   S E C U R E F I L E;
SEED:                         S E E D;
SEGMENT:                      S E G M E N T;
SELECT:                       S E L E C T;
SELECT_CATALOG_ROLE:          S E L E C T '_' C A T A L O G '_' R O L E;
SELF:                         S E L F;
SEQUENCE:                     S E Q U E N C E;
SEQUENTIAL:                   S E Q U E N T I A L;
SERIALIZABLE:                 S E R I A L I Z A B L E;
SERIALLY_REUSABLE:            S E R I A L L Y '_' R E U S A B L E;
SERVERERROR:                  S E R V E R E R R O R;
SESSION:                      S E S S I O N;
SESSIONTIMEZONE:              S E S S I O N T I M E Z O N E;
SET:                          S E T;
SETS:                         S E T S;
SETTINGS:                     S E T T I N G S;
SHARE:                        S H A R E;
SHOW:                         S H O W;
SHUTDOWN:                     S H U T D O W N;
SIBLINGS:                     S I B L I N G S;
SIGNTYPE:                     S I G N T Y P E;
SIMPLE_INTEGER:               S I M P L E '_' I N T E G E R;
SINGLE:                       S I N G L E;
SIZE:                         S I Z E;
SMALLINT:                     S M A L L I N T;
SNAPSHOT:                     S N A P S H O T;
SNMPAGENT:                    S N M P A G E N T;
SOME:                         S O M E;
SORT:                         S O R T;
SOURCE:                       S O U R C E;
SPECIFICATION:                S P E C I F I C A T I O N;
SQL:                          S Q L; //NP: Added Test 4
SQLDATA:                      S Q L D A T A;
SQLERROR:                     S Q L E R R O R;
STANDALONE:                   S T A N D A L O N E;
STALE:                        S T A L E;
START:                        S T A R T;
STARTUP:                      S T A R T U P;
STATEMENT:                    S T A T E M E N T;
STATEMENT_ID:                 S T A T E M E N T '_' I D;
STATIC:                       S T A T I C;
STATISTICS:                   S T A T I S T I C S;
STDDEV:                       S T D D E V;
STORAGE:                      S T O R A G E;
STORE:                        S T O R E;
STRING:                       S T R I N G;
SUBMULTISET:                  S U B M U L T I S E T;
SUBPARTITION:                 S U B P A R T I T I O N;
SUBPARTITIONS:                S U B P A R T I T I O N S;
SUBSTITUTABLE:                S U B S T I T U T A B L E;
SUBTYPE:                      S U B T Y P E;
SUCCESS:                      S U C C E S S;
SUM:                          S U M;
SUPPLEMENTAL:                 S U P P L E M E N T A L;
SUSPEND:                      S U S P E N D;
SYNC:                         S Y N C;
SYNCHRONOUS:                  S Y N C H R O N O U S;
SYNONYM:                      S Y N O N Y M;
SYSDBA:                       S Y S D B A;
SYSDATE:                      S Y S D A T E;
SYSOPER:                      S Y S O P E R;
SYSTEM:                       S Y S T E M;
TABLE:                        T A B L E;
TABLESPACE:                   T A B L E S P A C E;
TEMPLATE:                     T E M P L A T E;
TEMPORARY:                    T E M P O R A R Y;
THAN:                         T H A N;
THE:                          T H E;
THEN:                         T H E N;
TIME:                         T I M E;
TIMESTAMP:                    T I M E S T A M P;
TIMESTAMP_LTZ_UNCONSTRAINED:  T I M E S T A M P '_' L T Z '_' U N C O N S T R A I N E D;
TIMESTAMP_TZ_UNCONSTRAINED:   T I M E S T A M P '_' T Z '_' U N C O N S T R A I N E D;
TIMESTAMP_UNCONSTRAINED:      T I M E S T A M P '_' U N C O N S T R A I N E D;
TIMEZONE_ABBR:                T I M E Z O N E '_' A B B R;
TIMEZONE_HOUR:                T I M E Z O N E '_' H O U R;
TIMEZONE_MINUTE:              T I M E Z O N E '_' M I N U T E;
TIMEZONE_REGION:              T I M E Z O N E '_' R E G I O N;
TO:                           T O;
TO_DATE:                      T O '_' D A T E;
TRAILING:                     T R A I L I N G;
TRANSACTION:                  T R A N S A C T I O N;
TRANSLATE:                    T R A N S L A T E;
TREAT:                        T R E A T;
TRIGGER:                      T R I G G E R;
TRIM:                         T R I M;
TRUE:                         T R U E;
TRUNC:                        T R U N C;
TRUSTED:                      T R U S T E D;
TRUNCATE:                     T R U N C A T E;
TUNING:                       T U N I N G;
TYPE:                         T Y P E;
UNBOUNDED:                    U N B O U N D E D;
UNDER:                        U N D E R;
UNION:                        U N I O N;
UNIQUE:                       U N I Q U E;
UNLIMITED:                    U N L I M I T E D;
UNPIVOT:                      U N P I V O T;
UNTIL:                        U N T I L;
UNUSUABLE:                    U N U S U A B L E;
UPDATE:                       U P D A T E;
UPDATED:                      U P D A T E D;
UPSERT:                       U P S E R T;
UROWID:                       U R O W I D;
USE:                          U S E;
USER:                         U S E R;
USING:                        U S I N G;
VALIDATE:                     V A L I D A T E;
VALUE:                        V A L U E;
VALUES:                       V A L U E S;
VAR_:                         V A R '_';
VARCHAR:                      V A R C H A R;
VARCHAR2:                     V A R C H A R '2';
VARIABLE:                     V A R I A B L E;
VARIANCE:                     V A R I A N C E;
VARRAY:                       V A R R A Y;
VARYING:                      V A R Y I N G;
VERSION:                      V E R S I O N;
VERSIONS:                     V E R S I O N S;
VIEW:                         V I E W;
VIRTUAL:                      V I R T U A L;
WAIT:                         W A I T;
WARNING:                      W A R N I N G;
WELLFORMED:                   W E L L F O R M E D;
WHEN:                         W H E N;
WHENEVER:                     W H E N E V E R;
WHERE:                        W H E R E;
WHILE:                        W H I L E;
WITH:                         W I T H;
WITHOUT:                      W I T H O U T;
WITHIN:                       W I T H I N;
WORK:                         W O R K;
WRITE:                        W R I T E;
XDB:                          X D B;
XML:                          X M L;
XMLAGG:                       X M L A G G;
XMLATTRIBUTES:                X M L A T T R I B U T E S;
XMLCAST:                      X M L C A S T;
XMLCOLATTVAL:                 X M L C O L A T T V A L;
XMLELEMENT:                   X M L E L E M E N T;
XMLEXISTS:                    X M L E X I S T S;
XMLFOREST:                    X M L F O R E S T;
XMLINDEX:                     X M L I N D E X;
XMLNAMESPACES:                X M L N A M E S P A C E S;
XMLPARSE:                     X M L P A R S E;
XMLPI:                        X M L P I;
XMLQUERY:                     X M L Q U E R Y;
XMLROOT:                      X M L R O O T;
XMLSCHEMA:                    X M L S C H E M A;
XMLSCHEMAS:                   X M L S C H E M A S;
XMLSERIALIZE:                 X M L S E R I A L I Z E;
XMLTABLE:                     X M L T A B L E;
XMLTYPE:                      X M L T Y P E;
YEAR:                         Y E A R;
YES:                          Y E S;
YMINTERVAL_UNCONSTRAINED:     Y M I N T E R V A L '_' U N C O N S T R A I N E D;
ZONE:                         Z O N E;

fragment A: [aA];
fragment B: [bB];
fragment C: [cC];
fragment D: [dD];
fragment E: [eE];
fragment F: [fF];
fragment G: [gG];
fragment H: [hH];
fragment I: [iI];
fragment J: [jJ];
fragment K: [kK];
fragment L: [lL];
fragment M: [mM];
fragment N: [nN];
fragment O: [oO];
fragment P: [pP];
fragment Q: [qQ];
fragment R: [rR];
fragment S: [sS];
fragment T: [tT];
fragment U: [uU];
fragment V: [vV];
fragment W: [wW];
fragment X: [xX];
fragment Y: [yY];
fragment Z: [zZ];

/*FOR_NOTATION
    :    UNSIGNED_INTEGER
        {state.type = UNSIGNED_INTEGER; emit(); advanceInput();}
        '..'
        {state.type = DOUBLE_PERIOD; emit(); advanceInput();}
        UNSIGNED_INTEGER
        {state.type = UNSIGNED_INTEGER; emit(); advanceInput(); $channel=HIDDEN;}
    ;
*/

//{ Rule #358 <NATIONAL_CHAR_STRING_LIT> - subtoken typecast in <REGULAR_ID>, it also incorporates <character_representation>
//  Lowercase 'n' is a usual addition to the standard
NATIONAL_CHAR_STRING_LIT
    : N '\'' (~('\'' | '\r' | '\n' ) | '\'' '\'' | NEWLINE)* '\''
    ;
//}

//{ Rule #040 <BIT_STRING_LIT> - subtoken typecast in <REGULAR_ID>
//  Lowercase 'b' is a usual addition to the standard
BIT_STRING_LIT
    : B ('\'' ('0' | '1')* '\'' /*SEPARATOR?*/ )+
    ;
//}


//{ Rule #284 <HEX_STRING_LIT> - subtoken typecast in <REGULAR_ID>
//  Lowercase 'x' is a usual addition to the standard
HEX_STRING_LIT
    : X ('\'' ('a'..'f' | 'A'..'F' | '0'..'9')* '\'' /*SEPARATOR?*/ )+
    ;
//}

DOUBLE_PERIOD
    : '.' '.'
    ;

PERIOD
    : '.'
    ;

//{ Rule #238 <EXACT_NUM_LIT>
//  This rule is a bit tricky - it resolves the ambiguity with <PERIOD>
//  It als44o incorporates <mantisa> and <exponent> for the <APPROXIMATE_NUM_LIT>
//  Rule #501 <signed_integer> was incorporated directly in the token <APPROXIMATE_NUM_LIT>
//  See also the rule #617 <unsigned_num_lit>
/*
    : (
            UNSIGNED_INTEGER
            ( '.' UNSIGNED_INTEGER
            | {$type = UNSIGNED_INTEGER;}
            ) ( E ('+' | '-')? UNSIGNED_INTEGER {$type = APPROXIMATE_NUM_LIT;} )?
    | '.' UNSIGNED_INTEGER ( E ('+' | '-')? UNSIGNED_INTEGER {$type = APPROXIMATE_NUM_LIT;} )?
    )
    (D | F)?
    ;*/
//}

UNSIGNED_INTEGER: UNSIGNED_INTEGER_FRAGMENT;
APPROXIMATE_NUM_LIT: FLOAT_FRAGMENT (('e'|'E') ('+'|'-')? (FLOAT_FRAGMENT | UNSIGNED_INTEGER_FRAGMENT))? (D | F)?;


//{ Rule #--- <CHAR_STRING> is a base for Rule #065 <char_string_lit> , it incorporates <character_representation>
//  and a superfluous subtoken typecasting of the "QUOTE"
CHAR_STRING
    : '\'' (~('\'' | '\r' | '\n') | '\'' '\'' | NEWLINE)* '\''
    ;
//}

// Perl-style quoted string, see Oracle SQL reference, chapter String Literals
CHAR_STRING_PERL    : Q ( QS_ANGLE | QS_BRACE | QS_BRACK | QS_PAREN /*| QS_OTHER*/) -> type(CHAR_STRING);
fragment QUOTE      : '\'' ;
fragment QS_ANGLE   : QUOTE '<' .*? '>' QUOTE ;
fragment QS_BRACE   : QUOTE '{' .*? '}' QUOTE ;
fragment QS_BRACK   : QUOTE '[' .*? ']' QUOTE ;
fragment QS_PAREN   : QUOTE '(' .*? ')' QUOTE ;

fragment QS_OTHER_CH: ~('<' | '{' | '[' | '(' | ' ' | '\t' | '\n' | '\r');
/*fragment QS_OTHER:
    QUOTE QS_OTHER_CH { delimeter = _input.La(-1); }
    (. { _input.La(-1) != delimeter }? )* QS_OTHER_CH QUOTE;*/
/*fragment QS_OTHER
//    For C target we have to preserve case sensitivity.
//		@declarations {
//    		ANTLR3_UINT32 (*oldLA)(struct ANTLR3_INT_STREAM_struct *, ANTLR3_INT32);
//		}
//		@init {
//			oldLA = INPUT->istream->_LA;
//            INPUT->setUcaseLA(INPUT, ANTLR3_FALSE);
//		}
		:
		QUOTE delimiter=QS_OTHER_CH
// JAVA Syntax
    ( { input.LT(1) != $delimiter.text.charAt(0) || ( input.LT(1) == $delimiter.text.charAt(0) && input.LT(2) != '\'') }? => . )*
    ( { input.LT(1) == $delimiter.text.charAt(0) && input.LT(2) == '\'' }? => . ) QUOTE
		;*/

//{ Rule #163 <DELIMITED_ID>
DELIMITED_ID
    : '"' (~('"' | '\r' | '\n') | '"' '"')+ '"'
    ;
//}

//{ Rule #546 <SQL_SPECIAL_CHAR> was split into single rules
PERCENT
    : '%'
    ;

AMPERSAND
    : '&'
    ;

LEFT_PAREN
    : '('
    ;

RIGHT_PAREN
    : ')'
    ;

DOUBLE_ASTERISK
    : '**'
    ;

ASTERISK
    : '*'
    ;

PLUS_SIGN
    : '+'
    ;

MINUS_SIGN
    : '-'
    ;

COMMA
    : ','
    ;

SOLIDUS
    : '/'
    ;

AT_SIGN
    : '@'
    ;

ASSIGN_OP
    : ':='
    ;

// See OCI reference for more information about this
BINDVAR
    : ':' SIMPLE_LETTER  (SIMPLE_LETTER | '0' .. '9' | '_')*
    | ':' DELIMITED_ID  // not used in SQL but spotted in v$sqltext when using cursor_sharing
    | ':' UNSIGNED_INTEGER
    | QUESTION_MARK // not in SQL, not in Oracle, not in OCI, use this for JDBC
    ;

COLON
    : ':'
    ;

SEMICOLON
    : ';'
    ;

LESS_THAN_OR_EQUALS_OP
    : '<='
    ;

LESS_THAN_OP
    : '<'
    ;

GREATER_THAN_OR_EQUALS_OP
    : '>='
    ;

NOT_EQUAL_OP
    : '!='
    | '<>'
    | '^='
    | '~='
    ;

CARRET_OPERATOR_PART
    : '^'
    ;

TILDE_OPERATOR_PART
    : '~'
    ;

EXCLAMATION_OPERATOR_PART
    : '!'
    ;

GREATER_THAN_OP
    : '>'
    ;

fragment
QUESTION_MARK
    : '?'
    ;

// protected UNDERSCORE : '_' SEPARATOR ; // subtoken typecast within <INTRODUCER>
CONCATENATION_OP
    : '||'
    ;

VERTICAL_BAR
    : '|'
    ;

EQUALS_OP
    : '='
    ;

//{ Rule #532 <SQL_EMBDD_LANGUAGE_CHAR> was split into single rules:
LEFT_BRACKET
    : '['
    ;

RIGHT_BRACKET
    : ']'
    ;

//}

//{ Rule #319 <INTRODUCER>
INTRODUCER
    : '_' //(SEPARATOR {$type = UNDERSCORE;})?
    ;

//{ Rule #479 <SEPARATOR>
//  It was originally a protected rule set to be filtered out but the <COMMENT> and <'-'> clashed.
/*SEPARATOR
    : '-' -> type('-')
    | COMMENT -> channel(HIDDEN)
    | (SPACE | NEWLINE)+ -> channel(HIDDEN)
    ;*/
//}

SPACES
    : [ \t\r\n]+ -> skip
    ;

//{ Rule #504 <SIMPLE_LETTER> - simple_latin _letter was generalised into SIMPLE_LETTER
//  Unicode is yet to be implemented - see NSF0
fragment
SIMPLE_LETTER
    : 'a'..'z'
    | 'A'..'Z'
    | '\u03b1'..'\u03ce'
    ;
//}

//  Rule #176 <DIGIT> was incorporated by <UNSIGNED_INTEGER>
//{ Rule #615 <UNSIGNED_INTEGER> - subtoken typecast in <EXACT_NUM_LIT>
fragment
    UNSIGNED_INTEGER_FRAGMENT
    : ('0'..'9')+
    ;
//}

fragment
FLOAT_FRAGMENT
    : UNSIGNED_INTEGER* '.'? UNSIGNED_INTEGER+
    ;

//{ Rule #097 <COMMENT>
SINGLE_LINE_COMMENT: '--' ( ~('\r' | '\n') )* (NEWLINE|EOF) -> channel(HIDDEN);
MULTI_LINE_COMMENT: '/*' .*? '*/' -> channel(HIDDEN);

// SQL*Plus prompt
// TODO should be grammar rule, but tricky to implement
PROMPT
	: 'prompt' SPACE ( ~('\r' | '\n') )* (NEWLINE|EOF)
	;

//{ Rule #360 <NEWLINE>
fragment
NEWLINE: '\r'? '\n';

fragment
SPACE: [ \t];

//{ Rule #442 <REGULAR_ID> additionally encapsulates a few STRING_LITs.
//  Within testLiterals all reserved and non-reserved words are being resolved

SQL92_RESERVED_ALL
    : 'all'
    ;

SQL92_RESERVED_ALTER
    : 'alter'
    ;

SQL92_RESERVED_AND
    : 'and'
    ;

SQL92_RESERVED_ANY
    : 'any'
    ;

SQL92_RESERVED_AS
    : 'as'
    ;

SQL92_RESERVED_ASC
    : 'asc'
    ;

//SQL92_RESERVED_AT
//    : 'at'
//    ;

SQL92_RESERVED_BEGIN
    : 'begin'
    ;

SQL92_RESERVED_BETWEEN
    : 'between'
    ;

SQL92_RESERVED_BY
    : 'by'
    ;

SQL92_RESERVED_CASE
    : 'case'
    ;

SQL92_RESERVED_CHECK
    : 'check'
    ;

PLSQL_RESERVED_CLUSTERS
    : 'clusters'
    ;

PLSQL_RESERVED_COLAUTH
    : 'colauth'
    ;

PLSQL_RESERVED_COMPRESS
    : 'compress'
    ;

SQL92_RESERVED_CONNECT
    : 'connect'
    ;

//PLSQL_NON_RESERVED_COLUMNS
//    : 'columns'
//    ;

PLSQL_NON_RESERVED_CONNECT_BY_ROOT
    : 'connect_by_root'
    ;

PLSQL_RESERVED_CRASH
    : 'crash'
    ;

SQL92_RESERVED_CREATE
    : 'create'
    ;

SQL92_RESERVED_CURRENT
    : 'current'
    ;

SQL92_RESERVED_CURSOR
    : 'cursor'
    ;

SQL92_RESERVED_DATE
    : 'date'
    ;

SQL92_RESERVED_DECLARE
    : 'declare'
    ;

SQL92_RESERVED_DEFAULT
    : 'default'
    ;

SQL92_RESERVED_DELETE
    : 'delete'
    ;

SQL92_RESERVED_DESC
    : 'desc'
    ;

SQL92_RESERVED_DISTINCT
    : 'distinct'
    ;

SQL92_RESERVED_DROP
    : 'drop'
    ;

SQL92_RESERVED_ELSE
    : 'else'
    ;

SQL92_RESERVED_END
    : 'end'
    ;

SQL92_RESERVED_EXCEPTION
    : 'exception'
/* TODO    // "exception" is a keyword only withing the contex of the PL/SQL language
    // while it can be an identifier(column name, table name) in SQL
    // "exception" is a keyword if and only it is followed by "when"
    {
    $e.setType(SQL92_RESERVED_EXCEPTION);
    emit($e);
    advanceInput();

    $type = Token.INVALID_TOKEN_TYPE;
    int markModel = input.mark();

    // Now loop over next Tokens in the input and eventually set Token's type to REGULAR_ID

    // Subclassed version will return NULL unless EOF is reached.
    // nextToken either returns NULL => then the next token is put into the queue tokenBuffer
    // or it returns Token.EOF, then nothing is put into the queue
    Token t1 = super.nextToken();
    {    // This "if" handles the situation when the "model" is the last text in the input.
        if( t1 != null && t1.getType() == Token.EOF)
        {
             $e.setType(REGULAR_ID);
        } else {
             t1 = tokenBuffer.pollLast(); // "withdraw" the next token from the queue
             while(true)
             {
                 if(t1.getType() == EOF)   // is it EOF?
                 {
                     $e.setType(REGULAR_ID);
                     break;
                 }

                 if(t1.getChannel() == HIDDEN) // is it a white space? then advance to the next token
                 {
                     t1 = super.nextToken(); if( t1 == null) { t1 = tokenBuffer.pollLast(); };
                     continue;
                 }

                 if( t1.getType() != SQL92_RESERVED_WHEN && t1.getType() != SEMICOLON) // is something other than "when"
                 {
                     $e.setType(REGULAR_ID);
                     break;
                 }

                 break; // we are in the model_clase do not rewrite anything
              } // while true
         } // else if( t1 != null && t1.getType() == Token.EOF)
    }
    input.rewind(markModel);
    }*/
    ;

PLSQL_RESERVED_EXCLUSIVE
    : 'exclusive'
    ;

SQL92_RESERVED_EXISTS
    : 'exists'
    ;

SQL92_RESERVED_FALSE
    : 'false'
    ;

SQL92_RESERVED_FETCH
    : 'fetch'
    ;

SQL92_RESERVED_FOR
    : 'for'
    ;

SQL92_RESERVED_FROM
    : 'from'
    ;

SQL92_RESERVED_GOTO
    : 'goto'
    ;

SQL92_RESERVED_GRANT
    : 'grant'
    ;

SQL92_RESERVED_GROUP
    : 'group'
    ;

SQL92_RESERVED_HAVING
    : 'having'
    ;

PLSQL_RESERVED_IDENTIFIED
    : 'identified'
    ;

PLSQL_RESERVED_IF
    : 'if'
    ;

SQL92_RESERVED_IN
    : 'in'
    ;

PLSQL_RESERVED_INDEX
    : 'index'
    ;

PLSQL_RESERVED_INDEXES
    : 'indexes'
    ;

SQL92_RESERVED_INSERT
    : 'insert'
    ;

SQL92_RESERVED_INTERSECT
    : 'intersect'
    ;

SQL92_RESERVED_INTO
    : 'into'
    ;

SQL92_RESERVED_IS
    : 'is'
    ;

SQL92_RESERVED_LIKE
    : 'like'
    ;

PLSQL_RESERVED_LOCK
    : 'lock'
    ;

PLSQL_RESERVED_MINUS
    : 'minus'
    ;

PLSQL_RESERVED_MODE
    : 'mode'
    ;

PLSQL_RESERVED_NOCOMPRESS
    : 'nocompress'
    ;

SQL92_RESERVED_NOT
    : 'not'
    ;

PLSQL_RESERVED_NOWAIT
    : 'nowait'
    ;

SQL92_RESERVED_NULL
    : 'null'
    ;

SQL92_RESERVED_OF
    : 'of'
    ;

SQL92_RESERVED_ON
    : 'on'
    ;

SQL92_RESERVED_OPTION
    : 'option'
    ;

SQL92_RESERVED_OR
    : 'or'
    ;

SQL92_RESERVED_ORDER
    : 'order'
    ;

SQL92_RESERVED_OVERLAPS
    : 'overlaps'
    ;

SQL92_RESERVED_PRIOR
    : 'prior'
    ;

SQL92_RESERVED_PROCEDURE
    : 'procedure'
    ;

SQL92_RESERVED_PUBLIC
    : 'public'
    ;

PLSQL_RESERVED_RESOURCE
    : 'resource'
    ;

SQL92_RESERVED_REVOKE
    : 'revoke'
    ;

SQL92_RESERVED_SELECT
    : 'select'
    ;

PLSQL_RESERVED_SHARE
    : 'share'
    ;

SQL92_RESERVED_SIZE
    : 'size'
    ;

// SQL92_RESERVED_SQL
//     : 'sql'
//     ;

PLSQL_RESERVED_START
    : 'start'
    ;

PLSQL_RESERVED_TABAUTH
    : 'tabauth'
    ;

SQL92_RESERVED_TABLE
    : 'table'
    ;

SQL92_RESERVED_THE
    : 'the'
    ;

SQL92_RESERVED_THEN
    : 'then'
    ;

SQL92_RESERVED_TO
    : 'to'
    ;

SQL92_RESERVED_TRUE
    : 'true'
    ;

SQL92_RESERVED_UNION
    : 'union'
    ;

SQL92_RESERVED_UNIQUE
    : 'unique'
    ;

SQL92_RESERVED_UPDATE
    : 'update'
    ;

SQL92_RESERVED_VALUES
    : 'values'
    ;

SQL92_RESERVED_VIEW
    : 'view'
    ;

PLSQL_RESERVED_VIEWS
    : 'views'
    ;

SQL92_RESERVED_WHEN
    : 'when'
    ;

SQL92_RESERVED_WHERE
    : 'where'
    ;

SQL92_RESERVED_WITH
    : 'with'
    ;

PLSQL_NON_RESERVED_USING
    : 'using'
    ;

PLSQL_NON_RESERVED_MODEL
    : 'model'
    /* TODO {
         // "model" is a keyword if and only if it is followed by ("main"|"partition"|"dimension")
         // otherwise it is a identifier(REGULAR_ID).
         // This wodoo implements something like context sensitive lexer.
         // Here we've matched the word "model". Then the Token is created and en-queued in tokenBuffer
         // We still remember the reference($m) onto this Token
         $m.setType(PLSQL_NON_RESERVED_MODEL);
         emit($m);
         advanceInput();

         $type = Token.INVALID_TOKEN_TYPE;
         int markModel = input.mark();

         // Now loop over next Tokens in the input and eventually set Token's type to REGULAR_ID

         // Subclassed version will return NULL unless EOF is reached.
         // nextToken either returns NULL => then the next token is put into the queue tokenBuffer
         // or it returns Token.EOF, then nothing is put into the queue
         Token t1 = super.nextToken();
         {    // This "if" handles the situation when the "model" is the last text in the input.
              if( t1 != null && t1.getType() == Token.EOF)
              {
                  $m.setType(REGULAR_ID);
              } else {
                  t1 = tokenBuffer.pollLast(); // "withdraw" the next token from the queue
                  while(true)
                  {
                     if(t1.getType() == EOF)   // is it EOF?
                     {
                         $m.setType(REGULAR_ID);
                         break;
                     }

                     if(t1.getChannel() == HIDDEN) // is it a white space? then advance to the next token
                     {
                         t1 = super.nextToken(); if( t1 == null) { t1 = tokenBuffer.pollLast(); };
                         continue;
                     }

                     if( t1.getType() != REGULAR_ID || // is something other than ("main"|"partition"|"dimension")
                        ( !t1.getText().equalsIgnoreCase("main") &&
                          !t1.getText().equalsIgnoreCase("partition") &&
                          !t1.getText().equalsIgnoreCase("dimension")
                       ))
                     {
                         $m.setType(REGULAR_ID);
                         break;
                     }

                     break; // we are in the model_clase do not rewrite anything
                  } // while true
              } // else if( t1 != null && t1.getType() == Token.EOF)
         }
         input.rewind(markModel);
    }*/
    ;

PLSQL_NON_RESERVED_ELSIF: 'elsif';
PLSQL_NON_RESERVED_PIVOT: 'pivot';
PLSQL_NON_RESERVED_UNPIVOT: 'unpivot';

//if i move this on top it will create error.. i made char_2 to fix the single letter problems
REGULAR_ID
    : (SIMPLE_LETTER) (SIMPLE_LETTER | '$' | '_' | '#' | '0'..'9')*
    ;


ZV
    : '@!' -> channel(HIDDEN)
    ;
