grammar stripping;

/*
 * Parser Rules
*/

output: ANY;


/*
 * Lexer Rules
 */
SINGLE_LINE_COMMENT: '--' ~('\r' | '\n')* NEWLINE_EOF -> skip;
MULTI_LINE_COMMENT:  '/*' .*? '*/' -> skip;
ANY: . ;

fragment NEWLINE_EOF    : NEWLINE | EOF;
fragment NEWLINE        : '\r'? '\n';
